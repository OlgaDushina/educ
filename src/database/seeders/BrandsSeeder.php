<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		 DB::table('catalog_brands')->insert(
		 ['name'=>'1-veresnya',
		 'logo'=>'/img/img/brand/1_veresnya.png',
		 'url'=>'/catalog/brand/1-veresnya',
         'active'=>2		 
		     ]);
    		 DB::table('catalog_brands')->insert(
		 ['name'=>'Angry_Birds',
		 'logo'=>'/img/img/brand/Angry_Birds.png',
		 'url'=>'/catalog/brand/Angry_Birds',
         'active'=>2		 
		     ]);
			DB::table('catalog_brands')->insert(
		 ['name'=>'4D_Master',
		 'logo'=>'/img/img/brand/4D_Master.png',
		 'url'=>'/catalog/brand/4D_Master',
         'active'=>2		 
		     ]);
			DB::table('catalog_brands')->insert(
		 ['name'=>'avengus',
		 'logo'=>'/img/img/brand/avengus.png',
		 'url'=>'/catalog/brand/avengus',
         'active'=>2		 
		     ]);
			DB::table('catalog_brands')->insert(
		 ['name'=>'BABY_born',
		 'logo'=>'/img/img/brand/BABY_born.png',
		 'url'=>'/catalog/brand/BABY_born',
         'active'=>2		 
		     ]);
			DB::table('catalog_brands')->insert(
		 ['name'=>'lego',
		 'logo'=>'/img/img/brand/lego.png',
		 'url'=>'/catalog/brand/lego',
         'active'=>2		 
		     ]);
			DB::table('catalog_brands')->insert(
		 ['name'=>'chicco',
		 'logo'=>'/img/img/brand/chicco.png',
		 'url'=>'/catalog/brand/chicco',
         'active'=>2		 
		     ]);
			DB::table('catalog_brands')->insert(
		 ['name'=>'hipp',
		 'logo'=>'/img/img/brand/hipp.png',
		 'url'=>'/catalog/brand/hipp',
         'active'=>1		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Huggies',
		 'logo'=>'/img/img/brand/Huggies.png',
		 'url'=>'/catalog/brand/Huggies',
         'active'=>1		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Pampers',
		 'logo'=>'/img/img/brand/Pampers.png',
		 'url'=>'/catalog/brand/Pampers',
         'active'=>1		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Alles',
		 'logo'=>'/img/img/brand/Alles.png',
		 'url'=>'/catalog/brand/Alles',
         'active'=>3		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Arti',
		 'logo'=>'/img/img/brand/Arti.png',
		 'url'=>'/catalog/brand/Arti',
         'active'=>3		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Disney_by_RFS',
		 'logo'=>'/img/img/brand/Disney_by_RFS.png',
		 'url'=>'/catalog/brand/Disney_by_RFS',
         'active'=>2		 
		     ]);
				 
 				DB::table('catalog_brands')->insert(
		 ['name'=>'Baby_Breeze',
		 'logo'=>'/img/img/brand/Baby_Breeze.png',
		 'url'=>'/catalog/brand/Baby_Breeze',
         'active'=>3		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Bebetto',
		 'logo'=>'/img/img/brand/Bebetto.png',
		 'url'=>'/catalog/brand/Bebetto',
         'active'=>3		 
		     ]);
				DB::table('catalog_brands')->insert(
		 ['name'=>'Boy_Girl',
		 'logo'=>'/img/img/brand/Boy_Girl.png',
		 'url'=>'/catalog/brand/Boy_Girl',
         'active'=>3		 
		     ]);

    //
    }
}
