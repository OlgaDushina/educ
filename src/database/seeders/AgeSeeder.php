<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      		 DB::table('ages')->insert(
		 ['from'=>0,
		  'to'=>1,
		     ]);
     		 DB::table('ages')->insert(
		 ['from'=>1,
		  'to'=>2,
		     ]);
     		 DB::table('ages')->insert(
		 ['from'=>3,
		  'to'=>5,
		     ]);
     		 DB::table('ages')->insert(
		 ['from'=>6,
		  'to'=>9,
		     ]);
     		 DB::table('ages')->insert(
		 ['from'=>10,
		  'to'=>12,
		     ]);
     		 DB::table('ages')->insert(
		 ['from'=>12,
		  'to'=>18,
		     ]);
 
 //
    }
}
