<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Catalog_translationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      	 DB::table('catalog_translation')->insert(
		 ['name'=>'Малыши до года',
		 'catalog_id'=>'1',
		 'locale'=>'ru'
		 ]);
      	 DB::table('catalog_translation')->insert(
		 ['name'=>'Малюки до року',
		 'catalog_id'=>'1',
		 'locale'=>'uk'
		 ]);
      	 DB::table('catalog_translation')->insert(
		 ['name'=>'Игрушки',
		 'catalog_id'=>'2',
		 'locale'=>'ru'
		 ]);
      	 DB::table('catalog_translation')->insert(
		 ['name'=>'Іграшки',
		 'catalog_id'=>'2',
		 'locale'=>'uk'
		 ]);
      	 DB::table('catalog_translation')->insert(
		 ['name'=>'Одежда',
		 'catalog_id'=>'3',
		 'locale'=>'ru'
		 ]);
      	 DB::table('catalog_translation')->insert(
		 ['name'=>'Одяг',
		 'catalog_id'=>'3',
		 'locale'=>'uk'
		 ]);
  //
    }
}
