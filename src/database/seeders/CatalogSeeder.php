<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
		 DB::table('catalog')->insert(
		 ['name'=>'Малыши до года',
		 'url'=>'/catalog/Babies_up_to_a_year']);

		 DB::table('catalog')->insert(
		 ['name'=>'Игрушки',
		 'url'=>'/catalog/Toys']);
		 DB::table('catalog')->insert(
		 ['name'=>'Одежда',
		 'url'=>'/catalog/Clothes']);
        //
    }
}
