<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Menuadmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      		 DB::table('menuadmin')->insert(
		 ['name'=>'Каталог товаров',
		 'url'=>'admin/catalogi',
 	     ]);   
       		 DB::table('menuadmin')->insert(
		 ['name'=>'Категории товаров',
		 'url'=>'admin/catс',
 	     ]);   
       		 DB::table('menuadmin')->insert(
		 ['name'=>'Бренды',
		 'url'=>'admin/brand',
 	     ]); 
			DB::table('menuadmin')->insert(
		 ['name'=>'База пользователей',
		 'url'=>'admin/user',
 	     ]);   		 
    }
}
