<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('shops')->insert([
	   'name'=>'Kidsanna',
	   'addres'=>'пр-т Ушакова, 35',
	   'city'=>'Херсон',
	   'hours'=>'9:00-18:00',
	   'latitude'=>46.6378432,
	   'longitude'=>32.61668,
	   ]);
	   //
    }
}
