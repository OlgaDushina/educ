<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
		 DB::table('category')->insert(
		 ['name'=>'Одежда',
		 'catalog_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);

		 DB::table('category')->insert(
		 ['name'=>'Коляски',
		 'catalog_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);
		 DB::table('category')->insert(
		 ['name'=>'Все для кормления',
		 'catalog_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);
		  DB::table('category')->insert(
		 ['name'=>'Развивающие и обучающие',
		 'catalog_id'=>'2',
		 'url'=>'/catalog/toy/develop']);
		  DB::table('category')->insert(
		 ['name'=>'Музыкальные',
		 'catalog_id'=>'2',
		 'url'=>'/catalog/toy/music']);
		  DB::table('category')->insert(
		 ['name'=>'Куклы',
		 'catalog_id'=>'2',
		 'url'=>'/catalog/toy/doll']);
		  DB::table('category')->insert(
		 ['name'=>'Для девочек',
		 'catalog_id'=>'3',
		 'url'=>'/catalog/clothes/girl']);
		  DB::table('category')->insert(
		 ['name'=>'Для мальчиков',
		 'catalog_id'=>'3',
		 'url'=>'/catalog/clothes/boy']);
		  DB::table('category')->insert(
		 ['name'=>'Спортивная одежда',
		 'catalog_id'=>'3',
		 'url'=>'/catalog/clothes/sport']);

        //
    }

}
