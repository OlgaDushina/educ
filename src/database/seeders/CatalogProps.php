<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CatalogProps extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		 DB::table('props')->insert(
		 ['props'=>'gender',
		 'command'=>"whereIN('sex',['@','u'])",
		 'type'=>'1',
		  ]);
		 DB::table('props')->insert(
		 ['props'=>'price',
		 'command'=>"whereBetween('price', [@,@])",
         'type'=>'1',		 
		 ]); 
        //
    }
}
