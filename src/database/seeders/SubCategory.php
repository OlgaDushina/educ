<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      	 DB::table('sub_category')->insert(
		 ['name'=>'Боди',
		 'category_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);
 
         DB::table('sub_category')->insert(
		 ['name'=>'Ползунки',
		 'category_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);

      	 DB::table('sub_category')->insert(
		 ['name'=>'Человечки',
		 'category_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);
    
      	 DB::table('sub_category')->insert(
		 ['name'=>'Зимние',
		 'category_id'=>'2',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);

      	 DB::table('sub_category')->insert(
		 ['name'=>'Летние',
		 'category_id'=>'2',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);

      	 DB::table('sub_category')->insert(
		 ['name'=>'Парные',
		 'category_id'=>'2',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);
 
         DB::table('sub_category')->insert(
		 ['name'=>'Бутылки',
		 'category_id'=>'3',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);
 
         DB::table('sub_category')->insert(
		 ['name'=>'Соски',
		 'category_id'=>'3',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);

         DB::table('sub_category')->insert(
		 ['name'=>'Посуда',
		 'category_id'=>'3',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);

         DB::table('sub_category')->insert(
		 ['name'=>'Пазлы',
		 'category_id'=>'4',
		 'url'=>'/catalog/toy/develop']);

         DB::table('sub_category')->insert(
		 ['name'=>'Конструкторы',
		 'category_id'=>'4',
		 'url'=>'/catalog/toy/develop']);

         DB::table('sub_category')->insert(
		 ['name'=>'Мозаика',
		 'category_id'=>'4',
		 'url'=>'/catalog/toy/develop']);

         DB::table('sub_category')->insert(
		 ['name'=>'Клавишные инструменты',
		 'category_id'=>'5',
		 'url'=>'/catalog/toy/music']);

          DB::table('sub_category')->insert(
		 ['name'=>'Барабаны',
		 'category_id'=>'5',
		 'url'=>'/catalog/toy/music']);


         DB::table('sub_category')->insert(
		 ['name'=>'Балалайки',
		 'category_id'=>'5',
		 'url'=>'/catalog/toy/music']);


         DB::table('sub_category')->insert(
		 ['name'=>'Baby birn',
		 'category_id'=>'6',
		 'url'=>'/catalog/toy/doll']);

         DB::table('sub_category')->insert(
		 ['name'=>'Барби',
		 'category_id'=>'6',
		 'url'=>'/catalog/toy/doll']);
		 
		 DB::table('sub_category')->insert(
		 ['name'=>'Семейные пары',
		 'category_id'=>'6',
		 'url'=>'/catalog/toy/doll']);

          DB::table('sub_category')->insert(
		 ['name'=>'Платья',
		 'category_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);
 
         DB::table('sub_category')->insert(
		 ['name'=>'Юбки',
		 'category_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);

          DB::table('sub_category')->insert(
		 ['name'=>'Блузки',
		 'category_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);
 
         DB::table('sub_category')->insert(
		 ['name'=>'Сарафаны',
		 'category_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);
 
         DB::table('sub_category')->insert(
		 ['name'=>'Брюки',
		 'category_id'=>'8',
		 'url'=>'/catalog/clothes/boy']);

         DB::table('sub_category')->insert(
		 ['name'=>'Костюмы',
		 'category_id'=>'8',
		 'url'=>'/catalog/clothes/boy']);

         DB::table('sub_category')->insert(
		 ['name'=>'Футболки',
		 'category_id'=>'8',
		 'url'=>'/catalog/clothes/boy']);

         DB::table('sub_category')->insert(
		 ['name'=>'Спортивные костюмы',
		 'category_id'=>'9',
		 'url'=>'/catalog/clothes/sport']);
		 
		 DB::table('sub_category')->insert(
		 ['name'=>'Спортивные штаны',
		 'category_id'=>'9',
		 'url'=>'/catalog/clothes/sport']);

//
    }
}
