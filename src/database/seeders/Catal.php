<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Catal extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		 DB::table('catal')->insert(
		 ['name'=>'Одежда',
		 'parent_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);

		 DB::table('catal')->insert(
		 ['name'=>'Коляски',
		 'parent_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);
		 DB::table('catal')->insert(
		 ['name'=>'Все для кормления',
		 'parent_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);
		  DB::table('catal')->insert(
		 ['name'=>'Развивающие и обучающие',
		 'parent_id'=>'2',
		 'url'=>'/catalog/toy/develop']);
		  DB::table('catal')->insert(
		 ['name'=>'Музыкальные',
		 'parent_id'=>'2',
		 'url'=>'/catalog/toy/music']);
		  DB::table('catal')->insert(
		 ['name'=>'Куклы',
		 'parent_id'=>'2',
		 'url'=>'/catalog/toy/doll']);
		  DB::table('catal')->insert(
		 ['name'=>'Для девочек',
		 'parent_id'=>'3',
		 'url'=>'/catalog/clothes/girl']);
		  DB::table('catal')->insert(
		 ['name'=>'Для мальчиков',
		 'parent_id'=>'3',
		 'url'=>'/catalog/clothes/boy']);
		  DB::table('catal')->insert(
		 ['name'=>'Спортивная одежда',
		 'parent_id'=>'3',
		 'url'=>'/catalog/clothes/sport']);
		 
      	 DB::table('catal')->insert(
		 ['name'=>'Боди',
		 'parent_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);
 
         DB::table('catal')->insert(
		 ['name'=>'Ползунки',
		 'parent_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);

      	 DB::table('catal')->insert(
		 ['name'=>'Человечки',
		 'parent_id'=>'1',
		 'url'=>'/catalog/Babies_up_to_a_year/clothes']);
    
      	 DB::table('catal')->insert(
		 ['name'=>'Зимние',
		 'parent_id'=>'2',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);

      	 DB::table('catal')->insert(
		 ['name'=>'Летние',
		 'parent_id'=>'2',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);

      	 DB::table('catal')->insert(
		 ['name'=>'Парные',
		 'parent_id'=>'2',
		 'url'=>'/catalog/Babies_up_to_a_year/strollers']);
 
         DB::table('catal')->insert(
		 ['name'=>'Бутылки',
		 'parent_id'=>'3',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);
 
         DB::table('catal')->insert(
		 ['name'=>'Соски',
		 'parent_id'=>'3',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);

         DB::table('catal')->insert(
		 ['name'=>'Посуда',
		 'parent_id'=>'3',
		 'url'=>'/catalog/Babies_up_to_a_year/eat']);

         DB::table('catal')->insert(
		 ['name'=>'Пазлы',
		 'parent_id'=>'4',
		 'url'=>'/catalog/toy/develop']);

         DB::table('catal')->insert(
		 ['name'=>'Конструкторы',
		 'parent_id'=>'4',
		 'url'=>'/catalog/toy/develop']);

         DB::table('catal')->insert(
		 ['name'=>'Мозаика',
		 'parent_id'=>'4',
		 'url'=>'/catalog/toy/develop']);

         DB::table('catal')->insert(
		 ['name'=>'Клавишные инструменты',
		 'parent_id'=>'5',
		 'url'=>'/catalog/toy/music']);

          DB::table('catal')->insert(
		 ['name'=>'Барабаны',
		 'parent_id'=>'5',
		 'url'=>'/catalog/toy/music']);


         DB::table('catal')->insert(
		 ['name'=>'Балалайки',
		 'parent_id'=>'5',
		 'url'=>'/catalog/toy/music']);


         DB::table('catal')->insert(
		 ['name'=>'Baby birn',
		 'parent_id'=>'6',
		 'url'=>'/catalog/toy/doll']);

         DB::table('catal')->insert(
		 ['name'=>'Барби',
		 'parent_id'=>'6',
		 'url'=>'/catalog/toy/doll']);
		 
		 DB::table('catal')->insert(
		 ['name'=>'Семейные пары',
		 'parent_id'=>'6',
		 'url'=>'/catalog/toy/doll']);

          DB::table('catal')->insert(
		 ['name'=>'Платья',
		 'parent_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);
 
         DB::table('catal')->insert(
		 ['name'=>'Юбки',
		 'parent_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);

          DB::table('catal')->insert(
		 ['name'=>'Блузки',
		 'parent_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);
 
         DB::table('catal')->insert(
		 ['name'=>'Сарафаны',
		 'parent_id'=>'7',
		 'url'=>'/catalog/clothes/girl']);
 
         DB::table('catal')->insert(
		 ['name'=>'Брюки',
		 'parent_id'=>'8',
		 'url'=>'/catalog/clothes/boy']);

         DB::table('catal')->insert(
		 ['name'=>'Костюмы',
		 'parent_id'=>'8',
		 'url'=>'/catalog/clothes/boy']);

         DB::table('catal')->insert(
		 ['name'=>'Футболки',
		 'parent_id'=>'8',
		 'url'=>'/catalog/clothes/boy']);

         DB::table('catal')->insert(
		 ['name'=>'Спортивные костюмы',
		 'parent_id'=>'9',
		 'url'=>'/catalog/clothes/sport']);
		 
		 DB::table('catal')->insert(
		 ['name'=>'Спортивные штаны',
		 'parent_id'=>'9',
		 'url'=>'/catalog/clothes/sport']);

//
    }
}