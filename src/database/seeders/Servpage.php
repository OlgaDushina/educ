<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Servpage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		 DB::table('servpage')->insert(
		 ['var'=>'( ТЦ "Адмирал", 1 этаж, детский магазин Kidsanna )',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Показать на карте',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Заказать звонок',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Войти',
		 'sort'=>'0',
		 'status'=>'1',
		]);
 		 DB::table('servpage')->insert(
		 ['var'=>'Ваше имя*',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Выберите тему вопроса',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Техническая поддержка',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Отдел продаж',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Отдел рекламы',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Ваше сообщение*',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Отправить',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Поле обязательно для заполнения',
		 'sort'=>'0',
		 'status'=>'1',
		]);
		 DB::table('servpage')->insert(
		 ['var'=>'Подтвердите пароль',
		 'sort'=>'0',
		 'status'=>'1',
		]);
       //
    }
}
