<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServpageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('servpage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('var',255)->unique();
            $table->integer('sort')->default(0);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });

        Schema::create('servpage_translation', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name',255);
            $table->integer('servpage_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['servpage_id', 'locale']);
            $table->foreign('servpage_id')->references('id')->on('servpage')->onDelete('cascade');
        });
    }
 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servpage');
		Schema::dropIfExists('servpage_translation');
    }
}
