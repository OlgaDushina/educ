<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('sub_category', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name')->unique();
            $table->integer('category_id')->unsigned();
			$table->string('url',255);
			$table->integer('sort')->default(0);
			$table->boolean('status')->index()->default(1);
            $table->timestamps();
	        $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        });
		    Schema::create('sub_category_translation', function (Blueprint $table) {

            $table->increments('id');

            $table->string('namet',255);
            $table->integer('sub_category_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['sub_category_id', 'locale']);
            $table->foreign('sub_category_id')->references('id')->on('sub_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_category');
		Schema::dropIfExists('sub_category_translation');
    }
}
