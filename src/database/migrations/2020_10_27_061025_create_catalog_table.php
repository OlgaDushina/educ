<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name')->unique();
			$table->string('url',255);
			$table->integer('sort')->default(0);
			$table->boolean('status')->index()->default(1);
            $table->timestamps();
        });
		Schema::create('catalog_translation', function (Blueprint $table) {

            $table->increments('id');

            $table->string('namet',255);
            $table->integer('catalog_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['catalog_id', 'locale']);
            $table->foreign('catalog_id')->references('id')->on('catalog')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog');
		Schema::dropIfExists('catalog_translation');
    }
}
