<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuadminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuadmin', function (Blueprint $table) {
            $table->id();
			$table->string('name',75);
			$table->string('url',255)->unique();
            $table->integer('sort')->default(0);
            $table->boolean('status')->default(1);
 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menuadmins');
    }
}
