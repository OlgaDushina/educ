<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('catal', function (Blueprint $table) {
            $table->increments('id');
			$table->string('url',255)->unique();
            $table->integer('parent_id')->default(0);;
			$table->integer('sort')->default(0);
			$table->boolean('status')->index()->default(1);
         });
		Schema::create('catal_translation', function (Blueprint $table) {

            $table->increments('id');

            $table->string('namet',255);
            $table->integer('catal_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['catal_id', 'locale']);
            $table->foreign('catal_id')->references('id')->on('catal')->onDelete('cascade');
        });
		
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catal');
		Schema::dropIfExists('catal_translation');
    }
}
