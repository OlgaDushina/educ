<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void**/
    public function up()
    {
        Schema::create('catalog_products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('artikul');
            $table->string('barcode');
            $table->string('url')->unique();

            $table->decimal('price');
            $table->decimal('price_old');

            $table->integer('position');
            $table->integer('category_id')->index();
            $table->integer('brand_id')->index();
            $table->integer('parent_product_id')->index();

            $table->smallInteger('stock');

            $table->boolean('is_top_sale')->index();
            $table->boolean('is_super_price')->index();
            $table->boolean('is_new')->index();
            $table->char('sex',1)->default('u')->index();
            $table->tinyinteger('age_id')->index();
            $table->boolean('active')->index();

            $table->text('product_description');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_products');
    }
}
