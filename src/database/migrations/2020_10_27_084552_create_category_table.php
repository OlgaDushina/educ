<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name')->unique();
            $table->integer('catalog_id')->unsigned();
			$table->string('url',255);
			$table->integer('sort')->default(0);
			$table->boolean('status')->index()->default(1);
            $table->timestamps();
	        $table->foreign('catalog_id')->references('id')->on('catalog')->onDelete('cascade');
        });
		Schema::create('category_translation', function (Blueprint $table) {

            $table->increments('id');

            $table->string('namet',255);
            $table->integer('category_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['category_id', 'locale']);
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        });
		
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
		Schema::dropIfExists('category_translation');
    }
}
