<?php

namespace App\Widgets\Admin\Header\Desktop;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Menuadmin;

class AdminPanel extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        	    $menu_admin = Menuadmin::where('status',1)->orderBy('sort')->get();
//

        return view('widgets.admin.header.desktop.admin_panel', [
            'config' => $this->config,
			'menu_admin' => $menu_admin,

        ]);
    }
}
