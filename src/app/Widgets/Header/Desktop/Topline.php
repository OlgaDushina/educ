<?php

namespace App\Widgets\Header\Desktop;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Sitemenu;
use Illuminate\Support\Facades\Auth;

class Topline extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
  /*      $menu = [
            [
                'url' => '/',
                'title' => 'Главная'
            ],
            [
                'url' => '/contact',
                'title' => 'Контакты'
            ],
        ];

        return view('widgets.header.desktop.topline', [
            'config' => $this->config,
            'menu' => $menu
        ]);
		*/
	    $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
		//return view('index', ['data'=>News::all()]);
		$user = Auth::user();
		$id_u = Auth::id();
        return view('widgets.header.desktop.topline', [
            'config' => $this->config,
            'menu' => $menu,
			'user'=>$user,
			'id_u'=>$id_u,
			]);

		
    }
}
