<?php

namespace App\Widgets\Header\Desktop;

use Arrilot\Widgets\AbstractWidget;

class Middleline extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.header.desktop.middleline', [
            'config' => $this->config,
        ]);
    }
}
