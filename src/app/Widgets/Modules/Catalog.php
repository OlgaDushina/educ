<?php

namespace App\Widgets\Modules;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Sitemenu;
use App\Models\Catalog;
use App\Models\Category;
use App\Models\Subcategory;

class Catalogc extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
       	$menu = Sitemenu::where('status',1)->orderBy('sort')->get();
		$catalog = Catalogc::where('status',1)->orderBy('sort')->get();
		$category = Category::where('status',1)->orderBy('sort')->get();
		$sub_category = SubCategory::where('status',1)->orderBy('sort')->get();
        return view('widgets.modules.catalog', [
            'config' => $this->config,
			 'menu' => $menu,
			 'catalog'=> $catalog,
			 'category'=> $category,
			 'sub_category'=> $sub_category,
        ]);
 //

    }
}
