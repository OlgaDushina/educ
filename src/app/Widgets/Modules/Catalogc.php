<?php

namespace App\Widgets\Modules;

use App\Models\Sitemenu;
use App\Models\Catalog\Catalog;
use App\Models\Catalog\Category;
use App\Models\Catalog\Subcategory;
use Arrilot\Widgets\AbstractWidget;

class Catalogc extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
  //      $path = explode('-',substr(stristr($_SERVER['REQUEST_URI'],'/f/'),4));
      $path=[];
	  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
		$catalog = Catalog::where('status',1)->orderBy('sort')->get();
		$category = Category::where('status',1)->orderBy('sort')->get();
		$sub_category = SubCategory::where('status',1)->orderBy('sort')->get();
       	   return view('widgets.modules.catalogc', [
            'config' => $this->config,
			 'menu' => $menu,
			 'catalog'=> $catalog,
			 'category'=> $category,
			 'sub_category'=> $sub_category,
			 'path'=>$path,
		 ]);
 //

    }
}
