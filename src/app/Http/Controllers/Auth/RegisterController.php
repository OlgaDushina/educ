<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Laravel\Fortify\Rules\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\PasswordValidationRules;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class RegisterController extends Controller
{
	
	  public function showRegistrationForm()
	{

 		return view('auth.register');
	}
	
 	  public function register(Request $request)
	{
		    $input= $request->only('name','phone','email', 'password','password_confirmation');
		
	        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
			'phone' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' =>['required', 'string', new Password, 'confirmed'],
			])->validate();

      $user= User::create([
            'name' => $input['name'],
			'phone' => $input['phone'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
		]);
	
      return view('auth.login');
	
	}
 
	
 
 //
}
