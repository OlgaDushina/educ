<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
	 public function login()
	 {
		return view('auth.login'); 
	 }
	 
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
			 if(!Auth::user()->isAdmin()){
		           return redirect()->intended('/');
			 }
			 else {
				return redirect('/admin'); 
        }}
    }
}