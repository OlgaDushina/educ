<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Sitemenu;
use App\Models\Servpage;
use Mail;

class Contacts extends Controller
{
	
	  public function index()
	{
  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
		return view('pages.contacts',['menu' => $menu,'servp'=>$servp]);
	}
	
		  public function map()
	{
  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
		return view('pages.map',['menu' => $menu,'servp'=>$servp]);
	}

	public function __construct(Request $request) {
    $this->request = $request;
    }


 /*public function post(Request $request)
	{	
  $data = array($request['name'],$request['message'],$request['department']);	
		return $data;
	}
*/
/*	public function send()
	{
	Mail::send(['text'=>'mail'],['name','olgavnv777'],function($message)
	{
		$message->to('olgavnv777@gmail.com','To olgavnv777')->subject('Text email');
        $message->from('olgavnv777@gmail.com','from olgavnv777');
	});
	}*/
	 public function post(Request $request)
	{		
	$to_name = 'olgavnv777';
    $to_email = 'olgavnv777@gmail.com'; 
	$data = array('name'=>$request->name,'messages'=>$request->message, 'email'=>$request->email, 'department'=>$request->department);
	$from_email=$request->email;
   Mail::send('mail.emails', $data, function($message) use ($to_name, $to_email, $from_email) 
	{
    $message->to($to_email, $to_name)->subject('Artisans Web Testing Mail');
    $message->from($from_email,'Artisans Web');
    });
	$menu = Sitemenu::where('status',1)->orderBy('sort')->get();
    $servp = Servpage::where('status',1)->get();
		return view('pages.contacts_post',['menu' => $menu,'servp'=>$servp]);
//	header('Location: ' . $_SERVER["HTTP_REFERER"] );
//	exit;
	}
	

}


