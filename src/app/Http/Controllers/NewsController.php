<?php
namespace App\Http\Controllers;

use App\Models\News;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(){
		$path=substr(stristr($_SERVER['REQUEST_URI'],'/news/'),6);
		$data=News::get();
		return view('pages.news.'.$path);
    }
	public function news_enter(Request $req){
		$news=new News();
		$news->date=$req->input('date');
		$news->title=$req->input('title');
		$news->news=$req->input('news');
		$news->img=$req->input('img');
		$news->pris=$req->input('pris');
		
		$news->save();
		return redirect()->rout('index');
	}
		
    
}
