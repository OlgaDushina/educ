<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use App\Models\News;
use App\Models\User;

class Home extends Controller
{
    public function index()
	{
		//return view('index', ['data'=>News::all()]);
			return view('index');
	}
	 public function logout_u()
	 {
		 Auth::logout();
		 return view('index');
	 }

}
