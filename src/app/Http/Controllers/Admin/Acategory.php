<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\Catalog;
use App\Models\Catalog\Category;
use App\Models\Catalog\SubCategory;
use App\Models\Catalog\Products;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class Acategory extends Controller
{
    	public function index()
	{
    $catalog=Catalog::all();
	$category=Category::all();
	$sub_category=SubCategory::all();
	return view('admin.catalog',[
		'catalog'=>$catalog,
		'category'=>$category,
		'sub_category'=>$sub_category,
		]);
	}
	   	public function asubcat(Request $request)
	{
	$input=$request->only('id','idc');
    $catalog = Catalog::where('id',$input['idc'])->first();
	$category=Category::where('id',$input['id'])->first();
	$sub_category=Category::find($category['id'])->subcategories()->where('status',1)->orderBy('sort')->get();
	return view('admin.subcategory',[
		'catalog'=>$catalog,
		'category'=>$category,
		'sub_category'=>$sub_category,
		]);
	}
	   	public function acat(Request $request)
	{
		$input=$request->only('id');
    $catalog = Catalog::where('id',$input['id'])->first();
	$category=Catalog::find($catalog['id'])->categories()->where('status',1)->orderBy('sort')->get();
	$sub_category=SubCategory::all();
	return view('admin.category',[
		'catalog'=>$catalog,
		'category'=>$category,
		'sub_category'=>$sub_category,
		]);
	}


		public function add()
	{}
	public function del(Request $request)
	{
		
		
	}
	public function del_s(Request $request)
	{
			$inid= $request->only('id');
			$products=SubCategory::find($inid['id'])->products()->orderBy('position')->get();
			$sub_category=SubCategory::all();
	return view('admin.del_s',[
		'inid'=>$inid,
		'products'=>$products,
		'sub_category'=>$sub_category,
		]);
	}
	public function del_spost(Request $request)
	{
	}
	
	public function edit(Request $request)
	{}
	
	
	//
	
	
}
