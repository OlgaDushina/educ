<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\User;

class UserController extends Controller
{
    	public function index()
	{
	$user=User::all();
  	return view('admin.user',[
		'user'=>$user,
		]);
	}
	
	
			public function searchpost(Request $request)
	{
	$user=User::where('name', 'REGEXP', $request['query'])->get();
   	return view('admin.user',[
		'user'=>$user,
		]);
	}
  		public function delete(Request $request)
	{
		$inid= $request->only('id');
	return view('admin.deleteuser',[
		'inid'=>$inid,
		]);
	}

	  		public function deletepost(Request $request)
	{
		$input= $request->only('id','del');
		if(isset($input['del']) and $input['del']=='1') User::where('id', $input['id'])->delete();
		return redirect()->route('admin.UserController@index');

	}



		public function edit(Request $request)
	{
		$id=substr(stristr($_SERVER['REQUEST_URI'],'?id='),4);
    $user=User::where('id',$id)->first();;
	return view('admin.edituser',[
	'user'=>$user,
		]);
	}

		public function editpost(Request $request)
	{
		    $input= $request->only('id','name','phone','email', 'is_admin');
		
	        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
			'phone' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($input['id'])],
			'is_admin' =>['required','boolean'],
			])->validate();

      $user= User::where('id',$input['id'])->update([
            'name' => $input['name'],
			'phone' => $input['phone'],
            'email' => $input['email'],
			'is_admin' => $input['is_admin'],
		]);
		$user=User::all();
        return redirect()->route('admin.UserController@index');
    }

	
	//
}
