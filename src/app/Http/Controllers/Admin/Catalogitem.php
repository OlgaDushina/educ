<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use QCod\ImageUp\HasImageUploads;

use App\Models\Age;
use App\Models\Catalog\SubCategory;
use App\Models\Catalog\Brand;
use App\Models\Catalog\ProductCount;
use App\Models\Catalog\Products;

class Catalogitem extends Controller
{
	public function index(Products $products)
	{
   	return view('admin.catalogitem',[
		'products'=>$products::paginate(30),
			]);
	}


		public function edit(Request $request,Products $products,$id,$page)
	{   
	  		$products=$products::find($id);
		  if ($request->method() == 'POST') {
            $validator = Validator::make(
                $request->all(),
                $products->validateRules($id)
            );

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator->errors());
            }
			   try {
        if(!isset($request['is_top_sale'])) $request['is_top_sale']='0';
		if(!isset($request['is_super_price'])) $request['is_super_price']='0';
		if(!isset($request['is_new'])) $request['is_new']='0';
		if(!isset($request['is_color'])) $request['is_color']='0';
		if(!isset($request['is_size'])) $request['is_size']='0';
        if(!isset($request['active'])) $request['active']='0';
                $products->fill($request->all());
               if ($request->file('scr')) {
                    $products->uploadImage($request->file('scr'), 'scr');
 				}
                $products->save();
            } catch (Exception $exception) {
                return redirect()
                    ->back()
                    ->withErrors(['system-errors' => $exception->getMessage()]);
            }
			 return redirect('admin/catalogi?page='.$page);
			//return view('admin.cat',['request'=>$request->all(),]);
            }
         else
		 {
		 $brand=Brand::all();
	    $sub_category=SubCategory::all();
	    return view('admin.edititem',[
		'products'=>$products,
		'brand'=>$brand,
		'sub_category'=>$sub_category,
		'page'=>$page,
		]);
		 }
	}


	

		public function add(Request $request,Products $products)
	{
		if ($request->method() == 'POST') {
            $validator = Validator::make(
                $request->all(),
                $products->validateRules('0')
            );

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator->errors());
            }
			   try {
				    $products->fill($request->all());
					$products->url='/catalog/category/sub_category/item/'.$request['name'];
			        $products->stock='0';
			        $products->parent_product_id='0';
					
               if ($request->file('scr')) {
                    $products->uploadImage($request->file('scr'), 'scr');
 				}
				$products->save();
            } catch (Exception $exception) {
                return redirect()
                    ->back()
                    ->withErrors(['system-errors' => $exception->getMessage()]);
            }
			return redirect('admin/catalogi?page='.$products::paginate(30)->lastPage());
            }
         else
		 {
		
	$brand=Brand::all();
	$sub_category=SubCategory::all();
	return view('admin.additem',[
		'brand'=>$brand,
		'sub_category'=>$sub_category,
		]);
	}
	}



   
   
   
   		public function delete(Request $request,$id)
	{
		if ($request->method() == 'POST'){
			if(isset($request['del']) and $request['del']=='1') Products::where('id', $id)->delete();
		return redirect()->route('admin.Catalogitem@index');
		}
		else
	    return view('admin.deleteitem',[
		'id'=>$id,
		]);
	}



			public function searchpost(Request $request)
	{
	$products=Products::where('name', 'REGEXP', $request['query'])->get();
	return view('admin.catalogitem',[
		'products'=>$products,
		]);
	}


}