<?php

namespace App\Http\Controllers\Catalog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sitemenu;
use App\Models\Servpage;
use App\Models\Catalog\Catalog;
use App\Models\Catalog\Category;
use App\Models\Catalog\SubCategory;
use App\Models\Catalog\Brand;
use App\Models\Age;

class Catalogp extends Controller
{
   	  public function index()
	{
		$arr = [];
	        $path = explode('-',substr(stristr($_SERVER['REQUEST_URI'],'/f/'),4));	
  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
  		$catalog = Catalog::where('status',1)->orderBy('sort')->get();
		$catalogp=$catalog->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3])->first(); 
//		$category = Category::where('status',1)->orderBy('sort')->get();
//		$categoryp=$category->where('catalog_id',$catalogp['id'])->all();
        $categoryp=Catalog::find($catalogp['id'])->categories()->where('status',1)->orderBy('sort')->get();
		$sub_category = SubCategory::where('status',1)->orderBy('sort')->get();
		$brand = Brand::where('active',$catalogp['id'])->orderBy('position')->get();
        $age=Age::all();
		return view('pages.catalog-sections',['menu' => $menu,
		'servp'=>$servp,
        'catalogp'=> $catalogp,
		'catalog'=> $catalog,
		'categoryp'=> $categoryp,
		'sub_category'=> $sub_category,
		'brand'=>$brand,
		 'path'=>$path,
		 'age'=>$age,
		'arr'=>$arr, 
		]);
	}
 //
}
