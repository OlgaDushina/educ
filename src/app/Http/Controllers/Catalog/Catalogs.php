<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sitemenu;
use App\Models\Servpage;
use App\Models\Age;
use App\Models\Catalog\Catalog;
use App\Models\Catalog\Category;
use App\Models\Catalog\SubCategory;
use App\Models\Catalog\Brand;
use App\Models\Catalog\ProductCount;
use App\Models\Catalog\Products;


class Catalogs extends Controller
{
     	  public function index()
	{
		$arr = [];
	    //    $path = explode('-',substr(stristr($_SERVER['REQUEST_URI'],'/f/'),4));	
  $path=[];
  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
  		$catalog = Catalog::where('status',1)->orderBy('sort')->get();
		$catalogp=$catalog->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3])->first(); 
//		$category = Category::where('status',1)->orderBy('sort')->get();
//		$categoryp=$category->where('catalog_id',$catalogp['id'])->all();
//      $categoryp=Catalog::find($catalogp['id'])->categories()->where('status',1)->orderBy('sort')->get();
//		$sub_category = SubCategory::where('status',1)->orderBy('sort')->get();
  		$category = Category::where('status',1)->orderBy('sort')->get();
		$categoryp=$category->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3].'/'.explode('/',$_SERVER['REQUEST_URI'])[4])->first(); 
//        $sub_category =Category::find($categoryp['id'])->subcategories()->where('status',1)->orderBy('sort')->get();
        $sub_category=Category::find($categoryp['id'])->subcategories()->where('status',1)->orderBy('sort')->get();
//		$brand = Brand::where('active',$catalogp['id'])->orderBy('position')->get();
        $brand = Brand::all();
//$filtered = $collection->filter(function ($value, $key) {
 //   return $value > 2;
//}); 
// $products=SubCategory::find($sub_category['id'])->products()->orderBy('position')->get();
//		$products=Products::all();
//		$products=$products->filter(function($value,$key){
		$products = collect([]);
        $product_sub= collect([]);
//		$product_count_ar= collect([]);
		foreach($sub_category as $s){
 		$products=$products->merge(SubCategory::find($s['id'])->products()->get());
        $product_sub[$s->id]=SubCategory::find($s['id'])->products()->get();
//		$product_count_ar[$s->id]=Products::find(1)->productcount()->get();
		}
		$age=Age::all();
		$product_count=ProductCount::all();
		$i='0';
		$page='s';
		$select=[];
		return view('pages.catalog-products-with-filter',['menu' => $menu,
		'servp'=>$servp,
        'catalogp'=> $catalogp,
		'catalog'=> $catalog,
		'categoryp'=> $categoryp,
		'sub_category'=> $sub_category,
		'brand'=>$brand,
		'products'=>$products,
		'age'=>$age,
		'product_count'=>$product_count,
		'i'=>$i,
		'page'=>$page,
		'product_sub'=>$product_sub,
//		'product_count_ar'=>$product_count_ar,
		'path'=>$path,
		'arr'=>$arr, 
		'select'=>$select,
		]);
	} //
}
