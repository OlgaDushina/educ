<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Sitemenu;
use App\Models\Servpage;
use App\Models\Age;
use App\Models\Catalog\Catalog;
use App\Models\Catalog\Category;
use App\Models\Catalog\SubCategory;
use App\Models\Catalog\Brand;
use App\Models\Catalog\ProductCount;
use App\Models\Catalog\Products;
use App\Models\Catalog\Props;
use Illuminate\Support\Facades\DB;


class Catalogfiltr extends Controller
{
     	  public function index()
	{
		
//   <a href="{{LaravelLocalization::localizeUrl($sub['url'])}}">{{$sub['namet']}} <span>{{/**count($product_sub[$sub->id])**/}}</span></a>-->
		 
$path =substr(stristr($_SERVER['REQUEST_URI'],'/f/?'),4);
 $path =explode('/f/?',$path);
//$path =substr(stristr($_SERVER['REQUEST_URI'],'/f'),4);
 //    $path =explode('/f/?',$_SERVER['REQUEST_URI']);
		  $arr = [];
       foreach ($path as $pat) {
            $prop = explode('_',$pat);

            $arr[$prop[0]] = $prop[1];
        }
/**		$com='';
				 foreach ($arr as $key=>$value) {
			$pr=Props::where('props',$key)->first();
			$com=$com.str_replace('@1',$value,$pr->command);
		 }
**/		 
$path='';


  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
  		$catalog = Catalog::where('status',1)->orderBy('sort')->get();
		$catalogp=$catalog->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3])->first(); 
//		$category = Category::where('status',1)->orderBy('sort')->get();
//		$categoryp=$category->where('catalog_id',$catalogp['id'])->all();
      $categoryp=Catalog::find($catalogp['id'])->categories()->where('status',1)->orderBy('sort')->get();
//		$sub_category = SubCategory::where('status',1)->orderBy('sort')->get();
 // 		$category = Category::where('status',1)->orderBy('sort')->get();
//		$categoryp=$category->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3].'/'.explode('/',$_SERVER['REQUEST_URI'])[4])->first(); 
//        $sub_category =Category::find($categoryp['id'])->subcategories()->where('status',1)->orderBy('sort')->get();
//		$brand = Brand::where('active',$catalogp['id'])->orderBy('position')->get();
        $brand = Brand::all();
//$filtered = $collection->filter(function ($value, $key) {
 //   return $value > 2;
//}); 
// $products=SubCategory::find($sub_category['id'])->products()->orderBy('position')->get();
//		$products=Products::all();
//		$products=$products->filter(function($value,$key){
		$sub_category= collect([]);
		foreach($categoryp as $c){
 		$sub_category=$sub_category->merge(Category::find($c['id'])->subcategories()->get());
		}

/**		$filter = [
		'color' => [1,24]
		];
		
		$product = Products::where('status',1);
		
		foreach($filter as $param) {
			$product->whereIn($param);
		}
		
		$product = $product->get();
		
**/
		$products = collect([]);
        $product_sub= collect([]);
//		$product_count_ar= collect([]);
		foreach($sub_category as $s){
//		$products=$products->merge(SubCategory::find($s['id'])->products()->whereIN('sex',['b','u'])->get());
 //     $product_sub[$s->id]=SubCategory::find($s['id'])->products()->whereIN('sex',['b','u'])->get();
  //       $products1=$products->merge(SubCategory::find($s['id'])->products()->$com->get());
	   // $products1=&$pr_com;
//		$pr_sub_com='SubCategory::find($s[\'id\'])->products()'.$com.'->get()';
 //		$product_sub1[$s->id]=&$pr_sub_com;
//      $product_count_ar[$s->id]=Products::find(1)->productcount()->get();
 		$products=$products->merge(SubCategory::find($s['id'])->products()->get());
		$product_sub[$s->id]=SubCategory::find($s['id'])->products()->get();
		}
		$select=[];
	foreach ($arr as $key=>$value) {
		switch ($key) {
		case 'sex': $products=$products->whereIN('sex',[$value,'u']); 
		($value=='b')?$select['пол']='мальчик':$select['пол']='девочка';
		break;
		case 'price1' : $products=$products->where('price','>=' ,$value);
		$select['цена']=strval($value);
		break;
		case 'price2' : $products=$products->where('price','<' ,$value);
		$select['цена']=$select['цена'].'-'.strval($value).' грн.';
		break;
		case 'agefrom' : $products=$products->where('ageto','>=' ,floatval($value));
		$select['возраст']=strval($value);
 		break;
		case 'ageto' : $products=$products->where('agefrom','<=' ,floatval($value));
		$select['возраст']=$select['возраст'].'-'.strval($value).' лет';
		break;
		case 'brand' : $products=$products->where('brand_id',$value);
		$b=$brand->where('id',$value)->first();
		$select['ТМ']=$b->name;
		break;
		case 'category' : $products=$products->where('sub_category_id',$value);
		$s=$sub_category->where('id',$value)->first();
		$select['категория']=$s->namet;
		break;
		}
	}
		$age=Age::all();
		$products=$products->all();

		$product_count=ProductCount::all();
		$i='0';
		$page='filtr';
		return view('pages.catalog-products-with-filter',['menu' => $menu,
		'servp'=>$servp,
        'catalogp'=> $catalogp,
		'catalog'=> $catalog,
		'categoryp'=> $categoryp,
		'sub_category'=> $sub_category,
		'brand'=>$brand,
		'products'=>$products,
		'age'=>$age,
		'product_count'=>$product_count,
		'i'=>$i,
		'page'=>$page,
		'product_sub'=>$product_sub,
		'path'=>$path,
        'arr'=>$arr, 
		'select'=>$select,
		]);
	}
    	 
     	  public function index1()
	{
  //    $path = explode('-',substr(stristr($_SERVER['REQUEST_URI'],'/f/?'),4));
  $path =substr(stristr($_SERVER['REQUEST_URI'],'/f/?'),4);
 $path =explode('/f/?',$path);
         $arr = [];
       foreach ($path as $pat) {
            $prop = explode('_',$pat);

            $arr[$prop[0]] = $prop[1];
        }
$path='';


  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
  		$catalog = Catalog::where('status',1)->orderBy('sort')->get();
		$catalogp=$catalog->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3])->first(); 
//		$category = Category::where('status',1)->orderBy('sort')->get();
//		$categoryp=$category->where('catalog_id',$catalogp['id'])->all();
//      $categoryp=Catalog::find($catalogp['id'])->categories()->where('status',1)->orderBy('sort')->get();
//		$sub_category = SubCategory::where('status',1)->orderBy('sort')->get();
 		$category = Category::where('status',1)->orderBy('sort')->get();
		$categoryp=$category->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3].'/'.explode('/',$_SERVER['REQUEST_URI'])[4])->first(); 
        $sub_category =Category::find($categoryp['id'])->subcategories()->where('status',1)->orderBy('sort')->get();
//		$brand = Brand::where('active',$catalogp['id'])->orderBy('position')->get();
        $brand = Brand::all();
//$filtered = $collection->filter(function ($value, $key) {
 //   return $value > 2;
//}); 
// $products=SubCategory::find($sub_category['id'])->products()->orderBy('position')->get();
//		$products=Products::all();
//		$products=$products->filter(function($value,$key){
/**		$filter = [
		'color' => [1,24]
		];
		
		$product = Products::where('status',1);
		
		foreach($filter as $param) {
			$product->whereIn($param);
		}
		
		$product = $product->get();
		
**/
		$products = collect([]);
        $product_sub= collect([]);
//		$product_count_ar= collect([]);
		foreach($sub_category as $s){
//		$products=$products->merge(SubCategory::find($s['id'])->products()->whereIN('sex',['b','u'])->get());
 //     $product_sub[$s->id]=SubCategory::find($s['id'])->products()->whereIN('sex',['b','u'])->get();
  //       $products1=$products->merge(SubCategory::find($s['id'])->products()->$com->get());
	   // $products1=&$pr_com;
//		$pr_sub_com='SubCategory::find($s[\'id\'])->products()'.$com.'->get()';
 //		$product_sub1[$s->id]=&$pr_sub_com;
//      $product_count_ar[$s->id]=Products::find(1)->productcount()->get();
 		$products=$products->merge(SubCategory::find($s['id'])->products()->get());
		$product_sub[$s->id]=SubCategory::find($s['id'])->products()->get();
		}
	    $select=[];
	    foreach ($arr as $key=>$value) {
		switch ($key) {
		case 'sex': $products=$products->whereIN('sex',[$value,'u']); 
		($value=='b')?$select['пол']='мальчик':$select['пол']='девочка';
		break;
		case 'price1' : $products=$products->where('price','>=' ,$value);
		$select['цена']=strval($value);
		break;
		case 'price2' : $products=$products->where('price','<' ,$value);
		$select['цена']=$select['цена'].'-'.strval($value).' грн.';
		break;
		case 'agefrom' : $products=$products->where('ageto','>=' ,floatval($value));
		$select['возраст']=strval($value);
 		break;
		case 'ageto' : $products=$products->where('agefrom','<=' ,floatval($value));
		$select['возраст']=$select['возраст'].'-'.strval($value).' лет';
		break;
		case 'brand' : $products=$products->where('brand_id',$value);
		$b=$brand->where('id',$value)->first();
		$select['ТМ']=$b->name;
		break;
		case 'category' : $products=$products->where('sub_category_id',$value);
		$s=$sub_category->where('id',$value)->first();
		$select['категория']=$s->namet;
		break;

		}
	}
		$age=Age::all();
		$products=$products->all();

		$product_count=ProductCount::all();
		$i='0';
		$page='filtr';
		return view('pages.catalog-products-with-filter',['menu' => $menu,
		'servp'=>$servp,
        'catalogp'=> $catalogp,
		'catalog'=> $catalog,
		'categoryp'=> $categoryp,
		'sub_category'=> $sub_category,
		'brand'=>$brand,
		'products'=>$products,
		'age'=>$age,
		'product_count'=>$product_count,
		'i'=>$i,
		'page'=>$page,
		'product_sub'=>$product_sub,
		'path'=>$path,
        'arr'=>$arr,
         'select'=>$select,		
		]);
	}


		 public function index2()
	{
		
 //     $path = explode('-',substr(stristr($_SERVER['REQUEST_URI'],'/f/?'),4));
       $path =substr(stristr($_SERVER['REQUEST_URI'],'/f/?'),4);
       $path =explode('/f/?',$path);
        $arr = [];
       foreach ($path as $pat) {
            $prop = explode('_',$pat);

            $arr[$prop[0]] = $prop[1];
        }
$path='';


  $menu = Sitemenu::where('status',1)->orderBy('sort')->get();
  $servp = Servpage::where('status',1)->get();
  		$catalog = Catalog::where('status',1)->orderBy('sort')->get();
		$catalogp=$catalog->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3])->first(); 
  		$category = Category::where('status',1)->orderBy('sort')->get();
		$categoryp=$category->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3].'/'.explode('/',$_SERVER['REQUEST_URI'])[4])->first(); 
 //       $sub_category =Category::find($categoryp['id'])->subcategories()->where('status',1)->orderBy('sort')->get();
        $sub_category=SubCategory::where('status',1)->orderBy('sort')->get()->where('url','/catalog/'.explode('/',$_SERVER['REQUEST_URI'])[3].'/'.explode('/',$_SERVER['REQUEST_URI'])[4].'/'.explode('/',$_SERVER['REQUEST_URI'])[5])->first();
		$brand = Brand::where('active',$catalogp['id'])->orderBy('position')->get();
//       $brand = Brand::all();

//		$products = collect([]);
 //       $product_sub= collect([]);
//		$product_count_ar= collect([]);
 		$products=SubCategory::find($sub_category['id'])->products()->get();
		$product_sub=SubCategory::find($sub_category['id'])->products()->get();
        $select=[];
	    foreach ($arr as $key=>$value) {
		switch ($key) {
		case 'sex': $products=$products->whereIN('sex',[$value,'u']); 
		($value=='b')?$select['пол']='мальчик':$select['пол']='девочка';
		break;
		case 'price1' : $products=$products->where('price','>=' ,$value);
		$select['цена']=strval($value);
		break;
		case 'price2' : $products=$products->where('price','<' ,$value);
		$select['цена']=$select['цена'].'-'.strval($value).' грн.';
		break;
		case 'agefrom' : $products=$products->where('ageto','>=' ,floatval($value));
		$select['возраст']=strval($value);
 		break;
		case 'ageto' : $products=$products->where('agefrom','<=' ,floatval($value));
		$select['возраст']=$select['возраст'].'-'.strval($value).' лет';
		break;
		case 'brand' : $products=$products->where('brand_id',$value);
		$b=$brand->where('id',$value)->first();
		$select['ТМ']=$b->name;
		break;
		case 'category' : $products=$products->where('sub_category_id',$value);
		$select['категория']=$sub_category->namet;
		break;
	}
		$age=Age::all();
		$products=$products->all();

		$product_count=ProductCount::all();
		$i='0';
		$page='f';
		return view('pages.catalog-products-with-filter',['menu' => $menu,
		'servp'=>$servp,
        'catalogp'=> $catalogp,
		'catalog'=> $catalog,
		'categoryp'=> $categoryp,
		'sub_category'=> $sub_category,
		'brand'=>$brand,
		'products'=>$products,
		'age'=>$age,
		'product_count'=>$product_count,
		'i'=>$i,
		'page'=>$page,
		'product_sub'=>$product_sub,
		'path'=>$path,
	    'arr'=>$arr,
		'select'=>$select,
		]);
	}

	//
   }
}
