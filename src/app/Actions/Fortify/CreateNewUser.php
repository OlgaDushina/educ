<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
/**		$messages = [
    'required' => 'Поле должно быть заполнено!!!',
	'confirmed'=>'Подтверждение пароля не соответствует паролю!!!!',
	'unique:users'=>'Такое имя уже используется!!!',
	'The password must be at least 8 characters.'=>'стопудово',
	'min:8'=>'Пароль должен быть не меньше 8 символов',
    ];**/
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
			'phone' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        return User::create([
            'name' => $input['name'],
			'phone' => $input['phone'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
    }
}
