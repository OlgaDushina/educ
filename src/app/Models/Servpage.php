<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Servpage extends Model implements TranslatableContract
{
    use HasFactory;
	use Translatable;

    protected $table = 'servpage';

    protected $fillable = [
        'var', 'sort', 'status',
    ];

    public $translatedAttributes = ['name'];


}
