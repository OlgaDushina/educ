<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class Sitemenu extends Model implements TranslatableContract
{
    use HasFactory;
	
	use Translatable;

    protected $table = 'sitemenu';

    protected $fillable = [
        'url', 'sort', 'status', 'category','type',
    ];

    public $translatedAttributes = ['name'];


}
