<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class SubCategory extends Model implements TranslatableContract
{
    use HasFactory;
	use Translatable;
	
	protected $table = 'sub_category';

    protected $fillable = [
        'name', 'category_id', 'url', 'sort', 'status',
    ];

    public $translatedAttributes = ['namet'];
		
	   public function categories()
    {
        return $this->belongsTo('App\Models\Catalog\Category','foreign_key');
    }
 
     public function products()
    {
        return $this->hasMany('App\Models\Catalog\Products','sub_category_id');
    }



}
