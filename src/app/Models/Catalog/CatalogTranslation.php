<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatalogTranslation extends Model
{
    use HasFactory;
	protected $table = 'catalog_translation';

    public $timestamps = false;
    protected $fillable = ['namet'];
}
