<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    use HasFactory;
	protected $table = 'category_translation';

    public $timestamps = false;
    protected $fillable = ['namet'];
}
