<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Props extends Model
{
    use HasFactory;
		protected $table = 'props';
    protected $fillable = [
       'props', 'type', 'command',
    ];
   public function offers($props)
    {
        return $this->command;
	}

}
