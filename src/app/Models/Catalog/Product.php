<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
	
	 public function validateRules(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
			'artikul' => ['required'],
            'price' => ['required','numeric'],
			'price_old' => ['required','numeric'],
			'agefrom' => ['required','numeric'],
			'ageto' => ['required','numeric'],
			'position' => ['required'],
			'barcode' => ['required',Rule::unique('catalog_products')->ignore($request['id'])],
            'brand_id' => ['required'],
			'sub_category_id' => ['required'],
        ];
        return $rules;
    }

}
