<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Catal extends Model implements TranslatableContract
{
    use HasFactory;
	use Translatable;
	protected $table = 'catal';
	
    protected $fillable = [
       'parent_id', 'url', 'sort', 'status',
    ];

    public $translatedAttributes = ['namet'];
	
 
}

