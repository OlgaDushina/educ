<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatalogColor extends Model
{
    use HasFactory;
	protected $table = 'catalog_colors';
    protected $fillable = [
       'name','code',
    ];

}
