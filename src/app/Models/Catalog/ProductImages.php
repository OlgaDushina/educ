<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    use HasFactory;
	protected $table = 'catalog_products_images';

    protected $fillable = [
        'src', 'is_main', 'position'
    ];

}
