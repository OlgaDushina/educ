<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCount extends Model
{
    use HasFactory;
			protected $table = 'product_counts';
    protected $fillable = [
       'product_id','size','color','count',
    ];
	   public function products()
    {
        return $this->belongsTo('App\Models\Catalog\Products','foreign_key');
    }

}
