<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Category extends Model implements TranslatableContract
{
    use HasFactory;
	use Translatable;
	protected $table = 'category';
	
    protected $fillable = [
        'name', 'catalog_id', 'url', 'sort', 'status',
    ];

    public $translatedAttributes = ['namet'];
	
	   public function catalogs()
    {
        return $this->belongsTo('App\Models\Catalog\Catalog','foreign_key');
    }
 
     public function subcategories()
    {
        return $this->hasMany('App\Models\Catalog\SubCategory','category_id');
    }
 
}


