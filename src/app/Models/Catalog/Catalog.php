<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class Catalog extends Model implements TranslatableContract
{
    use HasFactory;
	use Translatable;
	protected $table = 'catalog';
    protected $fillable = [
       'name','url', 'sort', 'status',
    ];

   public $translatedAttributes = ['namet'];
 
   public function categories()
    {
        return $this->hasMany('App\Models\Catalog\Category','catalog_id');
    }
 
      public function brands()
    {
        return $this->belongstoMany('App\Models\Catalog\Brand');
    }
 
   
}
