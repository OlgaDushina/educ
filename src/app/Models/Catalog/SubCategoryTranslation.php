<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategoryTranslation extends Model
{
    use HasFactory;
	protected $table = 'sub_category_translation';

    public $timestamps = false;
    protected $fillable = ['namet'];
}
