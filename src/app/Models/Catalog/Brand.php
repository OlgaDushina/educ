<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
	   protected $table = 'catalog_brands';
    protected $fillable = [
        'name', 'logo' , 'url',
        'position', 'active',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Catalog\Products');
    }

      public function catalogs()
    {
        return $this->belongstoMany('App\Models\Catalog\Catalog');
    }
 

}
