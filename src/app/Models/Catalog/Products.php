<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use QCod\ImageUp\HasImageUploads;


class Products extends Model
{
	 use HasImageUploads;
    protected $table = 'catalog_products';
    protected $fillable = [
        'name', 'artikul', 'barcode','url', 'price', 'price_old',
        'position', 'sub_category_id','scr', 'brand_id','parent_product_id', 'stock',
        'is_top_sale', 'is_super_price', 'is_new','is_color','is_size','age_id','sex','active', 'product_description','agefrom','ageto'
    ];
	
	protected static $imageFields = [
        'scr' => [
            // width to resize image after upload
            'width' => 280,

            // height to resize image after upload
            'height' => 280,

            // set true to crop image with the given width/height and you can also pass arr [x,y] coordinate for crop.
            'crop' => false,

            // what disk you want to upload, default config('imageup.upload_disk')
            'disk' => 'public',

            // a folder path on the above disk, default config('imageup.upload_directory')
            'path' => 'upload/catalog/products',

            // placeholder image if image field is empty
            'placeholder' => '/img/img/catalog/products/avatar-placeholder.jpg',

            // validation rules when uploading image
            'rules' => 'image|max:2000',

            // override global auto upload setting coming from config('imageup.auto_upload_images')
            'auto_upload' => true,

            // if request file is don't have same name, default will be the field name
            'file_input' => 'scr',
        ]
        ];
		
		
    protected function scrUploadFilePath($file) {
        return $this->barcode.'.jpg';
    }
	 public function validateRules($id)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255',Rule::unique('catalog_products')->ignore($id)],
			'artikul' => ['required'],
            'price' => ['required','numeric'],
			'price_old' => ['required','numeric'],
			'agefrom' => ['required','numeric'],
			'ageto' => ['required','numeric'],
			'position' => ['required'],
			'barcode' => ['required',Rule::unique('catalog_products')->ignore($id)],
            'brand_id' => ['required'],
			'sub_category_id' => ['required'],
        ];
        return $rules;
    }
	   public function subcategories()
    {
        return $this->belongsTo('App\Models\Catalog\SubCategory','foreign_key');
    }
     public function age()
    {
        return $this->belongsTo('App\Models\Age');
    }

 
    public function images()
    {
        return $this->hasMany('App\Models\Catalog\ProductImages','product_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Catalog\Brand');
    }

    public function offers()
    {
        return $this->hasMany('App\Models\Catalog\ProductsOffers','product_id');
	}
	public function productcount()
    {
        return $this->hasMany('App\Models\Catalog\ProductCount','product_id');
    }

 }
