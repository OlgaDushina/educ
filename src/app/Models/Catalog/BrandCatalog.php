<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class BrandCatalog extends Pivot
{
    use HasFactory;
	public $incrementing = true;
	protected $table = 'brandcatalogs';
    protected $fillable = [
       'id_catalog','id_brand',
    ];

}
