<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServpageTranslation extends Model
{
    use HasFactory;
	protected $table = 'servpage_translation';

    public $timestamps = false;
    protected $fillable = ['name'];
}
