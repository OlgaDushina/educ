<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SitemenuTranslation extends Model {

    protected $table = 'sitemenu_translation';

    public $timestamps = false;
    protected $fillable = ['name'];
}
