<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <!-- with view items -->
                        <h1>Вы просматривали</h1>
                        <div class="area-catalog-wrap">
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                        </div>
                        <!-- without view items -->
                        <h1>Вы еще ничего не просматривали</h1>
                        <div class="area-grey">
                            <div class="area-grey__title">Обратите внимание на наши акции и топ продаж</div>
                            <a href="javascript:void(0)" class="right-arrow">Перейти в каталог</a>
                            <a href="javascript:void(0)" class="right-arrow">Смотреть акции</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>