<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Обувь</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Сапоги</span>
                        </a>
                        <meta itemprop="position" content="3">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Ботинки Superfit Blue Shark</span>
                        </a>
                        <meta itemprop="position" content="4">
                    </li>
                </ul>
                <h1>Ботинки Superfit Blue Shark</h1>
                <img src="img/temp/product-logo.jpg" alt="" class="product-logo">
                <div class="product-page-wrap">
                    <div class="column">
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper gallery">
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <span class="item-type">
                                <span class="item-type__price">Суперцена</span>
                                <span class="item-type__new">Новинка</span>
                        </span>
                        </div>
                        <div class="top-slider-wrap">
                            <div class="swiper-container top-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        @@include('page-fragments/catalog-item-big.html')
                                    </div>
                                    <div class="swiper-slide">
                                        @@include('page-fragments/catalog-item-big.html')
                                    </div>
                                    <div class="swiper-slide">
                                        @@include('page-fragments/catalog-item-big.html')
                                    </div>
                                    <div class="swiper-slide">
                                        @@include('page-fragments/catalog-item-big.html')
                                    </div>
                                    <div class="swiper-slide">
                                        @@include('page-fragments/catalog-item-big.html')
                                    </div>
                                    <div class="swiper-slide">
                                        @@include('page-fragments/catalog-item-big.html')
                                    </div>
                                </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="_f _j-between _mb-30 _i-center">
                            <div class="product-status">Вналичии</div>
                            <div class="vendor-code">Артикул: 12633</div>
                        </div>
                        <div class="product-price">
                            <div class="old"><span>42 999</span> грн.</div>
                            <div class="new"><span>42 999</span> грн.</div>
                        </div>
                        <div class="product-params">
                            <div class="product-params__column">
                                <div class="params-item__title">Цвет:</div>
                                <div class="_f _f-wrap">
                                    <label class="color-item">
                                        <input type="radio" name="color" value="#5099B9">
                                        <span class="color" style="background-color: #5099B9"></span>
                                    </label>
                                    <label class="color-item">
                                        <input type="radio" name="color" value="#233342">
                                        <span class="color" style="background-color: #233342"></span>
                                    </label>
                                    <label class="color-item">
                                        <input type="radio" name="color" value="#DD292E">
                                        <span class="color" style="background-color: #DD292E"></span>
                                    </label>
                                    <label class="color-item">
                                        <input type="radio" name="color" value="#5099B9">
                                        <span class="color" style="background-color: #5099B9"></span>
                                    </label>
                                    <label class="color-item">
                                        <input type="radio" name="color" value="#233342">
                                        <span class="color" style="background-color: #233342"></span>
                                    </label>
                                    <label class="color-item">
                                        <input type="radio" name="color" value="#DD292E">
                                        <span class="color" style="background-color: #DD292E"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="product-params__column">
                                <div class="params-item__title">Размер:</div>
                                <a href="javascript:void(0)" class="size-info-link">Таблица размеров</a>
                                <div class="_f _f-wrap">
                                    <label class="size-item">
                                        <input type="radio" name="size" value="26">
                                        <span class="size">26</span>
                                    </label>
                                    <label class="size-item">
                                        <input type="radio" name="size" value="27">
                                        <span class="size">27</span>
                                    </label>
                                    <label class="size-item">
                                        <input type="radio" name="size" value="28">
                                        <span class="size">28</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="buttons-wrap">
                            <a href="javascript:void(0)" class="button buy red _upper">Купить</a>
                            <a href="javascript:void(0)" class="button to-favorites _upper">Добавить в избранное</a>
                        </div>
                        <div class="product-accordeon-wrap">
                            <accordeon-item
                                    v-cloak
                                    title="Характеристики"
                            >
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                            </accordeon-item>
                            <accordeon-item
                                    v-cloak
                                    title="Описание товара"
                            >
                                <p>Вариант для тех, кто хочет быть модной и везде успевать. Просто выбирай свой цвет, который идеально подойдет к лицу, волосам и глазам. Надевай комплект и мотивируй других влюбиться навсегда в «спорт стайл» — свободный, удобный и очень стильный. Еще лучше заказать сразу несколько цветных костюмов. Их можно сочетать между собой и получать новые яркие комбинации.</p>
                            </accordeon-item>
                            <accordeon-item
                                    v-cloak
                                    title="Оплата и доставка"
                            >
                                <div class="accordeon-content-title">Доставка:</div>
                                <div class="product-delivery-wrap">
                                    <img src="img/img/new-post.svg" alt="">
                                    <img src="img/img/ukr-post.svg" alt="">
                                </div>
                                <div class="accordeon-content-title">Оплата:</div>
                                <div class="payment-stages-wrap">
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/sprite.svg#payment-1" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                <span>
                                        При получении
                                </span>
                                            <div class="hover-info">
                                                <div class="hover-info__trigger">?</div>
                                                <div class="hover-info__text">
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/sprite.svg#payment-2" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                            Банковской картой
                                        </div>
                                    </div>
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/sprite.svg#payment-3" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                            Безналичный расчет
                                            <div class="hover-info">
                                                <div class="hover-info__trigger">?</div>
                                                <div class="hover-info__text">
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/img/liqpay.svg" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                            Картой на сайте через LIQPAY
                                            <div class="hover-info">
                                                <div class="hover-info__trigger">?</div>
                                                <div class="hover-info__text">
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </accordeon-item>
                        </div>
                    </div>
                </div>
                <div class="h1">Похожие товары</div>
                <div class="swiper-container catalog-slider slider-arrows-top">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                        <div class="swiper-slide">
                            @@include('page-fragments/catalog-item-short.html')
                        </div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>


            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>