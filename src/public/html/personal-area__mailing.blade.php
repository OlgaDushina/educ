<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <h1>Управление рассылками</h1>
                        <div class="area-grey mailing">
                            <div class="area-grey__title">Рассылка на e-mail</div>
                            <div class="mail">berezina_a@gmail.com</div>
                            <label class="big-switch">
                                <input type="checkbox">
                                <div class="switch">
                                    <div class="on">вкл.</div>
                                    <div class="off">выкл.</div>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>