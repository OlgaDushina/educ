<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Контакты</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="contacts-wrap">
                    <div class="contacts-wrap__info">
                        <h1>Контакты</h1>
                        <div class="contact-item big">Херсон, пр. Ушакова, 35-А</div>
                        <div class="contact-item">( ТЦ "Адмирал", 1 этаж, детский магазин Kidsanna )</div>
                        <a href="javascript:void(0)" class="right-arrow">Показать на карте</a>
                        <a href="mailto:kidsanna.com@gmail.com" class="contact-item big">kidsanna.com@gmail.com</a>
                        @@include('page-fragments/phone-box-contacts.html')
                        @@include('page-fragments/social.html')
                    </div>
                    <div class="contacts-wrap__form">
                        <form action="/" class="contacts-form">
                            <div class="contacts-form-wrap">
                                <div class="column">
                                    <label class="validation-field" :class="{ hasvalue: contact.name, error: $v.contact.name.$error  }">
                                        <input v-model="$v.contact.name.$model" placeholder="Ваше имя*">
                                        <div class="error" v-if="!$v.contact.name.required && $v.contact.name.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                    <label class="validation-field" :class="{ hasvalue: contact.email, error: $v.contact.email.$error }">
                                        <input v-model="$v.contact.email.$model" placeholder="Email*">
                                        <div class="error" v-if="!$v.contact.email.email && $v.contact.email.$dirty ">Введите корректный e-mail</div>
                                        <div class="error" v-if="!$v.contact.email.required && $v.contact.email.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                    <div class="validation-field select-wrapper">
                                        <v-select
                                                v-model="contact.theme"
                                                placeholder="Выберите тему вопроса"
                                                :options="
                                        [
                                            'Техническая поддержка',
                                            'Отдел продаж',
                                            'Отдел рекламы'
                                        ]" >
                                        </v-select>
                                    </div>
                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                        Отправить
                                        <input type="submit" :disabled="$v.$anyError">
                                    </label>
                                </div>
                                <div class="column">
                                    <label class="validation-field" :class="{ hasvalue: contact.message, error: $v.contact.message.$error }">
                                        <textarea v-model="$v.contact.message.$model" placeholder="Ваше сообщение*" ></textarea>
                                        <div class="error" v-if="!$v.contact.message.required && $v.contact.message.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>