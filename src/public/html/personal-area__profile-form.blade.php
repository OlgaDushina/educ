<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <div class="profile-wrap">
                            <div class="form-box">
                                <form action="/">
                                    <div class="form-title">Личные данные</div>
                                    <label class="validation-field" :class="{ hasvalue: profile.name, error: $v.profile.name.$error  }">
                                        <input v-model="$v.profile.name.$model" placeholder="Ваше имя*">
                                        <div class="error" v-if="!$v.profile.name.required && $v.profile.name.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                    <label class="validation-field" :class="{ hasvalue: profile.phone }">
                                        <div>
                                            <the-mask mask="+38 (###) ### ##-##" v-model="profile.phone" placeholder="Телефон"/>
                                        </div>
                                    </label>
                                    <label class="validation-field" :class="{ hasvalue: profile.email, error: $v.profile.email.$error }">
                                        <input v-model="$v.profile.email.$model" placeholder="Email*">
                                        <div class="error" v-if="!$v.profile.email.email && $v.profile.email.$dirty ">Введите корректный e-mail</div>
                                        <div class="error" v-if="!$v.profile.email.required && $v.profile.email.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                    <div class="validation-field select-wrapper">
                                        <v-select
                                                v-model="profile.address"
                                                placeholder="Выберите ваш адрес"
                                                :options="
                                        [
                                            'Херсон (Херсонская обл.)',
                                            'Одесса (Одесская обл.)',
                                            'Херсон2 (Херсонская2 обл.)'
                                        ]" >
                                        </v-select>
                                    </div>
                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                        Сохранить изменения
                                        <input type="submit" :disabled="$v.$anyError">
                                    </label>
                                </form>
                                <div class="add-delivery-methods">
                                    <div class="add-delivery-methods__item">
                                        <div class="add-delivery-methods__title">Курьером по адресу</div>
                                        <form-container
                                                v-cloak
                                                trigger="Добавить город">
                                            <form action="/">
                                                <div class="validation-field select-wrapper">
                                                    <v-select
                                                            v-model="delivery.city"
                                                            placeholder="Выберите ваш город"
                                                            :options="
                                                        [
                                                            'Херсон',
                                                            'Одесса',
                                                            'Херсон2'
                                                        ]" >
                                                    </v-select>
                                                </div>
                                                <div class="line-field">
                                                    <div class="validation-field select-wrapper">
                                                        <v-select
                                                                v-model="delivery.address"
                                                                placeholder="Выберите ваш адрес"
                                                                :options="
                                                        [
                                                            'Херсон (Херсонская обл.)',
                                                            'Одесса (Одесская обл.)',
                                                            'Херсон2 (Херсонская2 обл.)'
                                                        ]" >
                                                        </v-select>
                                                    </div>
                                                    <label class="validation-field" :class="{ hasvalue: delivery.house, error: $v.delivery.house.$error  }">
                                                        <input v-model="$v.delivery.house.$model" placeholder="Дом">
                                                        <div class="error" v-if="!$v.delivery.house.required && $v.delivery.house.$dirty">Поле обязательно для заполнения</div>
                                                    </label>
                                                    <label class="validation-field" :class="{ hasvalue: delivery.flat, error: $v.delivery.flat.$error  }">
                                                        <input v-model="$v.delivery.flat.$model" placeholder="Кв. №">
                                                        <div class="error" v-if="!$v.delivery.flat.required && $v.delivery.flat.$dirty">Поле обязательно для заполнения</div>
                                                    </label>
                                                </div>
                                                <div class="recipient-wrap">
                                                    <label class="validation-field">
                                                        <input type="radio" @change="recipient($event.target.value)" name="recipient_1" value="self" v-model="delivery.recipient">
                                                        <span class="radio"></span>
                                                        <span class="input-title">Я получатель</span>
                                                    </label>
                                                    <label class="validation-field">
                                                        <input type="radio" @change="recipient($event.target.value)" name="recipient_1" value="another" v-model="delivery.recipient">
                                                        <span class="radio"></span>
                                                        <span class="input-title">Другой получатель</span>
                                                    </label>
                                                </div>
                                                <label class="validation-field" :class="{ hasvalue: delivery.name, error: $v.delivery.name.$error  }">
                                                    <input v-model="$v.delivery.name.$model" placeholder="Ваше имя*">
                                                    <div class="error" v-if="!$v.delivery.name.required && $v.delivery.name.$dirty">Поле обязательно для заполнения</div>
                                                </label>
                                                <label class="validation-field" :class="{ hasvalue: delivery.phone }">
                                                    <div>
                                                        <the-mask mask="+38 (###) ### ##-##" v-model="delivery.phone" placeholder="Телефон"/>
                                                    </div>
                                                </label>
                                                <label class="validation-field" :class="{ hasvalue: delivery.email, error: $v.delivery.email.$error }">
                                                    <input v-model="$v.delivery.email.$model" placeholder="Email*">
                                                    <div class="error" v-if="!$v.delivery.email.email && $v.delivery.email.$dirty ">Введите корректный e-mail</div>
                                                    <div class="error" v-if="!$v.delivery.email.required && $v.delivery.email.$dirty">Поле обязательно для заполнения</div>
                                                </label>
                                                <label class="validation-field check-wrapper">
                                                    <input type="checkbox" name="accept" checked>
                                                    <span class="check"></span>
                                                    <span class="input-title">Использовать в качестве моего адреса доставки по умолчанию</span>
                                                </label>
                                                <div class="delivery-submit-wrap">
                                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                                        Сохранить изменения
                                                        <input type="submit" :disabled="$v.$anyError">
                                                    </label>
                                                    <label class="reset" @click="reset">
                                                        Отменить
                                                    </label>
                                                </div>

                                            </form>
                                        </form-container>
                                    </div>
                                    <div class="add-delivery-methods__item">
                                        <div class="add-delivery-methods__title">Новой почтой</div>
                                        <form-container
                                                v-cloak
                                                trigger="Добавить адрес">
                                            <form action="/">
                                                <div class="validation-field select-wrapper">
                                                    <v-select
                                                            v-model="delivery.city"
                                                            placeholder="Выберите ваш город"
                                                            :options="
                                                        [
                                                            'Херсон',
                                                            'Одесса',
                                                            'Херсон2'
                                                        ]" >
                                                    </v-select>
                                                </div>
                                                <div class="line-field">
                                                    <div class="validation-field select-wrapper">
                                                        <v-select
                                                                v-model="delivery.address"
                                                                placeholder="Выберите ваш адрес"
                                                                :options="
                                                        [
                                                            'Херсон (Херсонская обл.)',
                                                            'Одесса (Одесская обл.)',
                                                            'Херсон2 (Херсонская2 обл.)'
                                                        ]" >
                                                        </v-select>
                                                    </div>
                                                    <label class="validation-field" :class="{ hasvalue: delivery.house, error: $v.delivery.house.$error  }">
                                                        <input v-model="$v.delivery.house.$model" placeholder="Дом">
                                                        <div class="error" v-if="!$v.delivery.house.required && $v.delivery.house.$dirty">Поле обязательно для заполнения</div>
                                                    </label>
                                                    <label class="validation-field" :class="{ hasvalue: delivery.flat, error: $v.delivery.flat.$error  }">
                                                        <input v-model="$v.delivery.flat.$model" placeholder="Кв. №">
                                                        <div class="error" v-if="!$v.delivery.flat.required && $v.delivery.flat.$dirty">Поле обязательно для заполнения</div>
                                                    </label>
                                                </div>
                                                <div class="recipient-wrap">
                                                    <label class="validation-field">
                                                        <input type="radio" @change="recipient($event.target.value)" name="recipient_1" value="self" v-model="delivery.recipient">
                                                        <span class="radio"></span>
                                                        <span class="input-title">Я получатель</span>
                                                    </label>
                                                    <label class="validation-field">
                                                        <input type="radio" @change="recipient($event.target.value)" name="recipient_1" value="another" v-model="delivery.recipient">
                                                        <span class="radio"></span>
                                                        <span class="input-title">Другой получатель</span>
                                                    </label>
                                                </div>
                                                <label class="validation-field" :class="{ hasvalue: delivery.name, error: $v.delivery.name.$error  }">
                                                    <input v-model="$v.delivery.name.$model" placeholder="Ваше имя*">
                                                    <div class="error" v-if="!$v.delivery.name.required && $v.delivery.name.$dirty">Поле обязательно для заполнения</div>
                                                </label>
                                                <label class="validation-field" :class="{ hasvalue: delivery.phone }">
                                                    <div>
                                                        <the-mask mask="+38 (###) ### ##-##" v-model="delivery.phone" placeholder="Телефон"/>
                                                    </div>
                                                </label>
                                                <label class="validation-field" :class="{ hasvalue: delivery.email, error: $v.delivery.email.$error }">
                                                    <input v-model="$v.delivery.email.$model" placeholder="Email*">
                                                    <div class="error" v-if="!$v.delivery.email.email && $v.delivery.email.$dirty ">Введите корректный e-mail</div>
                                                    <div class="error" v-if="!$v.delivery.email.required && $v.delivery.email.$dirty">Поле обязательно для заполнения</div>
                                                </label>
                                                <label class="validation-field check-wrapper">
                                                    <input type="checkbox" name="accept" checked>
                                                    <span class="check"></span>
                                                    <span class="input-title">Использовать в качестве моего адреса доставки по умолчанию</span>
                                                </label>
                                                <div class="delivery-submit-wrap">
                                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                                        Сохранить изменения
                                                        <input type="submit" :disabled="$v.$anyError">
                                                    </label>
                                                    <label class="reset" @click="reset">
                                                        Отменить
                                                    </label>
                                                </div>

                                            </form>
                                        </form-container>
                                    </div>
                                    <div class="add-delivery-methods__item">
                                        <div class="add-delivery-methods__title">Укрпочтой</div>
                                        <form-container
                                                v-cloak
                                                trigger="Добавить адрес">
                                            <form action="/">
                                                <div class="validation-field select-wrapper">
                                                    <v-select
                                                            v-model="delivery.city"
                                                            placeholder="Выберите ваш город"
                                                            :options="
                                                        [
                                                            'Херсон',
                                                            'Одесса',
                                                            'Херсон2'
                                                        ]" >
                                                    </v-select>
                                                </div>
                                                <div class="line-field">
                                                    <div class="validation-field select-wrapper">
                                                        <v-select
                                                                v-model="delivery.address"
                                                                placeholder="Выберите ваш адрес"
                                                                :options="
                                                        [
                                                            'Херсон (Херсонская обл.)',
                                                            'Одесса (Одесская обл.)',
                                                            'Херсон2 (Херсонская2 обл.)'
                                                        ]" >
                                                        </v-select>
                                                    </div>
                                                    <label class="validation-field" :class="{ hasvalue: delivery.house, error: $v.delivery.house.$error  }">
                                                        <input v-model="$v.delivery.house.$model" placeholder="Дом">
                                                        <div class="error" v-if="!$v.delivery.house.required && $v.delivery.house.$dirty">Поле обязательно для заполнения</div>
                                                    </label>
                                                    <label class="validation-field" :class="{ hasvalue: delivery.flat, error: $v.delivery.flat.$error  }">
                                                        <input v-model="$v.delivery.flat.$model" placeholder="Кв. №">
                                                        <div class="error" v-if="!$v.delivery.flat.required && $v.delivery.flat.$dirty">Поле обязательно для заполнения</div>
                                                    </label>
                                                </div>
                                                <div class="recipient-wrap">
                                                    <label class="validation-field">
                                                        <input type="radio" @change="recipient($event.target.value)" name="recipient_1" value="self" v-model="delivery.recipient">
                                                        <span class="radio"></span>
                                                        <span class="input-title">Я получатель</span>
                                                    </label>
                                                    <label class="validation-field">
                                                        <input type="radio" @change="recipient($event.target.value)" name="recipient_1" value="another" v-model="delivery.recipient">
                                                        <span class="radio"></span>
                                                        <span class="input-title">Другой получатель</span>
                                                    </label>
                                                </div>
                                                <label class="validation-field" :class="{ hasvalue: delivery.name, error: $v.delivery.name.$error  }">
                                                    <input v-model="$v.delivery.name.$model" placeholder="Ваше имя*">
                                                    <div class="error" v-if="!$v.delivery.name.required && $v.delivery.name.$dirty">Поле обязательно для заполнения</div>
                                                </label>
                                                <label class="validation-field" :class="{ hasvalue: delivery.phone }">
                                                    <div>
                                                        <the-mask mask="+38 (###) ### ##-##" v-model="delivery.phone" placeholder="Телефон"/>
                                                    </div>
                                                </label>
                                                <label class="validation-field" :class="{ hasvalue: delivery.email, error: $v.delivery.email.$error }">
                                                    <input v-model="$v.delivery.email.$model" placeholder="Email*">
                                                    <div class="error" v-if="!$v.delivery.email.email && $v.delivery.email.$dirty ">Введите корректный e-mail</div>
                                                    <div class="error" v-if="!$v.delivery.email.required && $v.delivery.email.$dirty">Поле обязательно для заполнения</div>
                                                </label>
                                                <label class="validation-field check-wrapper">
                                                    <input type="checkbox" name="accept" checked>
                                                    <span class="check"></span>
                                                    <span class="input-title">Использовать в качестве моего адреса доставки по умолчанию</span>
                                                </label>
                                                <div class="delivery-submit-wrap">
                                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                                        Сохранить изменения
                                                        <input type="submit" :disabled="$v.$anyError">
                                                    </label>
                                                    <label class="reset" @click="reset">
                                                        Отменить
                                                    </label>
                                                </div>

                                            </form>
                                        </form-container>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <div class="card">
                                    <img src="img/img/card.svg" alt="">
                                    <div class="first">11</div>
                                    <div class="second">22</div>
                                    <div class="third">33</div>
                                    <div class="fourth">44</div>
                                    <div class="percent">-7%</div>
                                </div>
                                <div class="discount-box">
                                    <div class="discount-info">Для получения <span class="discount-percent">10%</span> скидки приобретите товар на сумму <span class="price"><span>25000</span> грн.</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>