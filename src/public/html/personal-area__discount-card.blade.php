<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <div class="card-wrap">
                            <div class="left">
                                <h1>Добавить номер дисконтной карты</h1>
                                <p>Активируйте свою Дисконтную карту, добавив ее номер, накапливайте неизменно действующую на все категории товаров скидку 3, 5, 7 и 10% при дальнейших покупках.</p>
                                <form action="/">
                                    <div class="card-numbers">
                                        <input type="text" maxlength="2">
                                        <input type="text" maxlength="2">
                                        <input type="text" maxlength="2">
                                        <input type="text" maxlength="2">
                                        <div class="hover-info">
                                            <div class="hover-info__trigger">?</div>
                                            <div class="hover-info__text">
                                                <p>Пример текста при наведении</p>
                                                <p>Пример текста при наведении</p>
                                                <p>Пример текста при наведении</p>
                                            </div>
                                        </div>
                                    </div>
                                    <label class="button">
                                        Сохранить карту
                                        <input type="submit">
                                    </label>
                                </form>
                            </div>
                            <div class="right">
                                <div class="discount-top-info">
                                    В зависимости от суммы накопления процент скидки будет составлять:
                                </div>
                                <div class="discount-info-wrap">
                                    <div class="discount-item">
                                        <div class="summ"><span>500</span> грн.</div>
                                        <div class="discount">3%</div>
                                    </div>
                                    <div class="discount-item">
                                        <div class="summ"><span>700</span> грн.</div>
                                        <div class="discount">5%</div>
                                    </div>
                                    <div class="discount-item">
                                        <div class="summ"><span>15000</span> грн.</div>
                                        <div class="discount">7%</div>
                                    </div>
                                    <div class="discount-item">
                                        <div class="summ"><span>25000</span> грн.</div>
                                        <div class="discount">10%</div>
                                    </div>
                                </div>
                                <div class="discount-bottom-info"><span>*</span>Указанные суммы необходимо накопить до 7 января 2020 года (включительно).</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>