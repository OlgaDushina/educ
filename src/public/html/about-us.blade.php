<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">О нас</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <h1>О нас</h1>
                <div class="text-content-gray">
                    <h2>О нас</h2>
                    <p>На сегодняшний день является интернет магазином всевозможных игрушек и товаров для детей. Услугами интернет магазина Kidsana воспользовались уже более 500 000 покупателей и многие из них – наши постоянные клиенты.</p>
                    <p>Мы осуществляем доставку по всей территории Украины. Нам удалось этого достичь благодаря разумной ценовой политике, максимально широкому ассортименту и отличному сервису.</p>
                    <p> Мы стремимся давать максимально полную информацию о продаваемых нами товарах, включая состав, возрастные ограничения, производителя, характеристики и комплектацию.</p>
                    <p> Нашей главной целью и основополагающим принципом в работе является удовлетворенность клиентов.</p>
                    <h2>Что отличает Kidsana ?</h2>
                    <p>Прежде всего – попытка сделать ваши покупки предельно удобными, выгодными и радостными! С момента открытия в 2012 году нашего первого магазина в Херсоне неизменно дарит Украинцам массу уникальных предложений и приятных эмоций.</p>
                    <div class="left">
                        <img src="img/img/about-img.svg" alt="">
                    </div>
                    <h2>Удивляем ассортиментом:</h2>
                    <ul>
                        <li>
                            Изысканные платья, блузки и юбки позволят чувствовать себя, как в сказке;
                        </li>
                        <li>
                            Верхняя одежда согреет в холодное время года;
                        </li>
                        <li>
                            Одежда в стиле casual раскрасит обычные дни;
                        </li>
                        <li>
                            Игрушки на любой вкус и возраст;
                        </li>
                        <li>
                            Все для детской комнаты, позволит обставить ее лучшим образом;
                        </li>
                        <li>
                            Большой асортимент товаров для самых маленьких;
                        </li>
                    </ul>
                    <a href="javasript:void(0)" class="button large _upper">Перейти в каталог</a>
                    <p>На сегодняшний день является интернет магазином всевозможных игрушек и товаров для детей. Услугами интернет магазина Kidsana воспользовались уже более 500 000 покупателей и многие из них – наши постоянные клиенты.</p>
                    <p>Мы осуществляем доставку по всей территории Украины. Нам удалось этого достичь благодаря разумной ценовой политике, максимально широкому ассортименту и отличному сервису.</p>
                    <p> Мы стремимся давать максимально полную информацию о продаваемых нами товарах, включая состав, возрастные ограничения, производителя, характеристики и комплектацию.</p>
                    <p> Нашей главной целью и основополагающим принципом в работе является удовлетворенность клиентов.</p>
                </div>


            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>