<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header-small.html')
        <div class="content">
            <div class="container">
                <div class="ordering-wrap">
                    <div class="column">
                        <div class="h1">Оформление заказа</div>
                        <div class="step active"><span>1 шаг</span></div>
                        <div class="contact-information">
                            <div class="h2">Контактная информация</div>
                            <div class="login-wrap">
                                <div class="login-wrap__title">Постоянный клиент?</div>
                                <a href="javascript:void(0)" class="right-arrow">Войти</a>
                            </div>
                            <form action="/">
                                <label class="validation-field" :class="{ hasvalue: profile.name, error: $v.profile.name.$error  }">
                                    <input v-model="$v.profile.name.$model" placeholder="Ваше имя*">
                                    <div class="error" v-if="!$v.profile.name.required && $v.profile.name.$dirty">Поле обязательно для заполнения</div>
                                </label>
                                <label class="validation-field" :class="{ hasvalue: profile.phone }">
                                    <div>
                                        <the-mask mask="+38 (###) ### ##-##" v-model="profile.phone" placeholder="Телефон"/>
                                    </div>
                                </label>
                                <label class="validation-field" :class="{ hasvalue: profile.email, error: $v.profile.email.$error }">
                                    <input v-model="$v.profile.email.$model" placeholder="Email*">
                                    <div class="error" v-if="!$v.profile.email.email && $v.profile.email.$dirty ">Введите корректный e-mail</div>
                                    <div class="error" v-if="!$v.profile.email.required && $v.profile.email.$dirty">Поле обязательно для заполнения</div>
                                </label>
                                <div class="_f _i-center _j-between submit-wrap">
                                    <a href="javascript:void(0)" class="right-arrow">Вернуться в каталог</a>
                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                        Продолжить
                                        <input type="submit" :disabled="$v.$anyError">
                                    </label>
                                </div>
                            </form>
                        </div>
                        <div class="step"><span>2 шаг</span></div>
                        <div class="h1">Доставка и оплата</div>
                    </div>
                    <div class="column">
                        <div class="h1">Ваш заказ (3)</div>
                        <a href="javascript:void(0)" class="right-arrow">Редактировать</a>
                        <div class="order-preview">
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                        </div>
                        <div class="order-preview-result">
                            <div class="order-preview-result__info">Всего 333 товара на сумму: <span class="summ"><span>42 999</span> грн.</span></div>
                            <div class="order-preview-result__info-final">Итого: <span class="summ"><span>42 999</span> грн.</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>