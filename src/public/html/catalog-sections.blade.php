<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Обувь</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="catalog-filter-wrap">
                    <div class="catalog-filter">
                        <div class="def-down-show">
                            <slide-box
                                    v-cloak
                                    trigger="filter-trigger button"
                                    direction="left"
                                    value="Фильтр каталога"
                            >
                                <div class="touch-menu">
                                    <vuescroll>
                                        <filter-box
                                                v-cloak
                                                trigger="Ботинки, сапоги asd"
                                        >
                                            <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                            </ul>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Ботинки, сапоги asd"
                                        >
                                            <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                            </ul>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Ботинки, сапоги asd"
                                        >
                                            <filter-box
                                                    v-cloak
                                                    trigger="Ботинки, сапоги asd"
                                            >
                                                <ul class="filter-box__list">
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                    </li>
                                                </ul>
                                            </filter-box>
                                            <filter-box
                                                    v-cloak
                                                    trigger="Ботинки, сапоги asd"
                                            >
                                                <ul class="filter-box__list">
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                    </li>
                                                </ul>
                                            </filter-box>
                                            <filter-box
                                                    v-cloak
                                                    trigger="Ботинки, сапоги asd"
                                            >
                                                <ul class="filter-box__list">
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                    </li>
                                                </ul>
                                            </filter-box>
                                        </filter-box>
                                    </vuescroll>
                                </div>
                            </slide-box>
                        </div>
                        <div class="def-down-hide">
                            <filter-box
                                    v-cloak
                                    trigger="Ботинки, сапоги asd"
                            >
                                <vuescroll>
                                    <ul class="filter-box__list">
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                        </li>
                                    </ul>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Ботинки, сапоги asd"
                            >
                                <vuescroll>
                                    <ul class="filter-box__list">
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                        </li>
                                    </ul>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Ботинки, сапоги asd"
                            >
                                <filter-box
                                        v-cloak
                                        trigger="Ботинки, сапоги asd"
                                >
                                    <vuescroll>
                                        <ul class="filter-box__list">
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                            </li>
                                        </ul>
                                    </vuescroll>
                                </filter-box>
                                <filter-box
                                        v-cloak
                                        trigger="Ботинки, сапоги asd"
                                >
                                    <vuescroll>
                                        <ul class="filter-box__list">
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                            </li>
                                        </ul>
                                    </vuescroll>
                                </filter-box>
                                <filter-box
                                        v-cloak
                                        trigger="Ботинки, сапоги asd"
                                >
                                    <vuescroll>
                                        <ul class="filter-box__list">
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                            </li>
                                        </ul>
                                    </vuescroll>
                                </filter-box>
                            </filter-box>
                        </div>
                    </div>
                    <div class="catalog-wrap">
                        <div class="sections-wrap">
                            <div class="sections-item w-2 h-1" style="background-color: #0275D2">
                                <span class="sections-item__title">Одежда для</span>
                                <nav class="sections-img-wrap">
                                    <a href="javascript:void(0)" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-boy.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Мальчика</span>
                                    </a>
                                    <a href="javascript:void(0)" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-girl.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Девочки</span>
                                    </a>
                                    <a href="javascript:void(0)" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-child.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Малыша</span>
                                    </a>
                                    <a href="javascript:void(0)" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-teenager.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Подростка</span>
                                    </a>
                                </nav>
                            </div>
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-container brands-slider-small">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        Все бренды
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                        <img src="img/temp/brand.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <h1>Детская обувь</h1>
                        <div class="sections-wrap">
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <a href="javascript:void(0)" class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                            </a>
                            <div class="sections-item w-2 h-1" style="background-color: #AD56F2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-2 h-1" style="background-color: #AD56F2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-2" style="background-color: #619FFB">
                                <span class="sections-item__title">Детская обувь</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-2" style="background-color: #619FFB">
                                <span class="sections-item__title">Детская обувь</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-2" style="background-color: #619FFB">
                                <span class="sections-item__title">Детская обувь</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-2" style="background-color: #619FFB">
                                <span class="sections-item__title">Детская обувь</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">1-3 года</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">3-5 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">6-9 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">9-12 лет</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">12 +</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-us">
                    <div class="about-us__description-wrap">
                        <dropdown-text
                                open="Читать все"
                                close="Читать меньше"
                        >
                            <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                        </dropdown-text>
                    </div>
                    <div class="about-us__form-wrap">
                        <div class="title">Узнавайте об акциях первыми</div>
                        <form action="/">
                            <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                            </label>
                            <label class="submit" :class="{ disable: $v.$anyError }">
                                <input type="submit" :disabled="$v.$anyError">
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>