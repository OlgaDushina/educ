<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header-small.html')
        <div class="content success-order-decor">
            <div class="container">
                <div class="success-order">
                    <div class="success-order__title">
                        Спасибо!<br> Ваш заказ принят!
                    </div>
                    <div class="success-order__number">Номер Вашего заказа 375</div>
                    <div class="success-order__info">Наши менеджеры свяжутся с вами в ближайшее время.</div>
                    <div class="success-order__info">Мы всегда стараемся сделать наш сервис лучше.</div>
                    <div class="success-order__buttons">
                        <a href="javascript:void(0)" class="right-arrow">Вернуться в каталог</a>
                        <a href="javascript:void(0)" class="button">на Главную</a>
                    </div>
                </div>
                <img src="img/img/ball-1.svg" alt="" class="ball-1 ball-decor">
                <img src="img/img/ball-2.svg" alt="" class="ball-2 ball-decor">
                <img src="img/img/ball-3.svg" alt="" class="ball-3 ball-decor">
                <img src="img/img/ball-4.svg" alt="" class="ball-4 ball-decor">
                <img src="img/img/ball-5.svg" alt="" class="ball-5 ball-decor">
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>