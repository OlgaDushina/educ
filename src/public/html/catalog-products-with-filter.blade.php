<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Обувь</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="catalog-filter-wrap">
                    <div class="catalog-filter type-2">
                        <div class="def-down-show">
                            <slide-box
                                    v-cloak
                                    trigger="filter-trigger button"
                                    direction="left"
                                    value="Фильтр каталога"
                            >
                                <div class="touch-menu">
                                    <vuescroll>
                                        <filter-box
                                                v-cloak
                                                trigger="Категория"
                                        >
                                            <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                                </li>
                                            </ul>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Цена"
                                        >
                                            <range-slider ref="range"></range-slider>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Пол"
                                        >
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="female" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Девочка</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="male" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Мальчик</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="all" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Мальчик/Девочка</div>
                                            </label>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Возраст"
                                        >
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="all" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">Все</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="0-1" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">До года</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="1-2" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">1 год - 2 годa</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="3-5" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">3 годa - 5 лет</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="6-9" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">6 лет - 9 лет</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="10-12" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">10 лет - 12 лет</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="12+" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">Старше 12 лет</div>
                                            </label>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Торговая марка"
                                        >
                                            <label class="validation-field">
                                                <input type="text" placeholder="Поиск по бренду" @input="filterBrand($event.target.value)">
                                            </label>
                                            <label class="validation-field check-wrapper" v-for="(item, index) in brandList" :key="index">
                                                <input type="checkbox" :value="item.value" v-model="filter.brand">
                                                <div class="check"></div>
                                                <div class="input-title">{{item.title}} <span>{{item.count}}</span></div>
                                            </label>
                                        </filter-box>
                                    </vuescroll>
                                </div>
                            </slide-box>
                        </div>
                        <div class="def-down-hide">
                            <filter-box
                                    v-cloak
                                    trigger="Категория"
                            >
                                <vuescroll>
                                    <ul class="filter-box__list">
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                        </li>
                                    </ul>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Цена"
                            >
                                <vuescroll>
                                    <range-slider ref="range"></range-slider>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Пол"
                            >
                                <vuescroll>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="female" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Девочка</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="male" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Мальчик</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="all" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Мальчик/Девочка</div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Возраст"
                            >
                                <vuescroll>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="all" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">Все</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="0-1" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">До года</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="1-2" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">1 год - 2 годa</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="3-5" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">3 годa - 5 лет</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="6-9" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">6 лет - 9 лет</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="10-12" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">10 лет - 12 лет</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="12+" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">Старше 12 лет</div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Торговая марка"
                            >
                                <label class="validation-field">
                                    <input type="text" placeholder="Поиск по бренду" :value="filterBrand" @input="debounceFilter">
                                </label>
                                <vuescroll>
                                    <label class="validation-field check-wrapper" v-for="(item, index) in brandList" :key="index">
                                        <input type="checkbox" :value="item.value" v-model="filter.brand">
                                        <div class="check"></div>
                                        <div class="input-title">{{item.title}} <span>{{item.count}}</span></div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                        </div>
                    </div>
                    <div class="catalog-wrap">
                        <div class="sorting-wrap">
                            <div class="child-products">
                                <div class="child-products__item-img">
                                    <img src="img/img/child.svg" alt="">
                                </div>
                                <div class="child-products__info">
                                    <div class="child-products__title">Товары для вашего ребенка</div>
                                    <span>
                                     <a href="javascript:void(0)" class="child-products__add-link">
                                    <span>Информация о ребенке</span>
                                </a>
                                     <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </span>

                                </div>
                            </div>
                            <div class="sorting">
                                <div class="sorting__title">Сортировать по:</div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="sorting"
                                            placeholder="Сортировка"
                                            :options="
                                        [
                                            'сначала с акциями',
                                            'сначала без акций',
                                            'по возрастанию цены'
                                        ]" >
                                    </v-select>
                                </div>
                            </div>
                        </div>
                        <div class="selected">
                            <div class="selected__title">Вы выбрали:</div>
                            <template v-for="(item1, index1) in filter">
                                <div class="selected__item" v-for="(item, index) in item1" :key="item" v-if="index1 !== 'MinMaxPrice' && index1 !== 'price' && index1 !== 'age'" @click="removeFromArr(index1, index)">
                                    {{item}}
                                </div>
                                <div class="selected__item" v-if="index1 === 'age' && item1 !== ''" @click="filter[index1] = ''">
                                    {{item1}}
                                </div>
                            </template>
                            <div class="selected__remove" @click="filter = {...clearFilter}; resetPrice();">Сбросить все</div>
                        </div>
                        <div class="area-catalog-wrap">
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                            @@include('page-fragments/catalog-item-full.html')
                        </div>
                        <div class="pagination-wrap">
                            <a href="javascript:void(0)" class="load-more">показать еще 30</a>
                            <ul class="pagination">
                                <li class="prev">
                                    <a href="javascript:void(0)"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">1</a>
                                </li>
                                <li class="active">
                                    <a href="javascript:void(0)">2</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">...</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">15</a>
                                </li>
                                <li class="next">
                                    <a href="javascript:void(0)"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap _mb-110">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="about-us">
                    <div class="about-us__description-wrap">
                        <dropdown-text
                                open="Читать все"
                                close="Читать меньше"
                        >
                            <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                        </dropdown-text>
                    </div>
                    <div class="about-us__form-wrap">
                        <div class="title">Узнавайте об акциях первыми</div>
                        <form action="/">
                            <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                            </label>
                            <label class="submit" :class="{ disable: $v.$anyError }">
                                <input type="submit" :disabled="$v.$anyError">
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
<script>
    App._data.filter = {
        brand: ['Alisa Line2'],
        sex: ['all'],
        age: '0-1',
        price: [150, 1985],
        MinMaxPrice: [0, 2500]
    };
    App._data.clearFilter = {
        brand: [],
        sex: [],
        age: '',
        price: App._data.filter.MinMaxPrice,
        MinMaxPrice: App._data.filter.MinMaxPrice
    };
    App._data.brandList = [
        {
            "title": "Alisa Line",
            "count": "124",
            "value": "Alisa Line"
        },
        {
            "title": "Alisa Line2",
            "count": "224",
            "value": "Alisa Line2"
        },
        {
            "title": "Alisa Line3",
            "count": "324",
            "value": "Alisa Line3"
        },
        {
            "title": "Alisa Line32",
            "count": "310",
            "value": "Alisa Line32"
        }
    ]
</script>
</body>
</html>