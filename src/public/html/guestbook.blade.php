<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Отзывы</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <h1>Отзывы</h1>
                <div class="send-review-wrap">
                    <div class="send-review-wrap__title">
                        Оставляй впечатления о сервисе бренда Kidsana! <br>Помоги нам становиться лучше.
                    </div>
                    <a href="javascript:void(0)" class="button large"  @click="$refs.guestbook.show();">Оставить отзыв</a>
                </div>
                <div class="reviews-wrap">
                    <div class="review">
                        <div class="review__top-line">
                            <div class="name">Ангелина</div>
                            <div class="date">13.07.2020</div>
                        </div>
                        <div class="review__bottom-line">
                            Хочу оставить большое слово Благодарности за вашу продукцию. Могу сказать, что Ваша команда очень слажено работает. Новые модели, стильные и неповторимые. Они всегда радуют. Когда их продаёшь, люди не перестают восхищаться. Да, цена немного дороговата, но, исходя из того, что дешевое – хорошим не бывает, то терпимо. И ваша продукция рассчитана на клиента, кто понимает в этом. Огромное Вам Спасибо за доверие и сотрудничество! Благодаря Вашей продукции у меня всегда есть Изюминка!!!
                        </div>
                    </div>
                    <div class="review">
                        <div class="review__top-line">
                            <div class="name">Ангелина</div>
                            <div class="date">13.07.2020</div>
                        </div>
                        <div class="review__bottom-line">
                            Хочу оставить большое слово Благодарности за вашу продукцию. Могу сказать, что Ваша команда очень слажено работает. Новые модели, стильные и неповторимые. Они всегда радуют. Когда их продаёшь, люди не перестают восхищаться. Да, цена немного дороговата, но, исходя из того, что дешевое – хорошим не бывает, то терпимо. И ваша продукция рассчитана на клиента, кто понимает в этом. Огромное Вам Спасибо за доверие и сотрудничество! Благодаря Вашей продукции у меня всегда есть Изюминка!!!
                        </div>
                    </div>
                    <div class="review">
                        <div class="review__top-line">
                            <div class="name">Ангелина</div>
                            <div class="date">13.07.2020</div>
                        </div>
                        <div class="review__bottom-line">
                            Хочу оставить большое слово Благодарности за вашу продукцию. Могу сказать, что Ваша команда очень слажено работает. Новые модели, стильные и неповторимые. Они всегда радуют. Когда их продаёшь, люди не перестают восхищаться. Да, цена немного дороговата, но, исходя из того, что дешевое – хорошим не бывает, то терпимо. И ваша продукция рассчитана на клиента, кто понимает в этом. Огромное Вам Спасибо за доверие и сотрудничество! Благодаря Вашей продукции у меня всегда есть Изюминка!!!
                        </div>
                    </div>
                </div>

                <div class="pagination-wrap">
                    <a href="javascript:void(0)" class="load-more">показать еще 30</a>
                    <ul class="pagination">
                        <li class="prev">
                            <a href="javascript:void(0)"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">1</a>
                        </li>
                        <li class="active">
                            <a href="javascript:void(0)">2</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">...</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">15</a>
                        </li>
                        <li class="next">
                            <a href="javascript:void(0)"></a>
                        </li>
                    </ul>
                </div>


            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>