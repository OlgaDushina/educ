<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="cart-page-wrapper">
                    <div class="column">
                        <div class="cart-top-line">
                            <span>Товар</span>
                            <span>Цена</span>
                            <span>Кол-во</span>
                            <span>Сумма</span>
                        </div>
                        <cart-item
                                type="full"></cart-item>
                        <cart-item
                                type="full"></cart-item>
                        <cart-item
                                type="full"></cart-item>
                    </div>
                    <div class="column">
                        <div class="order-preview-result__info">Всего 333 товара на сумму: <span class="summ"><span>42 999</span> грн.</span></div>
                        <div class="order-preview-result__info-final">Итого: <span class="summ"><span>42 999</span> грн.</span></div>
                        <a href="javascript:void(0)" class="button _upper red">Оформить заказ</a>
                        <div class="order-info">Стоимость доставки вы сможете узнать при оформлении заказа</div>
                    </div>
                </div>


            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>