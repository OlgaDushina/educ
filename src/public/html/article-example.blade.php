<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Полезные статьи</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <h1>Полезные статьи</h1>
                <div class="inline-item-img">
                    <img src="img/temp/top-img.jpg" alt="">
                </div>
            </div>
            <div class="container-small">
                <h1>Вот и лето пришло: напоминаем родителям и детям о правилах безопасности...</h1>
                <div class="item-date">12.06.2020</div>
                <p>На сегодняшний день является интернет магазином всевозможных игрушек и товаров для детей. Услугами интернет магазина Kidsana воспользовались уже более 500 000 покупателей и многие из них – наши постоянные клиенты. Мы осуществляем доставку по всей территории Украины. Нам удалось этого достичь благодаря разумной ценовой политике, максимально широкому ассортименту и отличному сервису. Мы стремимся давать максимально полную информацию о продаваемых нами товарах, включая состав, возрастные ограничения, производителя, характеристики и комплектацию. Нашей главной целью и основополагающим принципом в работе является удовлетворенность клиентов.</p>
                <ul>
                    <li>Изысканные платья, блузки и юбки позволят чувствовать себя, как в сказке;</li>
                    <li>Верхняя одежда согреет в холодное время года;</li>
                    <li>Мы осуществляем доставку по всей территории Украины. Нам удалось этого достичь благодаря разумной ценовой политике, максимально широкому ассортименту и отличному сервису.</li>
                </ul>
                <p>На сегодняшний день является интернет магазином всевозможных игрушек и товаров для детей. Услугами интернет магазина Kidsana воспользовались уже более 500 000 покупателей и многие из них – наши постоянные клиенты. Мы осуществляем доставку по всей территории Украины. Нам удалось этого достичь благодаря разумной ценовой политике, максимально широкому ассортименту и отличному сервису. Мы стремимся давать максимально полную информацию о продаваемых нами товарах, включая состав, возрастные ограничения, производителя, характеристики и комплектацию. Нашей главной целью и основополагающим принципом в работе является удовлетворенность клиентов.</p>
                <div class="inline-item-img">
                    <img src="img/temp/article-img.jpg" alt="">
                </div>
                <p>На сегодняшний день является интернет магазином всевозможных игрушек и товаров для детей. Услугами интернет магазина Kidsana воспользовались уже более 500 000 покупателей и многие из них – наши постоянные клиенты. Мы осуществляем доставку по всей территории Украины. Нам удалось этого достичь благодаря разумной ценовой политике, максимально широкому ассортименту и отличному сервису. Мы стремимся давать максимально полную информацию о продаваемых нами товарах, включая состав, возрастные ограничения, производителя, характеристики и комплектацию. Нашей главной целью и основополагающим принципом в работе является удовлетворенность клиентов.</p>
                <div class="share-wrap">
                    <a href="javascript:void(0)" class="share-item">
                        <span class="icon"></span>
                    </a>
                    @@include('page-fragments/social.html')
                </div>

            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>