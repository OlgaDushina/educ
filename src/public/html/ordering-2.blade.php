<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header-small.html')
        <div class="content">
            <div class="container">
                <div class="ordering-wrap">
                    <div class="column">
                        <div class="h1">Оформление заказа</div>
                        <div class="step"><span>1 шаг</span></div>
                        <div class="contact-information">
                            <div class="h2">Контактная информация</div>
                            <div class="contact-information__ready">
                                <div class="info">Анна Березина, +38 (978) 566-45-67</div>
                                <a href="javascript:void(0)" class="right-arrow">Редактировать</a>
                            </div>
                        </div>
                        <div class="step active"><span>2 шаг</span></div>
                        <div class="contact-information">
                            <div class="h2">Доставка и оплата</div>
                            <div class="login-wrap">
                                <div class="login-wrap__title">Способ доставки</div>
                            </div>
                            <form action="/">
                                <div class="_f _i-start provider-wrap">
                                    <label class="validation-field _f _i-center">
                                        <input type="radio" value="ukrpost" v-model="ordering__delivery.provider">
                                        <div class="radio"></div>
                                        <img src="img/img/ukr-post.svg" alt="" class="provider-img">
                                    </label>
                                    <label class="validation-field _f _i-center">
                                        <input type="radio" value="newpost" v-model="ordering__delivery.provider">
                                        <div class="radio"></div>
                                        <img src="img/img/new-post.svg" alt="" class="provider-img">
                                    </label>
                                </div>

                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="ordering__delivery.type"
                                            placeholder="Выберите способ доставки"
                                            :options="
                                        [
                                            'В отделении',
                                            'Курьером',
                                        ]" >
                                    </v-select>
                                </div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="ordering__delivery.city"
                                            placeholder="Выберите город"
                                            :options="
                                        [
                                            'Херсон',
                                            'Одесса',
                                        ]" >
                                    </v-select>
                                </div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="ordering__delivery.address"
                                            placeholder="Выберите отделение"
                                            :options="
                                        [
                                            'Почтомат №3086: Люстдорфская дорога, 140',
                                            'Почтомат №3087: Люстдорфская дорога, 138'
                                        ]" >
                                    </v-select>
                                </div>
                                <div class="ordering-title">Способ оплаты</div>
                                <div class="ordering-payment-wrap">
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="При получении">
                                        <span class="radio"></span>
                                        <span class="input-title">При получении
                                        <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                        </div>
                                    </span>
                                    </label>
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="Безналичный расчет">
                                        <span class="radio"></span>
                                        <span class="input-title">Безналичный расчет
                                        <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                    </span>
                                    </label>
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="Банковской картой">
                                        <span class="radio"></span>
                                        <span class="input-title">Банковской картой</span>
                                    </label>
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="Картой на сайте через LIQPAY">
                                        <span class="radio"></span>
                                        <span class="input-title">Картой на сайте через LIQPAY
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                    </span>
                                    </label>
                                </div>

                                <toggle-box
                                        trigger="Добавить коментарий к заказу"
                                >
                                    <label class="validation-field">
                                        <textarea v-model="ordering__delivery.message" placeholder="Сообщение"></textarea>
                                    </label>
                                </toggle-box>
                                <label class="validation-field _f">
                                    <input type="checkbox" v-model="ordering__delivery.nocall" value="true">
                                    <span class="check"></span>
                                    <span class="input-title">Не перезванивайте мне, я подтверждаю заказ</span>
                                </label>
                                <label class="button _upper" :class="{ disable: $v.$anyError }">
                                    Оформить заказ
                                    <input type="submit" :disabled="$v.$anyError">
                                </label>
                            </form>
                        </div>
                    </div>
                    <div class="column">
                        <div class="h1">Ваш заказ (3)</div>
                        <a href="javascript:void(0)" class="right-arrow">Редактировать</a>
                        <div class="order-preview">
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                        </div>
                        <div class="order-preview-result">
                            <div class="order-preview-result__info">Всего 333 товара на сумму: <span class="summ"><span>42 999</span> грн.</span></div>
                            <div class="order-preview-result__info-final">Итого: <span class="summ"><span>42 999</span> грн.</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>