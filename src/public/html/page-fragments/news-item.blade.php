<div class="news-item">
    <a href="javascript:void(0)" class="news-item__item-img">
        <img src="img/temp/news.jpg" alt="">
    </a>
    <a href="javascript:void(0)" class="news-item__title">
        Вот и лето пришло: напоминаем родителям и детям о правилах безопасности...
    </a>
    <div class="news-item__date">12.06.2020</div>
</div>