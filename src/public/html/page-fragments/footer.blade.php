<footer>
    <div class="container">
        <div class="top-line">
            <div class="footer-column-wrap">
                <div class="footer-column">
                    <a href="/" class="footer-logo">Kidsanna</a>
                    <a href="mailto:kidsanna.com@gmail.com" class="email">kidsanna.com@gmail.com</a>
                    @@include('phone-box-footer.html')
                    @@include('social.html')
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Магазин</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">О нас</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отзывы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Контакты</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Полезные статьи</a>
                        </li>
                    </ul>
                    <img src="img/img/visa.svg" alt="" class="visa">
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Заказ</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Дисконтная программа</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отследить заказ</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Личный кабинет</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Мой профиль</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Мои заказы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Избранное</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Популярные категории</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Игрушки</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Одежда</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Питание</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Обувь</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Подгузники</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Коляски</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Автокресла</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">% Акции</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Outlet</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom-line">
            <div class="copyright">© 2020 Все права защищены</div>
            <a href="javascript:void(0)" class="flaxen">
                <img src="img/img/flaxen.svg" alt="">
            </a>
        </div>

    </div>
</footer>