<div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>