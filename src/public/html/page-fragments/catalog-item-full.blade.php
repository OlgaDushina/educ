<div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>