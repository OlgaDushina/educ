<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <div class="add-children-wrap">
                            <div class="left">
                                <h1>Моя семья</h1>
                                <a href="javascript:void(0)" class="plus">Добавить ребенка</a>
                                <div class="children">
                                    <div class="child">
                                        <div class="child__item-img">
                                            <img src="img/img/boy.svg" alt="">
                                        </div>
                                        <div class="child__info">
                                            <div class="child__name">Егорка</div>
                                            <div class="child__age">7 лет</div>
                                        </div>
                                        <a href="javascript:void(0)" class="child__edit" @click="
                                        $refs.changechild.show();
                                         child =  {
                                            sex: 'female',
                                            name: 'asdasd',
                                            date: new Date(2018, 2, 25),
                                        };
                                    "></a>
                                        <a href="javascript:void(0)" class="child__close"></a>
                                    </div>
                                    <div class="child">
                                        <div class="child__item-img">
                                            <img src="img/img/boy.svg" alt="">
                                        </div>
                                        <div class="child__info">
                                            <div class="child__name">Егорка</div>
                                            <div class="child__age">7 лет</div>
                                        </div>
                                        <a href="javascript:void(0)" class="child__edit" @click="
                                        $refs.changechild.show();
                                         child =  {
                                            sex: 'female',
                                            name: 'asdasd',
                                            date: new Date(2018, 2, 25),
                                        };
                                    "></a>
                                        <a href="javascript:void(0)" class="child__close"></a>
                                    </div>
                                    <div class="child">
                                        <div class="child__item-img">
                                            <img src="img/img/boy.svg" alt="">
                                        </div>
                                        <div class="child__info">
                                            <div class="child__name">Егорка</div>
                                            <div class="child__age">7 лет</div>
                                        </div>
                                        <a href="javascript:void(0)" class="child__edit" @click="
                                        $refs.changechild.show();
                                         child =  {
                                            sex: 'female',
                                            name: 'asdasd',
                                            date: new Date(2018, 2, 25),
                                        };
                                    "></a>
                                        <a href="javascript:void(0)" class="child__close"></a>
                                    </div>
                                    <div class="child">
                                        <div class="child__item-img">
                                            <img src="img/img/girl.svg" alt="">
                                        </div>
                                        <div class="child__info">
                                            <div class="child__name">Светлана</div>
                                            <div class="child__age">5 лет</div>
                                        </div>
                                        <a href="javascript:void(0)" class="child__edit" @click="
                                        $refs.changechild.show();
                                         child =  {
                                            sex: 'female',
                                            name: 'asdasd',
                                            date: new Date(2018, 2, 25),
                                        };
                                    "></a>
                                        <a href="javascript:void(0)" class="child__close"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="right">
                                <div class="title">Выгоды</div>
                                <p>Добавьте информацию о своем ребенке и получайте:</p>
                                <ul>
                                    <li>Выгодные предложения каждый месяц</li>
                                    <li>Выгодные предложения ко дню рождения</li>
                                    <li>Специальные скидки и промокоды</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>