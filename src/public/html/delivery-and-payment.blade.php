<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Доставка и оплата</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <h1>Доставка и оплата</h1>
                <accordeon-item
                        v-cloak
                        title="Курьером по адресу"
                >
                    <p>Вам удобно получить посылку по адресу, оплатив при этом наличными — сообщите об этом менеджеру контакт-центра "Антошка" по телефону 0 (800) 306-063 или, оставив комментарий в заказе. Основные условия для отправки Вашего заказа в города Запорожье, Львов, Харьков, Днепр, Киев или Одесса курьерской службой "Нова пошта" с оплатой наличными курьеру.</p>
                    <ul>
                        <li>Сумма заказа к оплате — не более 14 тыс. грн.;</li>
                        <li>Ф.И.О. получателя;</li>
                        <li>Адрес получения заказа находится в границах следующих областных центров: Запорожье, Львов, Харьков, Днепр, Киев или Одесса.</li>
                    </ul>
                    <p>Если Вы проживаете в других населенных пунктах Украины — временно, при выборе доставки курьером компании «Нова пошта» необходимо предварительно оплатить заказ банковской картой или по безналичному расчету. Оплата заказа при выборе доставки в отделение «Нова пошта» возможна, как обычно, тремя способами: наличными, банковской картой или по безналичному расчету.</p>
                    <div class="payment-wrap">
                        <div class="accordeon-content-title">Оплата:</div>
                        <div class="payment-stages-wrap">
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-1" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                <span>
                                        При получении
                                </span>
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-2" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                    Банковской картой
                                </div>
                            </div>
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-3" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                    Безналичный расчет
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordeon-content-title">Стоимость доставки (грн. с учётом НДС):</div>
                        <ul>
                            <li><b>50 грн</b> - Мелкогабаритный товар</li>
                            <li><b>199 грн</b> - Крупногабаритный товар</li>
                        </ul>
                    </div>
                </accordeon-item>
                <accordeon-item
                        v-cloak
                        title="Новая почта"
                >
                    <div class="accorderon-top-info">
                        <div class="accorderon-top-info__item-img">
                            <img src="img/img/new-post.svg" alt="">
                        </div>
                        <ul>
                            <li>срок доставки 1-3 дня</li>
                        </ul>
                    </div>
                    <p>C 26 апреля 2017г. хранение отправления в отделении курьерской службы «Нова пошта» без дополнительных платежей возможно в течение 5 рабочих дней отделения со дня его прибытия на отделение.</p>
                    <div class="accordeon-content-title">Когда доставим:</div>
                    <p>Уточнить дату получения заказа вы можете на сайте компании «Новая Почта»</p>
                    <a href="javascript:void(0)" class="right-arrow">Перейти на сайт</a>
                    <div class="accordeon-content-title">Куда доставим:</div>
                    <p>Любой город, где есть отделение «Новая Почта»</p>
                    <div class="accordeon-content-title">Что доставим:</div>
                    <p>Любой товар весом до 100 кг и/или габаритами до 150*165*150 см</p>
                    <div class="payment-wrap">
                        <div class="accordeon-content-title">Оплата:</div>
                        <div class="payment-stages-wrap">
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-1" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                <span>
                                        При получении
                                </span>
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-2" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                    Банковской картой
                                </div>
                            </div>
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-3" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                    Безналичный расчет
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordeon-content-title">Стоимость доставки (грн. с учётом НДС):</div>
                        <ul>
                            <li><b>50 грн</b> - Мелкогабаритный товар</li>
                            <li><b>199 грн</b> - Крупногабаритный товар</li>
                        </ul>
                    </div>
                </accordeon-item>
                <accordeon-item
                        v-cloak
                        title="Укр почта"
                >
                    <div class="accorderon-top-info">
                        <div class="accorderon-top-info__item-img">
                            <img src="img/img/ukr-post.svg" alt="">
                        </div>
                        <ul>
                            <li>срок доставки 1-3 дня</li>
                        </ul>
                    </div>
                    <p>C 26 апреля 2017г. хранение отправления в отделении курьерской службы «Нова пошта» без дополнительных платежей возможно в течение 5 рабочих дней отделения со дня его прибытия на отделение.</p>
                    <div class="accordeon-content-title">Когда доставим:</div>
                    <p>Уточнить дату получения заказа вы можете на сайте компании «Новая Почта»</p>
                    <a href="javascript:void(0)" class="right-arrow">Перейти на сайт</a>
                    <div class="accordeon-content-title">Куда доставим:</div>
                    <p>Любой город, где есть отделение «Новая Почта»</p>
                    <div class="accordeon-content-title">Что доставим:</div>
                    <p>Любой товар весом до 100 кг и/или габаритами до 150*165*150 см</p>
                    <div class="payment-wrap">
                        <div class="accordeon-content-title">Оплата:</div>
                        <div class="payment-stages-wrap">
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-1" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                <span>
                                        При получении
                                </span>
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-2" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                    Банковской картой
                                </div>
                            </div>
                            <div class="payment-stages-item">
                                <div class="payment-stages-item__item-img">
                                    <img src="img/sprite.svg#payment-3" alt="">
                                </div>
                                <div class="payment-stages-item__title">
                                    Безналичный расчет
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordeon-content-title">Стоимость доставки (грн. с учётом НДС):</div>
                        <ul>
                            <li><b>50 грн</b> - Мелкогабаритный товар</li>
                            <li><b>199 грн</b> - Крупногабаритный товар</li>
                        </ul>
                    </div>
                </accordeon-item>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>
@@include('page-fragments/script-include.html')
</body>
</html>