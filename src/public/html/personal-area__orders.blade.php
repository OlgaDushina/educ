<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <h3>Мои заказы</h3>
                        <div class="orders-wrap">
                            <order
                                    v-cloak
                                    number="23456789"
                                    date="12.07.2020"
                                    count="3"
                                    summ="42999"
                                    status="Отменен"
                                    status-color="red"
                            >
                                <div class="order-product">
                                    <a href="javascript:void(0)" class="order-product__item-img">
                                        <img src="img/temp/order-product-img.svg" alt="">
                                    </a>
                                    <div class="order-product__info">
                                        <div class="order-product__top-info">3 мес. – 3 года   TM Biomecanics</div>
                                        <a href="javascript:void(0)" class="order-product__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                        <div class="order-product__bottom-info">
                                            <span>Артикул: 12633</span>
                                            <span>Размер: 24</span>
                                            <span>Цвет: красный</span>
                                        </div>
                                    </div>
                                    <div class="order-product__count">26 шт.</div>
                                    <div class="order-product__price"><span>42 999</span> грн.</div>
                                </div>
                                <div class="order-product">
                                    <a href="javascript:void(0)" class="order-product__item-img">
                                        <img src="img/temp/order-product-img.svg" alt="">
                                    </a>
                                    <div class="order-product__info">
                                        <div class="order-product__top-info">3 мес. – 3 года   TM Biomecanics</div>
                                        <a href="javascript:void(0)" class="order-product__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                        <div class="order-product__bottom-info">
                                            <span>Артикул: 12633</span>
                                            <span>Размер: 24</span>
                                            <span>Цвет: красный</span>
                                        </div>
                                    </div>
                                    <div class="order-product__count">26 шт.</div>
                                    <div class="order-product__price"><span>42 999</span> грн.</div>
                                </div>
                                <div class="result">
                                    <div class="result__info">
                                        <div class="title">Стоимость товаров:</div>
                                        <div class="value"><span>42999</span> грн.</div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Доставка:</div>
                                        <div class="value"><span>45</span> грн. <span>Новая почта</span></div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Оплата:</div>
                                        <div class="value"><span>Оплата картой</span></div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Общая сумма:</div>
                                        <div class="value"><span>43044</span> грн.</div>
                                    </div>
                                </div>
                            </order>
                            <order
                                    v-cloak
                                    number="23456789"
                                    date="12.07.2020"
                                    count="3"
                                    summ="42999"
                                    status="Доставлен"
                                    status-color="green"
                            >
                                <div class="order-product">
                                    <a href="javascript:void(0)" class="order-product__item-img">
                                        <img src="img/temp/order-product-img.svg" alt="">
                                    </a>
                                    <div class="order-product__info">
                                        <div class="order-product__top-info">3 мес. – 3 года   TM Biomecanics</div>
                                        <a href="javascript:void(0)" class="order-product__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                        <div class="order-product__bottom-info">
                                            <span>Артикул: 12633</span>
                                            <span>Размер: 24</span>
                                            <span>Цвет: красный</span>
                                        </div>
                                    </div>
                                    <div class="order-product__count">26 шт.</div>
                                    <div class="order-product__price"><span>42 999</span> грн.</div>
                                </div>
                                <div class="order-product">
                                    <a href="javascript:void(0)" class="order-product__item-img">
                                        <img src="img/temp/order-product-img.svg" alt="">
                                    </a>
                                    <div class="order-product__info">
                                        <div class="order-product__top-info">3 мес. – 3 года   TM Biomecanics</div>
                                        <a href="javascript:void(0)" class="order-product__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                        <div class="order-product__bottom-info">
                                            <span>Артикул: 12633</span>
                                            <span>Размер: 24</span>
                                            <span>Цвет: красный</span>
                                        </div>
                                    </div>
                                    <div class="order-product__count">26 шт.</div>
                                    <div class="order-product__price"><span>42 999</span> грн.</div>
                                </div>
                                <div class="result">
                                    <div class="result__info">
                                        <div class="title">Стоимость товаров:</div>
                                        <div class="value"><span>42999</span> грн.</div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Доставка:</div>
                                        <div class="value"><span>45</span> грн. <span>Новая почта</span></div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Оплата:</div>
                                        <div class="value"><span>Оплата картой</span></div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Общая сумма:</div>
                                        <div class="value"><span>43044</span> грн.</div>
                                    </div>
                                </div>
                            </order>
                            <order
                                    v-cloak
                                    number="23456789"
                                    date="12.07.2020"
                                    count="3"
                                    summ="42999"
                                    status="В обработке"
                                    status-color="blue"
                            >
                                <div class="order-product">
                                    <a href="javascript:void(0)" class="order-product__item-img">
                                        <img src="img/temp/order-product-img.svg" alt="">
                                    </a>
                                    <div class="order-product__info">
                                        <div class="order-product__top-info">3 мес. – 3 года   TM Biomecanics</div>
                                        <a href="javascript:void(0)" class="order-product__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                        <div class="order-product__bottom-info">
                                            <span>Артикул: 12633</span>
                                            <span>Размер: 24</span>
                                            <span>Цвет: красный</span>
                                        </div>
                                    </div>
                                    <div class="order-product__count">26 шт.</div>
                                    <div class="order-product__price"><span>42 999</span> грн.</div>
                                </div>
                                <div class="order-product">
                                    <a href="javascript:void(0)" class="order-product__item-img">
                                        <img src="img/temp/order-product-img.svg" alt="">
                                    </a>
                                    <div class="order-product__info">
                                        <div class="order-product__top-info">3 мес. – 3 года   TM Biomecanics</div>
                                        <a href="javascript:void(0)" class="order-product__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                        <div class="order-product__bottom-info">
                                            <span>Артикул: 12633</span>
                                            <span>Размер: 24</span>
                                            <span>Цвет: красный</span>
                                        </div>
                                    </div>
                                    <div class="order-product__count">26 шт.</div>
                                    <div class="order-product__price"><span>42 999</span> грн.</div>
                                </div>
                                <div class="result">
                                    <div class="result__info">
                                        <div class="title">Стоимость товаров:</div>
                                        <div class="value"><span>42999</span> грн.</div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Доставка:</div>
                                        <div class="value"><span>45</span> грн. <span>Новая почта</span></div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Оплата:</div>
                                        <div class="value"><span>Оплата картой</span></div>
                                    </div>
                                    <div class="result__info">
                                        <div class="title">Общая сумма:</div>
                                        <div class="value"><span>43044</span> грн.</div>
                                    </div>
                                </div>
                            </order>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>