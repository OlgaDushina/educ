<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        @@include('page-fragments/header.html')
        <div class="content">
            <div class="container">
                <div class="personal-area-wrap">
                    <div class="left-side">
                        @@include('page-fragments/personal-area-list.html')
                    </div>
                    <div class="right-side">
                        <div class="profile-wrap">
                            <div class="grey-box">
                                <div class="_f _j-between _i-center">
                                    <div class="title">Мой профиль</div>
                                    <a href="javascript:void(0)" class="edit"></a>
                                </div>
                                <div class="profile__name">Анна Березина</div>
                                <div class="profile__phone">+38 (978) 566-45-67</div>
                                <div class="profile__email">berezina_a@gmail.com</div>
                                <div class="title">Добавьте адрес</div>
                                <p>и оформляйте заказы еще быстрее</p>
                                <a href="javascript:void(0)" class="add">Добавить</a>

                            </div>
                            <div class="grey-box">
                                <div class="_f _j-between _i-center">
                                    <a href="javascript:void(0)" class="title plus">Моя семья</a>
                                    <a href="javascript:void(0)" class="edit"></a>
                                </div>
                                <div class="family">
                                    <div class="child">
                                        <div class="child__item-img">
                                            <img src="img/img/boy.svg" alt="">
                                        </div>
                                        <div class="child__info">
                                            <div class="child__name">Егорка</div>
                                            <div class="child__age">7 лет</div>
                                        </div>
                                    </div>
                                    <div class="child">
                                        <div class="child__item-img">
                                            <img src="img/img/boy.svg" alt="">
                                        </div>
                                        <div class="child__info">
                                            <div class="child__name">Егорка</div>
                                            <div class="child__age">7 лет</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <div class="card">
                                    <img src="img/img/card.svg" alt="">
                                    <div class="first">11</div>
                                    <div class="second">22</div>
                                    <div class="third">33</div>
                                    <div class="fourth">44</div>
                                    <div class="percent">-7%</div>
                                </div>
                                <div class="discount-box">
                                    <div class="discount-info">Для получения <span class="discount-percent">10%</span> скидки приобретите товар на сумму <span class="price"><span>25000</span> грн.</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                            <div class="swiper-slide">
                                @@include('page-fragments/catalog-item-short.html')
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
        @@include('page-fragments/all-modals.html')
    </div>
    @@include('page-fragments/footer.html')
</div>

@@include('page-fragments/script-include.html')
</body>
</html>