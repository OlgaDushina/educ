Vue.component('cart',{
    components: {
        vuescroll
    },
    template: `
        <div class="cart-wrap" @mouseover="resultsShow = true"  @mouseleave="resultsShow = false">
            <a href="javascript:void(0)" class="cart-like-watched__item cart">
                <span>5</span>
            </a>
            <transition name="opacity-transition">
                <div class="cart-wrap__results" v-if="resultsShow">
                    <div class="cart-wrap__results__top-line">
                        <vuescroll>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                        </vuescroll>
                    </div>
                    <div class="cart-wrap__results__bottom-line">
                        <div class="full-summ">
                            <div class="full-summ__title">Итого:</div>
                            <span>42 999</span>
                            грн.
                        </div>
                        <a href="javascript:void(0)" class="button small">Перейти в корзину</a>
                        <a href="javascript:void(0)" class="to-order right-arrow">Оформить заказ</a>
                    </div>
                </div>
            </transition>
        </div>
    `,
    data() {
        return {
            resultsShow: false
        }
    }
});
Vue.component('cart-touch',{
    components: {
        vuescroll
    },
    template: `
        <div class="cart-wrap__results">
                    <div class="cart-wrap__results__top-line">
                        <vuescroll>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                            <cart-item></cart-item>
                        </vuescroll>
                    </div>
                    <div class="cart-wrap__results__bottom-line">
                        <div class="full-summ">
                            <div class="full-summ__title">Итого:</div>
                            <span>42 999</span>
                            грн.
                        </div>
                        <a href="javascript:void(0)" class="button small">Перейти в корзину</a>
                        <a href="javascript:void(0)" class="to-order right-arrow">Оформить заказ</a>
                    </div>
                </div>
    `,
    data() {
        return {

        }
    }
});
Vue.component('search',{
    components: {
        vuescroll
    },
    template: `
        <div class="search-wrap" :class="{focused: focused}">
            <form class="search" action="/">
                <input type="text" @focus="focused = true">
                <label class="search__sumbit">
                    <input type="submit">
                </label>
            </form>
            <transition name="opacity-transition">
            <div class="search-wrap__results" v-if="resultsShow">
                <div class="search-wrap__results__top-line">
                     <div class="similar">
                        <a href="javascript:void(0)">Босоножки</a>
                        <div class="count">125</div>
                     </div>  
                     <div class="similar">
                        <a href="javascript:void(0)">Ботинки, сапоги</a>
                        <div class="count">154</div>
                     </div>
                </div>
                <div class="search-wrap__results__middle-line">
                    <vuescroll>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                        <search-result-item></search-result-item>
                    </vuescroll>
                </div>
                <div class="search-wrap__results__bottom-line">
                    <div class="all-results-wrap">
                        <a href="javascript:void(0)" class="all-results right-arrow">Все результаты</a>
                        <div class="count">255</div>
                    </div>
                </div>
            </div>
            </transition>
        </div>
    `,
    data() {
        return {
            resultsShow: false,
            focused: false
        }
    },
    methods: {

    },
    watch: {
        focused: function (val) {
            let self = this;
            if (val){
                setTimeout(function () {
                    self.resultsShow = true;
                },500);
            } else {
                self.resultsShow = false;
            }
        }
    },
    created: function () {
        let self = this;
        window.addEventListener('click', function(e){
            if (!self.$el.contains(e.target)){
                self.focused = false
            }
        })
    }
});
Vue.component('cart-item',{
    template: `
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                     <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <div class="cart-item__top-info" v-if="type==='full'">3 мес. – 3 года   TM Biomecanics</div>
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params">р.24 ,красный</div>
                                </div>
                                 <div class="cart-item__one-price" v-if="type==='full'">
                                    <span>42 999</span> грн
                                </div>
                                <div class="cart-item__count">
                                    <div class="minus" @click="minus">-</div>
                                    <input type="text" v-model="count">
                                    <div class="plus" @click="plus">+</div>
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                                <div class="cart-item__remove" @click="remove"></div>
                            </div>
   `,
    data(){
        return {
            count: 1
        }
    },
    methods: {
        minus(){
            if (this.count > 1){
                this.count = this.count - 1;
            }
        },
        plus(){
            this.count = this.count + 1;
        },
        remove(){
            alert('удаляем товар')
        }
    },
    props: ['type']

});
Vue.component('search-result-item',{
    template: `
          <div class="result">
                <a href="javascript:void(0)" class="result__item-img">
                    <img src="img/temp/result-img.jpg" alt="">
                </a>
                <div class="result__info">
                    <a href="javascript:void(0)" class="result__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                    <div class="result__info__params">р.24 ,красный</div>
                </div>
                <div class="result__price">
                    <div class="old">
                        <span>42 999</span> грн
                    </div>
                    <div class="new">
                        <span>41 999</span> грн
                    </div>
                </div>
          </div>
   `,
    data(){
        return {

        }
    }
});
Vue.component('slide-box',{
    template: `
        <div class="slide-box">
            <div :class="trigger" @click="show">
                <span>{{value}}</span>
            </div>
            <transition name="opacity-transition" v-cloak>
                <div :class="'backlayer type-'+direction" v-if="visible" @click="hide"></div>
            </transition>
            <transition :name="direction" v-cloak>
                <div :class="direction" v-if="visible">
                    <div class="close" v-if="direction == 'top'" @click="hide"></div>
                    <div class="slide-box__content">
                       <slot></slot>
                    </div>
                </div>
            </transition>
        </div>
    `,
    data(){
        return {
            visible: false
        }
    },
    props: ['trigger', 'direction','value'],
    methods: {
        show(){
            this.visible = true
            this.$root._data.isScrollDisable = true;
        },
        hide(){
            this.visible = false;
            this.$root._data.isScrollDisable = false;
        }
    }
});
Vue.component('next-level',{
    template: `
        <li :class="{'decoration-bottom':decoration}">
            <slot name="link">
            
            </slot>
            <span class="next-level" @click="next"></span>
            <transition name="opacity-transition">        
                    <slot name="list" v-if="toggle">
                        
                    </slot>
            </transition>
        </li>
    `,
    data(){
        return{
            toggle: false,
        }
    },
    props: ['decoration'],
    methods: {
        next(){
            this.toggle=true;
            this.$root.nextLevelContext = this;
            this.$root.nextLevelText = this.$slots.link[0].children[0].text;
            let ParentUl = this.$parent.$el.getElementsByTagName("ul")[0];
            ParentUl.style.cssText = "height: 0; overflow: hidden"
        },
        back(){
            this.toggle=false;
            let ParentUl = this.$parent.$el.getElementsByTagName("ul")[0];
            ParentUl.style.cssText = "height: auto; overflow: visible"
            if (this.$parent.$slots.link){
                this.$root.nextLevelContext = this.$parent;
                this.$root.nextLevelText = this.$parent.$slots.link[0].children[0].text;
            } else {
                this.$root.nextLevelContext = null;
            }

        }
    }
});
Vue.component('dropdown-text',{
   template: `
    <div class="dropdown-text">
         <div class="dropdown-text__content" :class="{open : toggle}">
            <slot></slot>
         </div>
        <div class="dropdown-text__trigger" v-if="!toggle" @click="toggle=!toggle">{{open}}</div>
         <div class="dropdown-text__trigger open" v-if="toggle" @click="toggle=!toggle">{{close}}</div>
         
    </div>
   `,
    data(){
       return {
           toggle: false
       }
    },
    props: ['open','close']
});
Vue.component('catalog-dropdown',{
   template: `
    <div class="catalog-menu">
        <div class="catalog-menu__trigger" :class="{disabled: initialVisible}" @click="toggle=!toggle">
                <span>{{trigger}}</span>
        </div>
        <transition name="opacity-transition">
            <slot v-if="toggle"></slot>
        </transition>
    </div>
   `,
    data(){
       return{
           toggle: !!this.initialVisible
       }
    },
    props: ['trigger', 'initialVisible'],
});
Vue.component('accordeon-item', {
    template: `
        <div class="accordeon-item">
            <div class="accordeon-item__title" :class="{open: toggle}" @click="toggle=!toggle">
                {{title}}
            </div>
            <transition name="opacity-transition">
                <div class="accordeon-item__content" v-if="toggle">
                    <slot></slot>
                </div>
            </transition>
        </div>
    `,
    data(){
        return {
            toggle: false
        }
    },
    props: ['title']
});
Vue.component('order',{
   template: `
        <div class="order">
            <div class="order__top-line" @click="toggle = !toggle" :class="{open: toggle}">
                <div class="number">№ {{number}}</div>
                <div class="date">{{date}}</div>
                <div class="info">{{count}} тов. на {{summ}} грн.</div>
                <div class="status" :class="statusColor">{{status}}</div>
            </div>
            <transition name="opacity-transition">
                <div class="order__bottom-line" v-if="toggle">
                    <slot></slot>
                </div>
            </transition>
        </div>
   `,
   data(){
       return {
            toggle: false
       }
   },
   props: ['number', 'date', 'count', 'summ', 'status','statusColor']
});
Vue.component('form-container',{
    template: `
        <div class="form-container">
            <div class="add-delivery-methods__trigger" @click="toggle=!toggle">{{trigger}}</div>
            <transition name="opacity-transition">
                <div class="form-container__content" v-if="toggle">
                    <slot></slot>
                </div>
            </transition>
        </div>
    `,
    data(){
        return {
            toggle: false
        }
    },
    props: ['trigger']
});
Vue.component('toggle-box',{
   template: `
        <div class="toggle-box">
            <div class="toggle-box__trigger" @click="toggle=!toggle">{{trigger}}</div>   
            <transition name="opacity-transition">
                <div class="toggle-box__content" v-if="toggle">
                    <slot></slot>
                </div> 
            </transition>
        </div>
   `,
    data(){
      return{
          toggle: false
      }
    },
    props: ['trigger']
});
Vue.component('filter-box',{
   template: `
    <div class="filter-box" :class="{open: toggle}">
        <div class="filter-box__trigger" :class="{open: toggle}" @click="toggle=!toggle">{{trigger}}</div>
        <transition name="opacity-transition">
            <div class="filter-box__content" v-if="toggle">
                <slot></slot>
            </div>
        </transition>
    </div>
   `,
    data(){
      return{
          toggle: false
      }
    },
    props: ['trigger']
});
Vue.component('range-slider',{
    template: `
        <div class="range-slider">
                <div class="slider" ref="slider"></div>
                <div class="slider-inputs-wrap">
                    <span class="label">от</span>
                    <input type="text" class="min" v-model="$root._data.filter.price[0]" @change="setRangeMin">
                    <span class="label">до</span>
                    <input type="text" class="max" v-model="$root._data.filter.price[1]" @change="setRangeMax">
                </div>
        </div>
   `,
    data(){
        return {
            sliderInstance: null,
        }
    },
    methods: {
        setRangeMin: function () {
            this.sliderInstance.set([this.$root._data.filter.price[0], null]);
        },
        setRangeMax: function () {
            this.sliderInstance.set([null, this.$root._data.filter.price[1]]);
        },
        setRangeMinMax: function () {
            this.sliderInstance.set([this.$root._data.filter.price[0], this.$root._data.filter.price[1]]);
        }
    },
    mounted: function () {
        let slider = this.$refs.slider;
        this.sliderInstance = noUiSlider.create(slider, {
            start: this.$root._data.filter.price,
            connect: true,
            step: 1,
            tooltips:  false,
            range: {
                'min': this.$root._data.filter.MinMaxPrice[0],
                'max': this.$root._data.filter.MinMaxPrice[1]
            },
            format: wNumb({
                decimals: 0,
                // suffix: ' грн.'
            }),
        });
        let self = this;
        this.sliderInstance.on('update', function () {
            self.$root._data.filter.price = self.sliderInstance.get()
        });
    },
});
Vue.component("modal", {
    template:`
        <transition-group name="opacity-transition">
            <div key="bg" class="backlayer" v-if="toggle" @click="hide"></div>
            <div key="m" class="modal__content" v-if="toggle" :class="{tight: type === 'tight'}">
                <div class="modal__close" @click="hide"></div>
                <slot></slot>
            </div>
        </transition-group>
    `,
    data() {return {toggle: false}},
    methods: {
        show: function () {
            this.toggle = true;
            this.$parent._data.isScrollDisable = true;
        },
        hide: function () {
            this.toggle = false;
            this.$parent._data.isScrollDisable = false;
        }
    },
    props: ['type']
});
Vue.use(vuescroll);
Vue.prototype.$vuescrollConfig = {
    bar: {
        keepShow: true,
        opacity: 1,
        onlyShowBarOnScroll: false,
        specifyBorderRadius: false,
        size: '10px',
        background: '#939393',
    },
    rail: {
        keepShow: false,
        disable: false,
        opacity: 1,
        background: '#F5F5F5',
        size: '10px'
    },
};
Vue.use(window.vuelidate.default);
Vue.use(VueTheMask);
Vue.component('v-select', VueSelect.VueSelect);
const { required, minLength, email } = window.validators;
let App = new Vue({
    el: '#app',
    components: {
        VueSelect,  vuescroll
    },
    validations: {
        subscribe: {
            email: {
                required,
                email
            },
        },
        contact: {
            name: {
                required
            },
            email: {
                required,
                email
            },
            message: {
                required
            },
        },
        profile: {
            name: {
                required
            },
            email: {
                required,
                email
            },
            address: {
                required
            }
        },
        child: {
            name: {required},
            date: {required},
        },
        order: {
            number: {
                required
            }
        },
        callback: {
          name: {
              required
          }
        },
        guestbook: {
            name: {
                required
            },
            review: {
                required
            },
        },
        delivery: {
            name: {
                required
            },
            flat: {
                required
            },
            house: {
                required
            },
            email: {
                required,
                email
            },
            address: {
                required
            },
            city: {
                required
            }
        },
    },
    data: {
        isScrollDisable: false,
        personalListVisible: false,
        catalogMenuTouchVisible: false,
        nextLevelText: null,
        nextLevelContext: null,
        filterBrand: '',
        subscribe: {
            email: ''
        },
        sorting: 'сначала с акциями',
        contact: {
            name: '',
            email: '',
            message: '',
            theme: '',
        },
        profile: {
            name: '',
            email: '',
            address: '',
            phone: ''
        },
        child: {
          sex: 'male',
          name: '',
          date: '',
        },
        guestbook: {
            name: '',
            review: '',
        },
        order: {
          number: ''
        },
        delivery: {
            name: '',
            email: '',
            address: '',
            city: '',
            phone: '',
            flat: '',
            house: '',
            recipient: 'self'
        },
        ordering__delivery: {
            provider: 'newpost',
            type: '',
            city: '',
            address: '',
            payment: 'Безналичный расчет',
            comment: '',
            nocall: true
        },
        callback: {
            name: '',
            phone: ''
        },
        filter: {},
        brandList: [],
        clearFilter: {}
    },
    methods: {
        resetPrice(){
            if (this.$refs.range){
                this.$refs.range.setRangeMinMax();
            }
        },
        debounceFilter: function(event) {
            clearTimeout(this.debounce)
            this.debounce = setTimeout(() => {
                this.filterBrand = event.target.value;
                this.filterBrandRequest();
            }, 600)
        },
        filterBrandRequest: function () {
            axios
                .get('../brandRequestExample.json',
                    {
                        params: {
                            query: this.filterBrand,
                        }
                    }
                )
                .then(response => (
                    this.brandList = response.data.result
                ));
        },
        removeFromArr(index1, index){
            this.filter[index1].splice(index, 1);
        },
        touchDetect() {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },
        backLevel(){
            this.nextLevelContext.back();
        },
        recipient(a){
            if (a === 'self') {
                this.delivery.name = this.profile.name;
                this.delivery.email = this.profile.email;
                this.delivery.phone = this.profile.phone;
            } else {
                this.delivery.name = '';
                this.delivery.email = '';
                this.delivery.phone = '';
            }
        },
        reset(){
            if (this.delivery.recipient === 'self'){
                this.delivery.name = this.profile.name;
                this.delivery.email = this.profile.email;
                this.delivery.phone = this.profile.phone;
            } else {
                this.delivery.name = '';
                this.delivery.email = '';
                this.delivery.phone = '';
            }
            this.delivery.city = '';
            this.delivery.flat = '';
            this.delivery.house = '';
            this.delivery.recipient = 'self';
        }
    },
    mounted(){
        if (this.delivery.recipient === 'self'){
            this.delivery.name = this.profile.name;
            this.delivery.email = this.profile.email;
            this.delivery.phone = this.profile.phone;
        }
    },
    watch: {
        isScrollDisable: function(val) {
            if(val){
                document.body.style.overflow = 'hidden';
                return
            }
            document.body.style.overflow = 'auto';
        },
    },
    created: function () {
        this.isTouch = this.touchDetect();
    }
});
let banner = new Swiper('.swiper-container.banner', {
    speed: 600,
    navigation: {
        nextEl: '.banner .swiper-button-next',
        prevEl: '.banner .swiper-button-prev',
    },
});
let profitable = new Swiper('.profitable-slider', {
    speed: 600,
    spaceBetween: 16,
    breakpoints: {
        798: {
            slidesPerView: 2,
        },
        1024: {
            slidesPerView: 1,
        },
        1280: {
            slidesPerView: 2,
        },
        1366: {
            slidesPerView: 3,
        },
    },
    navigation: {
        nextEl: '.profitable-slider > .swiper-button-next',
        prevEl: '.profitable-slider > .swiper-button-prev',
    },
});
let catalogSlider = new Swiper('.catalog-slider', {
    speed: 600,
    spaceBetween: 16,
    breakpoints: {
        798: {
            slidesPerView: 2,
        },
        1024: {
            slidesPerView: 3,
        },
        1280: {
            slidesPerView: 4,
        },
        1366: {
            slidesPerView: 5,
        },
    },
    navigation: {
        nextEl: '.catalog-slider > .swiper-button-next',
        prevEl: '.catalog-slider > .swiper-button-prev',
    },
});
let topSlider = new Swiper('.top-slider', {
    speed: 600,
    navigation: {
        nextEl: '.top-slider > .swiper-button-next',
        prevEl: '.top-slider > .swiper-button-prev',
    },
});
let brandsSlider = new Swiper('.brands-slider', {
    speed: 600,
    breakpoints: {
        798: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 5,
        },
        1280: {
            slidesPerView: 6,
        },
        1366: {
            slidesPerView: 7,
        },
    },
    navigation: {
        nextEl: '.brands-slider > .swiper-button-next',
        prevEl: '.brands-slider > .swiper-button-prev',
    },
});
let brandsSliderSmall = new Swiper('.brands-slider-small', {
    speed: 600,
    breakpoints: {
        798: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
        1280: {
            slidesPerView: 4,
        },
        1366: {
            slidesPerView: 5,
        },
    },
    navigation: {
        nextEl: '.brands-slider-small > .swiper-button-next',
        prevEl: '.brands-slider-small > .swiper-button-prev',
    },
});
let newsSlider = new Swiper('.news-slider', {
    speed: 600,
    spaceBetween: 16,

    breakpoints: {
        798: {
            slidesPerView: 2,
        },
        1024: {
            slidesPerView: 3,
        },
        1280: {
            slidesPerView: 4,
        },
    },
    navigation: {
        nextEl: '.news-slider > .swiper-button-next',
        prevEl: '.news-slider > .swiper-button-prev',
    },
});
let galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 5,
    slidesPerView: 5,
    watchSlidesVisibility: true,
    direction: 'vertical',
});
let galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs
    }
});
baguetteBox.run('.gallery', {
    noScrollbars: true,
    buttons: true
});








