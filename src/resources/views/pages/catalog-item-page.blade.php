@extends('layouts._layout')
@section('content')
    <div class="wrap">
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[0]->url)}}" itemprop="item">
                            <span itemprop="name">{{$menu->where('url','/')->first()->name}}</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($catalogp['url'])}}" itemprop="item">
                            <span itemprop="name">{{$catalogp['namet']}}</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($categoryp['url'])}}" itemprop="item">
                            <span itemprop="name">{{$categoryp['namet']}}</span>
                        </a>
                        <meta itemprop="position" content="3">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($sub_category['url'])}}" itemprop="item">
                            <span itemprop="name">{{$sub_category['namet']}}</span>
                        </a>
                        <meta itemprop="position" content="4">
                    </li>
                     <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($sub_category['url'])}}" itemprop="item">
                            <span itemprop="name">{{$products['name']}}</span>
                        </a>
                        <meta itemprop="position" content="5">
                    </li>
              
			   </ul>
                <h1>{{$products['name']}}</h1>
                <img src="img/temp/product-logo.jpg" alt="" class="product-logo">
                <div class="product-page-wrap">
                    <div class="column">
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="{{asset('storage/'.$products['scr'])}}" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper gallery">
                                <a href="{{asset('storage/'.$products['scr'])}}" class="swiper-slide">
                                    <img src="{{asset('storage/'.$products['scr'])}}" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <span class="item-type">
                              @if($products->is_super_price) 
							  <span class="item-type__price">Суперцена</span>
                              @endif
							  @if($products->is_new)
							  <span class="item-type__new">Новинка</span>
                              @endif
						</span>
                        </div>
                        <div class="top-slider-wrap">
                            <div class="swiper-container top-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="_f _j-between _mb-30 _i-center">
                            @if(count($product_count->where('product_id',$products->id)->all()))
						<div class="product-status">В наличии</div>
					    @else	
						<div class="product-status">Нет в наличии</div>
						@endif
                            <div class="vendor-code">Артикул: {{$products->artikul}}</div>
                        </div>
                        <div class="product-price">
                            @if($products->price_old<>0)
							<div class="old"><span>{{$products->price_old}}</span> грн.</div>
                            @endif
							<div class="new"><span>{{$products->price}}</span> грн.</div>
                        </div>
                        <div class="product-params">
                                @if($products->is_color) <?php $i='0';?>
								 <div class="product-params__column">
								 <div class="params-item__title">Цвет:</div>
                                <div class="_f _f-wrap">
                                  @foreach($product_count->where('product_id',$products->id)->sortBy('color')->all() as $pr) 
								   @if($pr->color<>$i) <?php $i=$pr->color;?>
								   <label class="color-item">
                                        <input type="radio" name="color" value="{{$pr->color}}">
                                        <span class="color" style="background-color:{{$pr->color}}"></span>
                                    </label>
                                 @endif
							    @endforeach
							   </div>
                              </div>
							  @endif
							  @if($products->is_size) <?php $i='0';?>
							  <div class="product-params__column">
                                <div class="params-item__title">Размер:</div>
                                <a href="javascript:void(0)" class="size-info-link">Таблица размеров</a>
                                <div class="_f _f-wrap">
                                    @foreach($product_count->where('product_id',$products->id)->sortBy('size')->all() as $pr)
                                   @if($pr->size<>$i) <?php $i=$pr->size;?>
									<label class="size-item">
                                        <input type="radio" name="size" value="26">
                                        <span class="size">{{$pr->size}}</span>
                                    </label>
                                    @endif
					                @endforeach
                               </div>
                            </div>
                             @endif
						</div>
                        <div class="buttons-wrap">
                            <a href="javascript:void(0)" class="button buy red _upper">Купить</a>
                            <a href="javascript:void(0)" class="button to-favorites _upper">Добавить в избранное</a>
                        </div>
                        <div class="product-accordeon-wrap">
                            <accordeon-item
                                    v-cloak
                                    title="Характеристики"
                            >
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                                <div class="characteristic-item">
                                    <div class="characteristic-item__title">Производитель ТМ:</div>
                                    <div class="characteristic-item__value">Super Fit</div>
                                </div>
                            </accordeon-item>
                            <accordeon-item
                                    v-cloak
                                    title="Описание товара"
                            >
                                <p>Вариант для тех, кто хочет быть модной и везде успевать. Просто выбирай свой цвет, который идеально подойдет к лицу, волосам и глазам. Надевай комплект и мотивируй других влюбиться навсегда в «спорт стайл» — свободный, удобный и очень стильный. Еще лучше заказать сразу несколько цветных костюмов. Их можно сочетать между собой и получать новые яркие комбинации.</p>
                            </accordeon-item>
                            <accordeon-item
                                    v-cloak
                                    title="Оплата и доставка"
                            >
                                <div class="accordeon-content-title">Доставка:</div>
                                <div class="product-delivery-wrap">
                                    <img src="img/img/new-post.svg" alt="">
                                    <img src="img/img/ukr-post.svg" alt="">
                                </div>
                                <div class="accordeon-content-title">Оплата:</div>
                                <div class="payment-stages-wrap">
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/sprite.svg#payment-1" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                <span>
                                        При получении
                                </span>
                                            <div class="hover-info">
                                                <div class="hover-info__trigger">?</div>
                                                <div class="hover-info__text">
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/sprite.svg#payment-2" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                            Банковской картой
                                        </div>
                                    </div>
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/sprite.svg#payment-3" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                            Безналичный расчет
                                            <div class="hover-info">
                                                <div class="hover-info__trigger">?</div>
                                                <div class="hover-info__text">
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="payment-stages-item">
                                        <div class="payment-stages-item__item-img">
                                            <img src="img/img/liqpay.svg" alt="">
                                        </div>
                                        <div class="payment-stages-item__title">
                                            Картой на сайте через LIQPAY
                                            <div class="hover-info">
                                                <div class="hover-info__trigger">?</div>
                                                <div class="hover-info__text">
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                    <p>Пример текста при наведении</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </accordeon-item>
                        </div>
                    </div>
                </div>
                <div class="h1">Похожие товары</div>
                <div class="swiper-container catalog-slider slider-arrows-top">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                        <div class="swiper-slide">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                        </div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="green-border-wrap">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>


            </div>
        </div>
 
    </div>
@stop 
