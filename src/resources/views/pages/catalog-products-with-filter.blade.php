@extends('layouts._layout')
@section('content')
    <div class="wrap">
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[0]->url)}}" itemprop="item">
                            <span itemprop="name">{{$menu->where('url','/')->first()->name}} </span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($catalogp['url'])}}" itemprop="item">
                            <span itemprop="name">{{$catalogp['namet']}}</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
					@if($page<>'filtr')
                     <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($categoryp['url'])}}" itemprop="item">
                            <span itemprop="name">{{$categoryp['namet']}}</span>
                        </a>
                        <meta itemprop="position" content="3">
                    </li>
					@endif
                   @if($page=='f')
				   <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($sub_category['url'])}}" itemprop="item">
                            <span itemprop="name">{{$sub_category['namet']}}</span>
                        </a>
                        <meta itemprop="position" content="3">
                    </li>
                @endif
                 </ul>
                <div class="catalog-filter-wrap">
                    <div class="catalog-filter type-2">
                        <div class="def-down-show">
                            <slide-box
                                    v-cloak
                                    trigger="filter-trigger button"
                                    direction="left"
                                    value="Фильтр каталога"
                            >
                                <div class="touch-menu">
                                    <vuescroll>
                                        <filter-box
                                                v-cloak
                                                trigger="Категория"
                                        >
                                            <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                                </li>
                                            </ul>
                                        </filter-box>}
                                        <filter-box
                                                v-cloak
                                                trigger="Цена"
                                        >
                                            <range-slider ref="range"></range-slider>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Пол"
                                        >
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="female"  v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Девочка</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="male"  v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Мальчик</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="all" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Мальчик/Девочка</div>
                                            </label>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Возраст"
                                        >
                                   <label class="validation-field check-wrapper">
                                        <input type="radio" value="all" name="ages" onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?agefrom_0/f/?ageto_18'}}"' v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">Все</div>
                                    </label>
									@foreach($age as $ag)
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="{{$ag->from.'-'.$ag->to}}" name='ages' onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?agefrom_'.$ag->from.'/f/?ageto_'.$ag->to}}"'  v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">{{$ag->from.'-'.$ag->to}} лет</div>
                                    </label>
									@endforeach
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Торговая марка"
                                        >
                                            <label class="validation-field">
                                                <input type="text" placeholder="Поиск по бренду" @input="filterBrand($event.target.value)">
                                            </label>
                                            <label class="validation-field check-wrapper" v-for="(item, index) in brandList" :key="index">
                                                <input type="checkbox" :value="item.value" v-model="filter.brand">
                                                <div class="check"></div>
                                                <div class="input-title">{!!'{{item.title}}'!!} <span>{!!'{{item.count}}'!!}</span></div>
                                            </label>
                                        </filter-box>
                                    </vuescroll>
                                </div>
                            </slide-box>
                        </div>
                        <div class="def-down-hide">
 									    @if($page=='s'or $page=='filtr')
									<filter-box
                                    v-cloak
                                    trigger="Категория"
                            >
                                <vuescroll>
                                    <ul class="filter-box__list">
                                        @foreach($sub_category as $sub)
										<li>
                                          <a href="{{$_SERVER['REQUEST_URI'].'/f/?category_'.$sub['id']}}">{{$sub['namet']}} <span>{{count($product_sub[$sub->id])}}</span></a> 
                                        </li>
                                        @endforeach
									</ul>
                                </vuescroll>
                            </filter-box>
							@endif
                            <filter-box
                                    v-cloak
                                    trigger="Цена"
                            >
                                <vuescroll>
                                    <range-slider ref="range"></range-slider>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Пол"
                            >
                                <vuescroll>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="female" onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?sex_g'}}"' v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Девочка</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="male" onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?sex_b'}}"' v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Мальчик</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="all" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Мальчик/Девочка</div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Возраст"
                            >
                                <vuescroll>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="all" name="ages" onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?agefrom_0/f/?ageto_18'}}"' v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">Все</div>
                                    </label>
									@foreach($age as $ag)
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="{{$ag->from.'-'.$ag->to}}" name='ages' onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?agefrom_'.$ag->from.'/f/?ageto_'.$ag->to}}"'  v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">{{$ag->from.'-'.$ag->to}} лет</div>
                                    </label>
									@endforeach
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Торговая марка"
                            >
                                <label class="validation-field">
                                    <input type="text" placeholder="Поиск по бренду" :value="filterBrand" @input="debounceFilter">
                                </label>
                                <vuescroll>
      <!--                                <label class="validation-field check-wrapper" v-for="(item, index) in brandList" :key="index"> -->
                                      @foreach($brand as $br)                                    
									 <label class="validation-field check-wrapper">
		<!--							 <input type="checkbox" :value="item.value" v-model="filter.brand"> -->
                                       <input type="checkbox" value="{{$br->id}}" onchange='location="{{$_SERVER['REQUEST_URI'].'/f/?brand_'.$br->id}}"' v-model="filter.brand"> 
									   <div class="check"></div>
        <!--                                <div class="input-title">{!!'{{item.title}}'!!} <span>{!!'{{item.count}}'!!}</span></div> -->
                                    <div class="input-title">{{$br->name}} <span>Пока не считаю</span></div>
									</label>
									@endforeach
                                </vuescroll>
                            </filter-box>
                        </div>
                    </div>
                    <div class="catalog-wrap">
                        <div class="sorting-wrap">
                            <div class="child-products">
                                <div class="child-products__item-img">
                                    <img src="img/img/child.svg" alt="">
                                </div>
                                <div class="child-products__info">
                                    <div class="child-products__title">Товары для вашего ребенка</div>
                                    <span>
                                     <a href="javascript:void(0)" class="child-products__add-link">
                                    <span>Информация о ребенке</span>
                                </a>
                                     <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </span>

                                </div>
                            </div>
                            <div class="sorting">
                                <div class="sorting__title">Сортировать по:</div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="sorting"
                                            placeholder="Сортировка"
                                            :options="
                                        [
                                            'сначала с акциями',
                                            'сначала без акций',
                                            'по возрастанию цены'
                                        ]" >
                                    </v-select>
                                </div>
                            </div>
                        </div>
                        <div class="selected">
                            <div class="selected__title">Вы выбрали:</div>
 	<!--						<template v-for="(item1, index1) in filter">
                                <div class="selected__item" v-for="(item, index) in item1" :key="item" v-if="index1 !== 'MinMaxPrice' && index1 !== 'price' && index1 !== 'age'" @click="removeFromArr(index1, index)">
								{!!'{{item}}'!!}
                                </div>
                                <div class="selected__item" v-if="index1 === 'age' && item1 !== ''" @click="filter[index1] = ''">
								{!!'{{item1}}'!!}
                                </div>
                            </template>
							-->
							@foreach($select  as $key=>$value)
							<div class="selected__item" >
							{{$key}}: {{$value}}
							</div>
							@endforeach 
  <!--                          <div class="selected__remove" @click="filter = {...clearFilter}; resetPrice();">Сбросить все</div> -->
                        <div class="selected__remove">
						<button onclick='location="{{stristr($_SERVER['REQUEST_URI'],'/f/?',true)}}"'>Сбросить все</button></div> 
						</div>
                        <div class="area-catalog-wrap">
    @foreach($products as $product)
                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="{{asset('storage/'.$product->scr)}}" alt="">
        <span class="item-type">
			@if($product->is_super_price) 
		 <span class="item-type__price">Суперцена</span>
	  		@endif
			@if($product->is_new)
            <span class="item-type__new">Новинка</span>
			@endif
        </span>
    </a>
    <div class="catalog-item__info">
          <div class="catalog-item__description">
	   @if($product->agefrom<>'-')
	   {{stristr($product->agefrom,'.')?intval(substr($product->agefrom,2)):$product->agefrom}} – {{stristr($product->ageto,'.')?intval(substr($product->ageto,2)).'мес.':$product->ageto.'лет'}} @endif   TM {{$brand[$product->brand_id-1]->name}}</div>
               <a href="{{LaravelLocalization::localizeUrl($product['url'])}}" class="catalog-item__title">{{$product->name}}</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>{{$product->price_old==0 ?' ':$product->price_old}}</span> {{$product->price_old==0 ?' ':'грн'}}</div>
                <div class="new"><span>{{$product->price}}</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
				@if($product->is_color) <?php $i='0';?>
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
			@foreach($product_count->where('product_id',$product->id)->sortBy('color')->all() as $pr)
                    @if($pr->color<>$i) <?php $i=$pr->color;?>
					<label class="color-item">
                        <input type="radio" name="color" value="{{$pr->color}}">
                        <span class="color" style="background-color: {{$pr->color}}"></span>
                    </label>
					@endif
            @endforeach
                </div>
 				@endif 
           </div>
               <div class="params-item">
				@if($product->is_size) <?php $i='0';?>
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
 			        @foreach($product_count->where('product_id',$product->id)->sortBy('size')->all() as $pr)
                     @if($pr->size<>$i) <?php $i=$pr->size;?>
                    <label class="size-item">
                        <input type="radio" name="size" value="{{$pr->size}}">
                        <span class="size">{{$pr->size}}</span>
                    </label>
					@endif
					@endforeach
                </div>
 				@endif
            </div>
       </div>
        <div class="hover-dropdown__bottom-line">
  	    @if(count($product_count->where('product_id',$product->id)->all()))
	<div class="status green"> В наличии</div>
   	   @else
	<div class="status red">Нет в наличии</div>  <!-- или класс red если не в наличии -->
		@endif
            <div class="vendor-code">Артикул: {{$product->artikul}}</div>
        </div>
    </div>
</div>
        @endforeach      
                        </div>
                        <div class="pagination-wrap">
                            <a href="javascript:void(0)" class="load-more">показать еще 30</a>
                            <ul class="pagination">
                                <li class="prev">
                                    <a href="javascript:void(0)"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">1</a>
                                </li>
                                <li class="active">
                                    <a href="javascript:void(0)">2</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">...</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">15</a>
                                </li>
                                <li class="next">
                                    <a href="javascript:void(0)"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap _mb-110">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="about-us">
                    <div class="about-us__description-wrap">
                        <dropdown-text
                                open="Читать все"
                                close="Читать меньше"
                        >
                            <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                        </dropdown-text>
                    </div>
                    <div class="about-us__form-wrap">
                        <div class="title">Узнавайте об акциях первыми</div>
                        <form action="/">
                            <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                            </label>
                            <label class="submit" :class="{ disable: $v.$anyError }">
                                <input type="submit" :disabled="$v.$anyError">
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    App._data.filter = {
        brand: ['Alisa Line2'],
        sex: ['all'],
        age: '0-1',
        price: [150, 1985],
        MinMaxPrice: [0, 2500]
    };
    App._data.clearFilter = {
        brand: [],
        sex: [],
        age: '',
        price: App._data.filter.MinMaxPrice,
        MinMaxPrice: App._data.filter.MinMaxPrice
    };
    App._data.brandList = [
        {
            "title": "Alisa Line",
            "count": "124",
            "value": "Alisa Line"
        },
        {
            "title": "Alisa Line2",
            "count": "224",
            "value": "Alisa Line2"
        },
        {
            "title": "Alisa Line3",
            "count": "324",
            "value": "Alisa Line3"
        },
        {
            "title": "Alisa Line32",
            "count": "310",
            "value": "Alisa Line32"
        }
    ]
</script>
@stop
