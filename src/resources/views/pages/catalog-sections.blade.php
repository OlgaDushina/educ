@extends('layouts._layout')
@section('content')
    <div class="wrap">
        <div class="content">
            <div class="container">
			<!--  print_r($catalog); ?> -->
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[0]->url)}}" itemprop="item">
                            <span itemprop="name">{{$menu->where('url','/')->first()->name}}</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($catalogp['url'])}}" itemprop="item">
          <!--                  <span itemprop="name"> {$catalog->where('url','/catalog/'.(explode('/',$_SERVER['REQUEST_URI'])[3]))->first()->namet}</span> -->
                             <span itemprop="name">{{$catalogp['namet']}}</span>
                    	</a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="catalog-filter-wrap">
                    <div class="catalog-filter">
                        <div class="def-down-show">
                            <slide-box
                                    v-cloak
                                    trigger="filter-trigger button"
                                    direction="left"
                                    value="Фильтр каталога"
                            >
                                <div class="touch-menu">
								   <vuescroll>
									   <filter-box
                                                v-cloak
                                                trigger="Good morning!!!!"
                                        >
		                                        <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                            </ul>
                                        </filter-box>
										<filter-box
                                                v-cloak
                                                trigger="Ботинки, сапоги asd"
                                        >
                                            <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd </a>
                                                </li>
                                            </ul>
                                        </filter-box>
											
                                    </vuescroll>
                                </div>
                            </slide-box>
                        </div>
                       <div class="def-down-hide">
							  @foreach($categoryp as $cat2)  
                            <filter-box
                                    v-cloak
                                    trigger="{{$cat2->namet}}"
                            >
                                <vuescroll>
                                    <ul class="filter-box__list">
									@foreach($sub_category->where('category_id',$cat2->id)->all() as $cat3)
                                        <li>
                                            <a href="{{LaravelLocalization::localizeUrl($cat3->url)}}">{{$cat3->namet}}</a>
                                        </li>
                            										@endforeach
                                    </ul>
                                </vuescroll>
                            </filter-box>
								@endforeach	
                        </div>
                    </div>
					<div class="catalog-wrap">
                       @if($catalogp['id']>1)
					   <div class="sections-wrap">
                            <div class="sections-item w-2 h-1" style="background-color: #0275D2">
                                <span class="sections-item__title">{{$catalogp['namet']}} для</span>
                                <nav class="sections-img-wrap">
                                    <a href="{{LaravelLocalization::localizeUrl($catalogp['url'].'/f\/?sex_b')}}" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-boy.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Мальчика</span>
                                    </a>
                                    <a href="{{LaravelLocalization::localizeUrl($catalogp['url'].'/f\/?sex_g')}}" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-girl.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Девочки</span>
                                    </a>
                                    <a href="javascript:void(0)" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-child.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Малыша</span>
                                    </a>
                                    <a href="javascript:void(0)" class="sections-img-item">
                                    <span class="sections-img-item__item-img">
                                        <img src="img/img/sections-teenager.svg" alt="">
                                    </span>
                                        <span class="sections-img-item__title">Подростка</span>
                                    </a>
                                </nav>
                            </div>
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По возрасту</span>
                                <ul class="sections-item__list">
                                    @foreach($age as $ag)
									<li>
                                        <a href="{{$_SERVER['REQUEST_URI'].'/f/?agefrom_'.$ag->from.'/f/?ageto_'.$ag->to}}">{{$ag->from.'-'.$ag->to}} лет</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">По цене</span>
                                <ul class="sections-item__list">
                                    <li>
                                        <a href="{{$_SERVER['REQUEST_URI'].'/f/?price1_0/f/?price2_300'}}">0-300 грн</a>
                                    </li>
                                    <li>
                                        <a href="{{$_SERVER['REQUEST_URI'].'/f/?price1_300/f/?price2_500'}}">300-500 грн</a>
                                    </li>
                                    <li>
                                        <a href="{{$_SERVER['REQUEST_URI'].'/f/?price1_500/f/?price2_1000'}}">500-1000 грн</a>
                                    </li>
                                    <li>
                                        <a href="{{$_SERVER['REQUEST_URI'].'/f/?price1_1000/f/?price2_1500'}}">1000-1500 грн</a>
                                    </li>
                                    <li>
                                        <a href="{{$_SERVER['REQUEST_URI'].'/f/?price1_1500'}})"> свыше 1500 грн</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
						@endif
                        <div class="swiper-container brands-slider-small"> 
						<h2>Бренды    <a href="javascript:void(0)" class="swiper-container brands-slider-small">Все бренды
                                    </a> </h4>
                           <div class="swiper-wrapper">
                 		    @foreach($brand as $br)
                               <div class="swiper-slide">
                                    <a href="javascript:void(0)" class="brand-item">
                                       <img src="{{$br->logo}}" alt="">
                                  </a>
                                </div>
                            @endforeach
 						  </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <h1>{{$catalogp['namet']}}</h1>
                        <div class="sections-wrap">
						 @foreach($categoryp as $cat2) 
                            <div class="sections-item w-1 h-1" style="background-color: #F386C2">
                                <span class="sections-item__title">{{$cat2->namet}}</span>
                                <ul class="sections-item__list">
								@foreach($sub_category->where('category_id',$cat2->id)->all() as $cat3)
                                    <li>
                                        <a href="{{LaravelLocalization::localizeUrl($cat3->url)}}">{{$cat3->namet}}</a>
                                    </li>
                                 @endforeach
                                </ul>
                            </div>
							@endforeach
                        </div>
                    </div>
                </div>
                <div class="about-us">
                    <div class="about-us__description-wrap">
                        <dropdown-text
                                open="Читать все"
                                close="Читать меньше"
                        >
                            <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                        </dropdown-text>
                    </div>
                    <div class="about-us__form-wrap">
                        <div class="title">Узнавайте об акциях первыми</div>
                        <form action="/">
                            <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                            </label>
                            <label class="submit" :class="{ disable: $v.$anyError }">
                                <input type="submit" :disabled="$v.$anyError">
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
