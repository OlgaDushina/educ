<!DOCTYPE html>
<html lang="en">

<!--<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	@include('layouts.styles')   
    <title>Kidsanna</title>
</head> -->

<body >
@extends('layouts._layout')
@section('content')
    <div class="wrap"> 
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[0]->url)}}" itemprop="item">
                            <span itemprop="name">{{$menu->where('url','/')->first()->name}}</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[1]->url)}}" itemprop="item">
						 <span itemprop="name">{{$menu->where('url','/contacts')->first()->name}}</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="contacts-wrap">
                    <div class="contacts-wrap__info">
                        <h1>{{$menu->where('url','/contacts')->first()->name}}</h1>
                        <div class="contact-item big">Херсон, пр. Ушакова, 35-А</div>
							<div class="contact-item">{{$servp->where('var','( ТЦ "Адмирал", 1 этаж, детский магазин Kidsanna )')->first()->name}}</div> 
					   <a href="{{LaravelLocalization::localizeUrl('/map')}}" class="right-arrow">{{$servp->where('var','Показать на карте')->first()->name}}</a>
                        <a href="mailto:kidsanna.com@gmail.com" class="contact-item big">kidsanna.com@gmail.com</a>
                        <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">{{$servp->where('var','Заказать звонок')->first()->name}}</a>
    </div>
</div>
                        <nav class="social">
    <a href="javascript:void(0)" class="fb"><span class="icon"></span></a>
    <a href="javascript:void(0)" class="inst"><span class="icon"></span></a>
</nav>
                    </div>
                    <div class="contacts-wrap__form">
                        <form action="{{route('Contacts@post')}}" class="contacts-form" method="post">
						 @csrf
                            <div class="contacts-form-wrap">
                                <div class="column">
                                    <label class="validation-field" :class="{ hasvalue: contact.name, error: $v.contact.name.$error  }">
                                        <input v-model="$v.contact.name.$model" name="name" placeholder="{{$servp->where('var','Ваше имя*')->first()->name}}">
                                        <div class="error" v-if="!$v.contact.name.required && $v.contact.name.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                    <label class="validation-field" :class="{ hasvalue: contact.email, error: $v.contact.email.$error }">
                                        <input v-model="$v.contact.email.$model" name="email" placeholder="Email*">
                                        <div class="error" v-if="!$v.contact.email.email && $v.contact.email.$dirty ">Введите корректный e-mail</div>
                                        <div class="error" v-if="!$v.contact.email.required && $v.contact.email.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                    <div class="validation-field select-wrapper">
                                        <v-select
                                                v-model="contact.theme"
												name='department'
                                                placeholder="{{$servp->where('var','Выберите тему вопроса')->first()->name}}"
                                                :options="
                                        [
                                            '{{$servp->where('var','Техническая поддержка')->first()->name}}',
                                            '{{$servp->where('var','Отдел продаж')->first()->name}}',
                                            '{{$servp->where('var','Отдел рекламы')->first()->name}}'
                                        ]">
                                        </v-select>
										<input type="hidden" name='department' v-model="contact.theme">
                                    </div>
                                    <label class="button _upper" :class="{ disable: $v.$anyError }">
                                        {{$servp->where('var','Отправить')->first()->name}}
                                        <input type="submit" :disabled="$v.$anyError">
                                    </label>
                                </div>
                                <div class="column">
                                    <label class="validation-field" :class="{ hasvalue: contact.message, error: $v.contact.message.$error }">
                                        <textarea v-model="$v.contact.message.$model" name="message" placeholder="{{$servp->where('var','Ваше сообщение*')->first()->name}}" ></textarea>
                                        <div class="error" v-if="!$v.contact.message.required && $v.contact.message.$dirty">Поле обязательно для заполнения</div>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
	@stop
</body>
</html>