<!DOCTYPE html>
<html lang="en">

<!--<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	@include('layouts.styles')   
    <title>Kidsanna</title>
</head> -->

<body >
@extends('layouts._layout')
@section('content')
    <div class="wrap"> 
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[0]->url)}}" itemprop="item">
                            <span itemprop="name">{{$menu[0]->name}}</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[1]->url)}}" itemprop="item">
						 <span itemprop="name">{{$menu[1]->name}}</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="contacts-wrap">
                    <div class="contacts-wrap__info">
                        <h1>{{$menu[1]->name}}</h1>
                        <div class="contact-item big">Херсон, пр. Ушакова, 35-А</div>
				  <!--p print_r($servp);?>-->
					<div class="contact-item">{{$servp[0]->name}}</div> 
					   <a href="javascript:void(0)" class="right-arrow">{{$servp[1]->name}}</a>
                        <a href="mailto:kidsanna.com@gmail.com" class="contact-item big">kidsanna.com@gmail.com</a>
                        <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">{{$servp[2]->name}}</a>
    </div>
</div>
                        <nav class="social">
    <a href="javascript:void(0)" class="fb"><span class="icon"></span></a>
    <a href="javascript:void(0)" class="inst"><span class="icon"></span></a>
</nav>
                    </div>
     <div class="contacts-form-wrap">     
		    <h1>   <br><br><br> Спасибо!<br>Ваше сообщение отправлено в обработку!
		 <br>В ближайшее время постараемся Вам ответить!</h1>
      
      </div>	  
 </div>

            </div>
        </div>
   </div>
	@stop
</body>
</html>