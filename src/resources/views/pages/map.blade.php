<!DOCTYPE html>
<html lang="en">

<body >
@extends('layouts._layout')
@section('content')
<div id="app">
    <div class="wrap">
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu->where('url','/')->first()->url)}}" itemprop="item">
                            <span itemprop="name">{{$menu->where('url','/')->first()->name}}</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu->where('url','/contacts')->first()->url)}}" itemprop="item">
						 <span itemprop="name">{{$menu->where('url','/contacts')->first()->name}}</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
					                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl($menu[1]->url)}}" itemprop="item">
						 <span itemprop="name">Карта</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>

                </ul>
    <!--            <div class="text-content-gray"> -->
                    <div class="contacts-wrap">
                    <!--<h1>Kidsanna</h1> -->
                    <div class="map">
					<iframe src="https://www.google.com/maps/d/embed?mid=1NK8UjOVVSGvytZET4S44GM96E-g-t1MS" width="840" height="380"></iframe>	
					</div>
                </div>
           </div>
        </div>
        
    </div>
</div>
@stop
</body>
</html>