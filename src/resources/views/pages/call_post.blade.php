<!DOCTYPE html>
<html lang="en">

<body >
@extends('layouts._layout')
@section('content')
<div id="app">
    <div class="wrap">
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
               </ul>
                <div class="text-content-gray">
                    <h1>Благодарим Вас за обращение!</h1>
                    <p><h2>Постараемся перезвонить вам в ближайшие пять минут!<h2></p>
                      <a href="javascript:history.back()" class="button large _upper">Нажмите для продолжения</a>
                </div>
           </div>
        </div>
        
    </div>
</div>
@stop
</body>
</html>