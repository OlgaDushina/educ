<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        <header class="header">
    <div class="header__top-line">
        <div class="container">
            <div class="lang-switch">
                <a href="javascript:void(0)" class="lang-switch__item active">RU</a>
                <a href="javascript:void(0)" class="lang-switch__item">UA</a>
            </div>
            <ul class="nav">
                <li>
                    <a href="javascript:void(0)">Контакты</a>
                </li>
                <li>
                    <a href="javascript:void(0)">О нас</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Отзывы</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Полезные статьи</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Доставка и оплата</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Дисконтная программа</a>
                </li>
            </ul>
            <a href="javascript:void(0)" class="track-order" @click="$refs.track.show()">Отследить заказ</a>
            <div class="personal-box">
                <a href="javascript:void(0)" class="personal-box__title">Анна Березина</a>
                <ul class="personal-box__dropdown">
                    <li>
                        <a href="javascript:void(0)">Профиль</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Мои заказы</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Моя семья</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Избранное</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Просмотренное</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Рассылки</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Моя скидка</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Бонусы</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="right-arrow">Выйти</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header__middle-line">
        <div class="container">
            <a href="javascript:void(0)" class="logo">Kidsanna</a>
            <search ref="search" v-cloak></search>
            <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Перезвоните мне</a>
    </div>
</div>
            <div class="cart-like-watched">
                <a href="javascript:void(0)" class="cart-like-watched__item watched">
                    <span>12</span>
                </a>
                <a href="javascript:void(0)" class="cart-like-watched__item like">
                    <span>10</span>
                </a>
                <cart v-cloak></cart>
            </div>
        </div>
    </div>
    <div class="header__bottom-line">
        <div class="container">
            <!-- initial-visible = true для развернутого списка меню каталога,
            без возможности свернуть, false для выпадающего списка по клику -->
            <catalog-dropdown
                    v-cloak
                    trigger="Каталог товаров"
                    v-bind:initial-visible="false"
            >
                <ul class="catalog-menu__dropdown">
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Малыши до года</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Игрушки</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Одежда</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0 decoration-bottom">
                        <a href="javascript:void(0)" class="link-level-0">Обувь</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Темы и персонажи</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Игрушки и путешествия</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Детское питание</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Детская комната</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Уход за ребенком</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Все для кормления</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Транспорт для детей</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Спорт и отдых</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Товары для мам</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Все для школы</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Органические товары</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Аксессуары</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Уцененные товары</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                </ul>
            </catalog-dropdown>
            <ul class="nav-2">
                <li>
                    <a href="javascript:void(0)">Игрушки</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Одежда</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Питание</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Обувь</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Подгузники</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Коляски</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Автокресла</a>
                </li>
                <li>
                    <a href="javascript:void(0)"><span>%</span> Акции</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Outlet</a>
                </li>
            </ul>
        </div>
    </div>
</header>
<header class="header-touch">
    <div class="container _f _i-center">
        <slide-box
                v-cloak
                trigger="burger"
                direction="left"
        >
            <div class="touch-menu">
                <div class="touch-menu__top-line" :class="{green: catalogMenuTouchVisible}">
                    <div class="personal-area-title" v-if="!catalogMenuTouchVisible" @click="personalListVisible = !personalListVisible" :class="{show: personalListVisible}">Анна Березина</div>
                    <div class="lang-switch" v-if="!catalogMenuTouchVisible">
                        <a href="javascript:void(0)" class="lang-switch__item active">RU</a>
                        <a href="javascript:void(0)" class="lang-switch__item">UA</a>
                    </div>
                    <div class="back" @click="catalogMenuTouchVisible = false" v-if="catalogMenuTouchVisible && !nextLevelContext"> <span class="arrow"></span>Назад</div>
                    <div v-if="nextLevelContext" class="back" @click="backLevel"><span class="arrow"></span>{{nextLevelText}}</div>
                </div>
                <div class="touch-menu__middle-line">
                    <vuescroll>
                        <div v-if="!catalogMenuTouchVisible">
                            <div v-if="personalListVisible" v-cloak="">
                                <ul class="personal-area-list">
    <li>
        <a href="javascript:void(0)">Профиль</a>
    </li>
    <li class="active">
        <a href="javascript:void(0)">Мои заказы</a>
    </li>
    <li>
        <a href="javascript:void(0)">Моя семья</a>
    </li>
    <li>
        <a href="javascript:void(0)">Избранное </a>
        <span>45 тов.</span>
    </li>
    <li>
        <a href="javascript:void(0)">Просмотренное </a>
        <span>122</span>
    </li>
    <li>
        <a href="javascript:void(0)">Рассылка</a>
    </li>
    <li>
        <a href="javascript:void(0)">Моя скидка</a>
        <span class="red">7%</span>
    </li>
    <li class="divide">
        <a href="javascript:void(0)">Бонусы</a>
        <span>15000</span>
    </li>
    <li class="ff">
        <a href="javascript:void(0)" class="right-arrow">Выйти</a>
    </li>
    <li class="ff">
        <a href="javascript:void(0)">Изменить пароль</a>
    </li>
</ul>
                            </div>
                            <div class="track">
                                <a href="javascript:void(0)" @click="$refs.track.show()">Отследить заказ</a>
                            </div>
                            <div class="to-catalog" @click="catalogMenuTouchVisible = true">
                                <span>КАТАЛОГ ТОВАРОВ</span>
                                <span class="arrow"></span>
                            </div>
                            <ul class="touch-nav-1">
                                <li>
                                    <a href="javascript:void(0)">Игрушки</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Одежда</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Питание</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Обувь</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Подгузники</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Коляски</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Автокресла</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><span>%</span> Акции</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Outlet</a>
                                </li>
                            </ul>
                            <ul class="touch-nav-2">
                                <li>
                                    <a href="javascript:void(0)">Контакты</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">О нас</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Отзывы</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Полезные статьи</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Доставка и оплата</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Дисконтная программа</a>
                                </li>
                            </ul>
                        </div>
                        <transition name="opacity-transition">
                        <div v-if="catalogMenuTouchVisible">
                            <ul class="catalog-nav-touch">
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level
                                        :decoration="true"
                                >
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                            </ul>
                        </div>
                        </transition>
                    </vuescroll>
                </div>
                <div class="touch-menu__bottom-line">
                    <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Перезвоните мне</a>
    </div>
</div>
                </div>
            </div>
        </slide-box>
        <a href="javascript:void(0)" class="logo">Kidsanna</a>
        <slide-box
                v-cloak
                trigger="search-trigger"
                direction="top"
        >
            <div class="container">
                <search ref="search"></search>
            </div>

        </slide-box>
        <div class="cart-like-watched">
            <a href="javascript:void(0)" class="cart-like-watched__item watched">
                <span class="count">12</span>
            </a>
            <a href="javascript:void(0)" class="cart-like-watched__item like">
                <span class="count">10</span>
            </a>
            <slide-box
                    v-cloak
                    trigger="cart-like-watched__item cart"
                    direction="top"
                    value="10"
            >
                <div class="container">
                    <cart-touch></cart-touch>
                </div>
            </slide-box>
        </div>
    </div>
</header>
        <div class="content">
            <div class="container">
                <ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Главная</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="javascript:void(0)" itemprop="item">
                            <span itemprop="name">Обувь</span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
                <div class="catalog-filter-wrap">
                    <div class="catalog-filter type-2">
                        <div class="def-down-show">
                            <slide-box
                                    v-cloak
                                    trigger="filter-trigger button"
                                    direction="left"
                                    value="Фильтр каталога"
                            >
                                <div class="touch-menu">
                                    <vuescroll>
                                        <filter-box
                                                v-cloak
                                                trigger="Категория"
                                        >
                                            <ul class="filter-box__list">
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                                </li>
                                            </ul>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Цена"
                                        >
                                            <range-slider ref="range"></range-slider>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Пол"
                                        >
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="female" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Девочка</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="male" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Мальчик</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="checkbox" value="all" v-model="filter.sex">
                                                <div class="check"></div>
                                                <div class="input-title">Мальчик/Девочка</div>
                                            </label>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Возраст"
                                        >
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="all" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">Все</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="0-1" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">До года</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="1-2" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">1 год - 2 годa</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="3-5" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">3 годa - 5 лет</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="6-9" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">6 лет - 9 лет</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="10-12" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">10 лет - 12 лет</div>
                                            </label>
                                            <label class="validation-field check-wrapper">
                                                <input type="radio" value="12+" v-model="filter.age">
                                                <div class="radio"></div>
                                                <div class="input-title">Старше 12 лет</div>
                                            </label>
                                        </filter-box>
                                        <filter-box
                                                v-cloak
                                                trigger="Торговая марка"
                                        >
                                            <label class="validation-field">
                                                <input type="text" placeholder="Поиск по бренду" @input="filterBrand($event.target.value)">
                                            </label>
                                            <label class="validation-field check-wrapper" v-for="(item, index) in brandList" :key="index">
                                                <input type="checkbox" :value="item.value" v-model="filter.brand">
                                                <div class="check"></div>
                                                <div class="input-title">{{item.title}} <span>{{item.count}}</span></div>
                                            </label>
                                        </filter-box>
                                    </vuescroll>
                                </div>
                            </slide-box>
                        </div>
                        <div class="def-down-hide">
                            <filter-box
                                    v-cloak
                                    trigger="Категория"
                            >
                                <vuescroll>
                                    <ul class="filter-box__list">
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 1 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 2 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 3 asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 4asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 5asd <span>388</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ботинки, сапоги 6asd <span>388</span></a>
                                        </li>
                                    </ul>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Цена"
                            >
                                <vuescroll>
                                    <range-slider ref="range"></range-slider>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Пол"
                            >
                                <vuescroll>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="female" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Девочка</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="male" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Мальчик</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="checkbox" value="all" v-model="filter.sex">
                                        <div class="check"></div>
                                        <div class="input-title">Мальчик/Девочка</div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Возраст"
                            >
                                <vuescroll>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="all" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">Все</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="0-1" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">До года</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="1-2" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">1 год - 2 годa</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="3-5" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">3 годa - 5 лет</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="6-9" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">6 лет - 9 лет</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="10-12" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">10 лет - 12 лет</div>
                                    </label>
                                    <label class="validation-field check-wrapper">
                                        <input type="radio" value="12+" v-model="filter.age">
                                        <div class="radio"></div>
                                        <div class="input-title">Старше 12 лет</div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                            <filter-box
                                    v-cloak
                                    trigger="Торговая марка"
                            >
                                <label class="validation-field">
                                    <input type="text" placeholder="Поиск по бренду" :value="filterBrand" @input="debounceFilter">
                                </label>
                                <vuescroll>
                                    <label class="validation-field check-wrapper" v-for="(item, index) in brandList" :key="index">
                                        <input type="checkbox" :value="item.value" v-model="filter.brand">
                                        <div class="check"></div>
                                        <div class="input-title">{{item.title}} <span>{{item.count}}</span></div>
                                    </label>
                                </vuescroll>
                            </filter-box>
                        </div>
                    </div>
                    <div class="catalog-wrap">
                        <div class="sorting-wrap">
                            <div class="child-products">
                                <div class="child-products__item-img">
                                    <img src="img/img/child.svg" alt="">
                                </div>
                                <div class="child-products__info">
                                    <div class="child-products__title">Товары для вашего ребенка</div>
                                    <span>
                                     <a href="javascript:void(0)" class="child-products__add-link">
                                    <span>Информация о ребенке</span>
                                </a>
                                     <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                </span>

                                </div>
                            </div>
                            <div class="sorting">
                                <div class="sorting__title">Сортировать по:</div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="sorting"
                                            placeholder="Сортировка"
                                            :options="
                                        [
                                            'сначала с акциями',
                                            'сначала без акций',
                                            'по возрастанию цены'
                                        ]" >
                                    </v-select>
                                </div>
                            </div>
                        </div>
                        <div class="selected">
                            <div class="selected__title">Вы выбрали:</div>
                            <template v-for="(item1, index1) in filter">
                                <div class="selected__item" v-for="(item, index) in item1" :key="item" v-if="index1 !== 'MinMaxPrice' && index1 !== 'price' && index1 !== 'age'" @click="removeFromArr(index1, index)">
                                    {{item}}
                                </div>
                                <div class="selected__item" v-if="index1 === 'age' && item1 !== ''" @click="filter[index1] = ''">
                                    {{item1}}
                                </div>
                            </template>
                            <div class="selected__remove" @click="filter = {...clearFilter}; resetPrice();">Сбросить все</div>
                        </div>
                        <div class="area-catalog-wrap">
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                            <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
    <div class="catalog-item__hover-dropdown">
        <div class="hover-dropdown__top-line">
            <div class="params-item">
                <div class="params-item__title">Цвет:</div>
                <div class="params-item__params-wrap">
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#5099B9">
                        <span class="color" style="background-color: #5099B9"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#233342">
                        <span class="color" style="background-color: #233342"></span>
                    </label>
                    <label class="color-item">
                        <input type="radio" name="color" value="#DD292E">
                        <span class="color" style="background-color: #DD292E"></span>
                    </label>
                </div>
            </div>
            <div class="params-item">
                <div class="params-item__title">Размер:</div>
                <div class="params-item__params-wrap">
                    <label class="size-item">
                        <input type="radio" name="size" value="26">
                        <span class="size">26</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="27">
                        <span class="size">27</span>
                    </label>
                    <label class="size-item">
                        <input type="radio" name="size" value="28">
                        <span class="size">28</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="hover-dropdown__bottom-line">
            <div class="status green">В наличии</div> <!-- или класс red если не в наличии -->
            <div class="vendor-code">Артикул: 12633</div>
        </div>
    </div>
</div>
                        </div>
                        <div class="pagination-wrap">
                            <a href="javascript:void(0)" class="load-more">показать еще 30</a>
                            <ul class="pagination">
                                <li class="prev">
                                    <a href="javascript:void(0)"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">1</a>
                                </li>
                                <li class="active">
                                    <a href="javascript:void(0)">2</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">...</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">15</a>
                                </li>
                                <li class="next">
                                    <a href="javascript:void(0)"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="green-border-wrap _mb-110">
                    <div class="h1">Вы просматривали</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="about-us">
                    <div class="about-us__description-wrap">
                        <dropdown-text
                                open="Читать все"
                                close="Читать меньше"
                        >
                            <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                        </dropdown-text>
                    </div>
                    <div class="about-us__form-wrap">
                        <div class="title">Узнавайте об акциях первыми</div>
                        <form action="/">
                            <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                            </label>
                            <label class="submit" :class="{ disable: $v.$anyError }">
                                <input type="submit" :disabled="$v.$anyError">
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <modal v-cloak ref="favorite">
    <div class="modal__title">Товар добавлен в ваш список желаний</div>
    <div class="cart-item">
        <a href="javascript:void(0)" class="cart-item__item-img">
            <img src="img/temp/result-img.jpg" alt="">
        </a>
        <div class="cart-item__info">
            <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
            <div class="cart-item__info__params">р.24 ,красный</div>
        </div>
        <div class="cart-item__price">
            <span>42 999</span> грн
        </div>
    </div>
</modal>
<modal v-cloak ref="buy">
    <div class="modal__title">Товар добавлен в корзину</div>
    <cart-item></cart-item>
    <div class="buy-info">
        <div class="buy-info__title">Всего в корзине 333 товара на сумму: </div>
        <div class="buy-info__summ"> <span>42 999</span> грн.</div>
    </div>
    <a href="javascript:void(0)" class="button medium _upper">Перейти в корзину</a>
</modal>
<modal v-cloak ref="track" type="tight">
    <div class="modal__title">Отследить заказ</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: order.number, error: $v.order.number.$error  }">
            <input v-model="$v.order.number.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.order.number.required && $v.order.number.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="addchild" type="tight">
    <div class="modal__title">Добавить ребенка</div>
    <form action="/" class="max-width">
        <div class="sex-radio">
            <label class="sex-radio__item">
                <input type="radio" value="male" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Мальчик</span>
            </label>
            <label class="sex-radio__item">
                <input type="radio" value="female" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Девочка</span>
            </label>
        </div>
        <label class="validation-field" :class="{ hasvalue: child.name, error: $v.child.name.$error  }">
            <input v-model="$v.child.name.$model" placeholder="Имя*">
            <div class="error" v-if="!$v.child.name.required && $v.child.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <div class="validation-field">
            <v-date-picker
                    :popover="{ placement: 'bottom', visibility: 'click' }"
                    mode='single'
                    color="green"
                    v-model='child.date'
                    :input-props='{
                        placeholder: "Дата рождения"
                    }'
            >
            </v-date-picker>
        </div>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="changechild" type="tight">
    <div class="modal__title">Изменить данные ребенка</div>
    <form action="/" class="max-width">
        <div class="sex-radio">
            <label class="sex-radio__item">
                <input type="radio" value="male" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Мальчик</span>
            </label>
            <label class="sex-radio__item">
                <input type="radio" value="female" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Девочка</span>
            </label>
        </div>
        <label class="validation-field" :class="{ hasvalue: child.name, error: $v.child.name.$error  }">
            <input v-model="$v.child.name.$model" placeholder="Имя*">
            <div class="error" v-if="!$v.child.name.required && $v.child.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <div class="validation-field">
            <v-date-picker
                    :popover="{ placement: 'bottom', visibility: 'click' }"
                    mode='single'
                    color="green"
                    v-model='child.date'
                    :input-props='{
                        placeholder: "Дата рождения"
                    }'
            >
            </v-date-picker>
        </div>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="callback" type="tight">
    <div class="modal__title">Заказать звонок</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: callback.name, error: $v.callback.name.$error  }">
            <input v-model="$v.callback.name.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.callback.name.required && $v.callback.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="validation-field" :class="{ hasvalue: callback.phone }">
            <div>
                <the-mask mask="+38 (###) ### ##-##" v-model="callback.phone" placeholder="Телефон"/>
            </div>
        </label>
        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Перезвоните мне
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="guestbook" type="tight">
    <div class="modal__title">Оставить отзыв</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: guestbook.name, error: $v.guestbook.name.$error  }">
            <input v-model="$v.guestbook.name.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.guestbook.name.required && $v.guestbook.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="validation-field" :class="{ hasvalue: guestbook.review, error: $v.guestbook.review.$error  }">
            <textarea v-model="$v.guestbook.review.$model" placeholder="Текст отзыва*"></textarea>
            <div class="error" v-if="!$v.guestbook.review.required && $v.guestbook.review.$dirty">Поле обязательно для заполнения</div>
        </label>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Перезвоните мне
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
    </div>
    <footer>
    <div class="container">
        <div class="top-line">
            <div class="footer-column-wrap">
                <div class="footer-column">
                    <a href="/" class="footer-logo">Kidsanna</a>
                    <a href="mailto:kidsanna.com@gmail.com" class="email">kidsanna.com@gmail.com</a>
                    <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Связаться с нами</a>
    </div>
</div>
                    <nav class="social">
    <a href="javascript:void(0)" class="fb"><span class="icon"></span></a>
    <a href="javascript:void(0)" class="inst"><span class="icon"></span></a>
</nav>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Магазин</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">О нас</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отзывы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Контакты</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Полезные статьи</a>
                        </li>
                    </ul>
                    <img src="img/img/visa.svg" alt="" class="visa">
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Заказ</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Дисконтная программа</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отследить заказ</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Личный кабинет</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Мой профиль</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Мои заказы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Избранное</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Популярные категории</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Игрушки</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Одежда</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Питание</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Обувь</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Подгузники</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Коляски</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Автокресла</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">% Акции</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Outlet</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom-line">
            <div class="copyright">© 2020 Все права защищены</div>
            <a href="javascript:void(0)" class="flaxen">
                <img src="img/img/flaxen.svg" alt="">
            </a>
        </div>

    </div>
</footer>
</div>
<script src="js/scripts.min.js"></script>
<script src="js/main.min.js"></script>

<script>
    App._data.filter = {
        brand: ['Alisa Line2'],
        sex: ['all'],
        age: '0-1',
        price: [150, 1985],
        MinMaxPrice: [0, 2500]
    };
    App._data.clearFilter = {
        brand: [],
        sex: [],
        age: '',
        price: App._data.filter.MinMaxPrice,
        MinMaxPrice: App._data.filter.MinMaxPrice
    };
    App._data.brandList = [
        {
            "title": "Alisa Line",
            "count": "124",
            "value": "Alisa Line"
        },
        {
            "title": "Alisa Line2",
            "count": "224",
            "value": "Alisa Line2"
        },
        {
            "title": "Alisa Line3",
            "count": "324",
            "value": "Alisa Line3"
        },
        {
            "title": "Alisa Line32",
            "count": "310",
            "value": "Alisa Line32"
        }
    ]
</script>
</body>
</html>