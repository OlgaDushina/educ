<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
<div id="app">
    <div class="wrap">
        <header class="header-small">
    <div class="container">
        <a href="javascript:void(0)" class="logo">Kidsanna</a>
        <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
</div>
        <a href="javascript:void(0)" class="button recall" @click="$refs.callback.show();">Связаться с нами</a>
    </div>
</header>
        <div class="content">
            <div class="container">
                <div class="ordering-wrap">
                    <div class="column">
                        <div class="h1">Оформление заказа</div>
                        <div class="step"><span>1 шаг</span></div>
                        <div class="contact-information">
                            <div class="h2">Контактная информация</div>
                            <div class="contact-information__ready">
                                <div class="info">Анна Березина, +38 (978) 566-45-67</div>
                                <a href="javascript:void(0)" class="right-arrow">Редактировать</a>
                            </div>
                        </div>
                        <div class="step active"><span>2 шаг</span></div>
                        <div class="contact-information">
                            <div class="h2">Доставка и оплата</div>
                            <div class="login-wrap">
                                <div class="login-wrap__title">Способ доставки</div>
                            </div>
                            <form action="/">
                                <div class="_f _i-start provider-wrap">
                                    <label class="validation-field _f _i-center">
                                        <input type="radio" value="ukrpost" v-model="ordering__delivery.provider">
                                        <div class="radio"></div>
                                        <img src="img/img/ukr-post.svg" alt="" class="provider-img">
                                    </label>
                                    <label class="validation-field _f _i-center">
                                        <input type="radio" value="newpost" v-model="ordering__delivery.provider">
                                        <div class="radio"></div>
                                        <img src="img/img/new-post.svg" alt="" class="provider-img">
                                    </label>
                                </div>

                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="ordering__delivery.type"
                                            placeholder="Выберите способ доставки"
                                            :options="
                                        [
                                            'В отделении',
                                            'Курьером',
                                        ]" >
                                    </v-select>
                                </div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="ordering__delivery.city"
                                            placeholder="Выберите город"
                                            :options="
                                        [
                                            'Херсон',
                                            'Одесса',
                                        ]" >
                                    </v-select>
                                </div>
                                <div class="validation-field select-wrapper">
                                    <v-select
                                            v-model="ordering__delivery.address"
                                            placeholder="Выберите отделение"
                                            :options="
                                        [
                                            'Почтомат №3086: Люстдорфская дорога, 140',
                                            'Почтомат №3087: Люстдорфская дорога, 138'
                                        ]" >
                                    </v-select>
                                </div>
                                <div class="ordering-title">Способ оплаты</div>
                                <div class="ordering-payment-wrap">
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="При получении">
                                        <span class="radio"></span>
                                        <span class="input-title">При получении
                                        <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                        </div>
                                    </span>
                                    </label>
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="Безналичный расчет">
                                        <span class="radio"></span>
                                        <span class="input-title">Безналичный расчет
                                        <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                    </span>
                                    </label>
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="Банковской картой">
                                        <span class="radio"></span>
                                        <span class="input-title">Банковской картой</span>
                                    </label>
                                    <label class="validation-field _f">
                                        <input type="radio" v-model="ordering__delivery.payment" value="Картой на сайте через LIQPAY">
                                        <span class="radio"></span>
                                        <span class="input-title">Картой на сайте через LIQPAY
                                    <div class="hover-info">
                                        <div class="hover-info__trigger">?</div>
                                        <div class="hover-info__text">
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                            <p>Пример текста при наведении</p>
                                        </div>
                                    </div>
                                    </span>
                                    </label>
                                </div>

                                <toggle-box
                                        trigger="Добавить коментарий к заказу"
                                >
                                    <label class="validation-field">
                                        <textarea v-model="ordering__delivery.message" placeholder="Сообщение"></textarea>
                                    </label>
                                </toggle-box>
                                <label class="validation-field _f">
                                    <input type="checkbox" v-model="ordering__delivery.nocall" value="true">
                                    <span class="check"></span>
                                    <span class="input-title">Не перезванивайте мне, я подтверждаю заказ</span>
                                </label>
                                <label class="button _upper" :class="{ disable: $v.$anyError }">
                                    Оформить заказ
                                    <input type="submit" :disabled="$v.$anyError">
                                </label>
                            </form>
                        </div>
                    </div>
                    <div class="column">
                        <div class="h1">Ваш заказ (3)</div>
                        <a href="javascript:void(0)" class="right-arrow">Редактировать</a>
                        <div class="order-preview">
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                            <div class="cart-item">
                                <a href="javascript:void(0)" class="cart-item__item-img">
                                    <img src="img/temp/result-img.jpg" alt="">
                                </a>
                                <div class="cart-item__info">
                                    <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
                                    <div class="cart-item__info__params"><span>Артикул: 12633</span> <span>р.24 ,красный</span></div>
                                </div>
                                <div class="cart-item__count-info">
                                    26 шт.
                                </div>
                                <div class="cart-item__price">
                                    <span>42 999</span> грн
                                </div>
                            </div>
                        </div>
                        <div class="order-preview-result">
                            <div class="order-preview-result__info">Всего 333 товара на сумму: <span class="summ"><span>42 999</span> грн.</span></div>
                            <div class="order-preview-result__info-final">Итого: <span class="summ"><span>42 999</span> грн.</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <modal v-cloak ref="favorite">
    <div class="modal__title">Товар добавлен в ваш список желаний</div>
    <div class="cart-item">
        <a href="javascript:void(0)" class="cart-item__item-img">
            <img src="img/temp/result-img.jpg" alt="">
        </a>
        <div class="cart-item__info">
            <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
            <div class="cart-item__info__params">р.24 ,красный</div>
        </div>
        <div class="cart-item__price">
            <span>42 999</span> грн
        </div>
    </div>
</modal>
<modal v-cloak ref="buy">
    <div class="modal__title">Товар добавлен в корзину</div>
    <cart-item></cart-item>
    <div class="buy-info">
        <div class="buy-info__title">Всего в корзине 333 товара на сумму: </div>
        <div class="buy-info__summ"> <span>42 999</span> грн.</div>
    </div>
    <a href="javascript:void(0)" class="button medium _upper">Перейти в корзину</a>
</modal>
<modal v-cloak ref="track" type="tight">
    <div class="modal__title">Отследить заказ</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: order.number, error: $v.order.number.$error  }">
            <input v-model="$v.order.number.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.order.number.required && $v.order.number.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="addchild" type="tight">
    <div class="modal__title">Добавить ребенка</div>
    <form action="/" class="max-width">
        <div class="sex-radio">
            <label class="sex-radio__item">
                <input type="radio" value="male" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Мальчик</span>
            </label>
            <label class="sex-radio__item">
                <input type="radio" value="female" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Девочка</span>
            </label>
        </div>
        <label class="validation-field" :class="{ hasvalue: child.name, error: $v.child.name.$error  }">
            <input v-model="$v.child.name.$model" placeholder="Имя*">
            <div class="error" v-if="!$v.child.name.required && $v.child.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <div class="validation-field">
            <v-date-picker
                    :popover="{ placement: 'bottom', visibility: 'click' }"
                    mode='single'
                    color="green"
                    v-model='child.date'
                    :input-props='{
                        placeholder: "Дата рождения"
                    }'
            >
            </v-date-picker>
        </div>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="changechild" type="tight">
    <div class="modal__title">Изменить данные ребенка</div>
    <form action="/" class="max-width">
        <div class="sex-radio">
            <label class="sex-radio__item">
                <input type="radio" value="male" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Мальчик</span>
            </label>
            <label class="sex-radio__item">
                <input type="radio" value="female" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Девочка</span>
            </label>
        </div>
        <label class="validation-field" :class="{ hasvalue: child.name, error: $v.child.name.$error  }">
            <input v-model="$v.child.name.$model" placeholder="Имя*">
            <div class="error" v-if="!$v.child.name.required && $v.child.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <div class="validation-field">
            <v-date-picker
                    :popover="{ placement: 'bottom', visibility: 'click' }"
                    mode='single'
                    color="green"
                    v-model='child.date'
                    :input-props='{
                        placeholder: "Дата рождения"
                    }'
            >
            </v-date-picker>
        </div>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="callback" type="tight">
    <div class="modal__title">Заказать звонок</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: callback.name, error: $v.callback.name.$error  }">
            <input v-model="$v.callback.name.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.callback.name.required && $v.callback.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="validation-field" :class="{ hasvalue: callback.phone }">
            <div>
                <the-mask mask="+38 (###) ### ##-##" v-model="callback.phone" placeholder="Телефон"/>
            </div>
        </label>
        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Перезвоните мне
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="guestbook" type="tight">
    <div class="modal__title">Оставить отзыв</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: guestbook.name, error: $v.guestbook.name.$error  }">
            <input v-model="$v.guestbook.name.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.guestbook.name.required && $v.guestbook.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="validation-field" :class="{ hasvalue: guestbook.review, error: $v.guestbook.review.$error  }">
            <textarea v-model="$v.guestbook.review.$model" placeholder="Текст отзыва*"></textarea>
            <div class="error" v-if="!$v.guestbook.review.required && $v.guestbook.review.$dirty">Поле обязательно для заполнения</div>
        </label>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Перезвоните мне
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
    </div>
    <footer>
    <div class="container">
        <div class="top-line">
            <div class="footer-column-wrap">
                <div class="footer-column">
                    <a href="/" class="footer-logo">Kidsanna</a>
                    <a href="mailto:kidsanna.com@gmail.com" class="email">kidsanna.com@gmail.com</a>
                    <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Связаться с нами</a>
    </div>
</div>
                    <nav class="social">
    <a href="javascript:void(0)" class="fb"><span class="icon"></span></a>
    <a href="javascript:void(0)" class="inst"><span class="icon"></span></a>
</nav>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Магазин</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">О нас</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отзывы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Контакты</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Полезные статьи</a>
                        </li>
                    </ul>
                    <img src="img/img/visa.svg" alt="" class="visa">
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Заказ</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Дисконтная программа</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отследить заказ</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Личный кабинет</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Мой профиль</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Мои заказы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Избранное</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Популярные категории</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Игрушки</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Одежда</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Питание</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Обувь</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Подгузники</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Коляски</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Автокресла</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">% Акции</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Outlet</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom-line">
            <div class="copyright">© 2020 Все права защищены</div>
            <a href="javascript:void(0)" class="flaxen">
                <img src="img/img/flaxen.svg" alt="">
            </a>
        </div>

    </div>
</footer>
</div>

<script src="js/scripts.min.js"></script>
<script src="js/main.min.js"></script>

</body>
</html>