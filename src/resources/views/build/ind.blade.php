<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kidsanna</title>
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
</head>

<body >
    <div id="app">
        <div class="wrap">
            <header class="header">
    <div class="header__top-line">
        <div class="container">
            <div class="lang-switch">
                <a href="javascript:void(0)" class="lang-switch__item active">RU</a>
                <a href="javascript:void(0)" class="lang-switch__item">UA</a>
            </div>
            <ul class="nav">
                <li>
                    <a href="/contacts">Контакты</a>
                </li>
                <li>
                    <a href="/about-us">О нас</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Отзывы</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Полезные статьи</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Доставка и оплата</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Дисконтная программа</a>
                </li>
            </ul>
            <a href="javascript:void(0)" class="track-order" @click="$refs.track.show()">Отследить заказ</a>
            <div class="personal-box">
                <a href="javascript:void(0)" class="personal-box__title">Анна Березина</a>
                <ul class="personal-box__dropdown">
                    <li>
                        <a href="javascript:void(0)">Профиль</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Мои заказы</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Моя семья</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Избранное</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Просмотренное</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Рассылки</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Моя скидка</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Бонусы</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="right-arrow">Выйти</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header__middle-line">
        <div class="container">
            <a href="javascript:void(0)" class="logo">Kidsanna</a>
            <search ref="search" v-cloak></search>
            <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Перезвоните мне</a>
    </div>
</div>
            <div class="cart-like-watched">
                <a href="javascript:void(0)" class="cart-like-watched__item watched">
                    <span>12</span>
                </a>
                <a href="javascript:void(0)" class="cart-like-watched__item like">
                    <span>10</span>
                </a>
                <cart v-cloak></cart>
            </div>
        </div>
    </div>
    <div class="header__bottom-line">
        <div class="container">
            <!-- initial-visible = true для развернутого списка меню каталога,
            без возможности свернуть, false для выпадающего списка по клику -->
            <catalog-dropdown
                    v-cloak
                    trigger="Каталог товаров"
                    v-bind:initial-visible="false"
            >
                <ul class="catalog-menu__dropdown">
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Малыши до года</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Игрушки</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Одежда</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0 decoration-bottom">
                        <a href="javascript:void(0)" class="link-level-0">Обувь</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Темы и персонажи</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Игрушки и путешествия</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Детское питание</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Детская комната</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Уход за ребенком</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Все для кормления</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Транспорт для детей</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Спорт и отдых</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Товары для мам</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Все для школы</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Органические товары</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Аксессуары</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                    <li class="item-level-0">
                        <a href="javascript:void(0)" class="link-level-0">Уцененные товары</a>
                        <ul class="level-1">
                            <vuescroll>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item-level-1">
                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                    <ul class="level-2">
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                        </li>
                                        <li class="item-level-2">
                                            <a href="javascript:void(0)" class="link-level-2 right-arrow">Все</a>
                                        </li>
                                    </ul>
                                </li>
                            </vuescroll>
                        </ul>
                    </li>
                </ul>
            </catalog-dropdown>
            <ul class="nav-2">
                <li>
                    <a href="javascript:void(0)">Игрушки</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Одежда</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Питание</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Обувь</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Подгузники</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Коляски</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Автокресла</a>
                </li>
                <li>
                    <a href="javascript:void(0)"><span>%</span> Акции</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Outlet</a>
                </li>
            </ul>
        </div>
    </div>
</header>
<header class="header-touch">
    <div class="container _f _i-center">
        <slide-box
                v-cloak
                trigger="burger"
                direction="left"
        >
            <div class="touch-menu">
                <div class="touch-menu__top-line" :class="{green: catalogMenuTouchVisible}">
                    <div class="personal-area-title" v-if="!catalogMenuTouchVisible" @click="personalListVisible = !personalListVisible" :class="{show: personalListVisible}">Анна Березина</div>
                    <div class="lang-switch" v-if="!catalogMenuTouchVisible">
                        <a href="javascript:void(0)" class="lang-switch__item active">RU</a>
                        <a href="javascript:void(0)" class="lang-switch__item">UA</a>
                    </div>
                    <div class="back" @click="catalogMenuTouchVisible = false" v-if="catalogMenuTouchVisible && !nextLevelContext"> <span class="arrow"></span>Назад</div>
                    <div v-if="nextLevelContext" class="back" @click="backLevel"><span class="arrow"></span>{nextLevelText}</div>
                </div>
                <div class="touch-menu__middle-line">
                    <vuescroll>
                        <div v-if="!catalogMenuTouchVisible">
                            <div v-if="personalListVisible" v-cloak="">
                                <ul class="personal-area-list">
    <li>
        <a href="javascript:void(0)">Профиль</a>
    </li>
    <li class="active">
        <a href="javascript:void(0)">Мои заказы</a>
    </li>
    <li>
        <a href="javascript:void(0)">Моя семья</a>
    </li>
    <li>
        <a href="javascript:void(0)">Избранное </a>
        <span>45 тов.</span>
    </li>
    <li>
        <a href="javascript:void(0)">Просмотренное </a>
        <span>122</span>
    </li>
    <li>
        <a href="javascript:void(0)">Рассылка</a>
    </li>
    <li>
        <a href="javascript:void(0)">Моя скидка</a>
        <span class="red">7%</span>
    </li>
    <li class="divide">
        <a href="javascript:void(0)">Бонусы</a>
        <span>15000</span>
    </li>
    <li class="ff">
        <a href="javascript:void(0)" class="right-arrow">Выйти</a>
    </li>
    <li class="ff">
        <a href="javascript:void(0)">Изменить пароль</a>
    </li>
</ul>
                            </div>
                            <div class="track">
                                <a href="javascript:void(0)" @click="$refs.track.show()">Отследить заказ</a>
                            </div>
                            <div class="to-catalog" @click="catalogMenuTouchVisible = true">
                                <span>КАТАЛОГ ТОВАРОВ</span>
                                <span class="arrow"></span>
                            </div>
                            <ul class="touch-nav-1">
                                <li>
                                    <a href="javascript:void(0)">Игрушки</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Одежда</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Питание</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Обувь</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Подгузники</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Коляски</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Автокресла</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><span>%</span> Акции</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Outlet</a>
                                </li>
                            </ul>
                            <ul class="touch-nav-2">
                                <li>
                                    <a href="/contacts">Контакты</a>
                                </li>
                                <li>
                                    <a href="about-us">О нас</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Отзывы</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Полезные статьи</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Доставка и оплата</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Дисконтная программа</a>
                                </li>
                            </ul>
                        </div>
                        <transition name="opacity-transition">
                        <div v-if="catalogMenuTouchVisible">
                            <ul class="catalog-nav-touch">
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level
                                        :decoration="true"
                                >
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                            </ul>
                        </div>
                        </transition>
                    </vuescroll>
                </div>
                <div class="touch-menu__bottom-line">
                    <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Перезвоните мне</a>
    </div>
</div>
                </div>
            </div>
        </slide-box>
        <a href="javascript:void(0)" class="logo">Kidsanna</a>
        <slide-box
                v-cloak
                trigger="search-trigger"
                direction="top"
        >
            <div class="container">
                <search ref="search"></search>
            </div>

        </slide-box>
        <div class="cart-like-watched">
            <a href="javascript:void(0)" class="cart-like-watched__item watched">
                <span class="count">12</span>
            </a>
            <a href="javascript:void(0)" class="cart-like-watched__item like">
                <span class="count">10</span>
            </a>
            <slide-box
                    v-cloak
                    trigger="cart-like-watched__item cart"
                    direction="top"
                    value="10"
            >
                <div class="container">
                    <cart-touch></cart-touch>
                </div>
            </slide-box>
        </div>
    </div>
</header>
            <div class="banner-wrap">
                <div class="container">
                    <div class="swiper-container banner">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="img/temp/banner.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/temp/banner.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/temp/banner.jpg" alt="">
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <div class="advantages">
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-1.svg" alt="">
                            </div>
                            <div class="advantages__title">Доставка по Украине и регионам</div>
                        </div>
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-2.svg" alt="">
                            </div>
                            <div class="advantages__title">Огромный ассортимент товаров в наличии</div>
                        </div>
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-3.svg" alt="">
                            </div>
                            <div class="advantages__title">Сертифицированные и безопасные товары</div>
                        </div>
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-4.svg" alt="">
                            </div>
                            <div class="advantages__title">Мы работаем пн-вс с 7:55 до 20:05</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profitable-offers">
                <div class="container">
                    <div class="profitable-slider-wrap">
                        <div class="catalog-slider-wrap profitable">
                            <div class="h1">Выгодные предложения</div>
                            <div class="swiper-container profitable-slider slider-arrows-top">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                        <div class="top-slider-wrap">
                            <div class="swiper-container top-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="new-offers">
                <div class="container">
                    <div class="h1">Новинки</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
            <div class="catalog-grid-wrapper">
                <div class="container">
                    <div class="catalog-wrapper">
                        <a href="javascript:void(0)" class="one catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Товары для <br> малышей</div>
                            <img src="img/img/decor-1-1.svg" alt="" class="decor-1-1 decor-item">
                            <img src="img/img/decor-1-2.svg" alt="" class="decor-1-2 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="two catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Детская комната</div>
                            <img src="img/img/decor-2-1.svg" alt="" class="decor-2-1 decor-item">
                            <img src="img/img/decor-2-2.svg" alt="" class="decor-2-2 decor-item">
                            <img src="img/img/decor-2-3.svg" alt="" class="decor-2-3 decor-item">
                            <img src="img/img/decor-2-4.svg" alt="" class="decor-2-4 decor-item">
                            <img src="img/img/decor-2-5.svg" alt="" class="decor-2-5 decor-item">
                            <img src="img/img/decor-2-6.svg" alt="" class="decor-2-6 decor-item">
                            <img src="img/img/decor-2-7.svg" alt="" class="decor-2-7 decor-item">
                        </a>
                        <div class="three catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Одежда и Обувь</div>
                            <ul class="catalog-wrapper__item__list">
                                <li>
                                    <a href="javascript:void(0)">Верхняя одежда</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Одежда для малышей</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Штаны джинсы шорты</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Футболки и топы</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Кросовки и кеды</a>
                                </li>
                            </ul>
                            <img src="img/img/decor-3-1.svg" alt="" class="decor-3-1 decor-item">
                            <img src="img/img/decor-3-2.svg" alt="" class="decor-3-2 decor-item">
                            <img src="img/img/decor-3-3.svg" alt="" class="decor-3-3 decor-item">
                        </div>
                        <a href="javascript:void(0)" class="four catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Подобрать игрушку</div>
                            <img src="img/img/decor-4-1.svg" alt="" class="decor-4-1 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="five catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Настольные игры</div>
                            <img src="img/img/decor-5-1.svg" alt="" class="decor-5-1 decor-item">
                            <img src="img/img/decor-5-2.svg" alt="" class="decor-5-2 decor-item">
                            <img src="img/img/decor-5-3.svg" alt="" class="decor-5-3 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="six catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Куклы L.O.L</div>
                            <img src="img/img/decor-6-1.svg" alt="" class="decor-6-1 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="seven catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">LEGO</div>
                            <img src="img/img/decor-7-1.svg" alt="" class="decor-7-1 decor-item">
                            <img src="img/img/decor-7-2.svg" alt="" class="decor-7-2 decor-item">
                            <img src="img/img/decor-7-3.svg" alt="" class="decor-7-3 decor-item">
                            <img src="img/img/decor-7-4.svg" alt="" class="decor-7-4 decor-item">
                            <img src="img/img/decor-7-5.svg" alt="" class="decor-7-5 decor-item">
                            <img src="img/img/decor-7-6.svg" alt="" class="decor-7-6 decor-item">
                        </a>
                        <div class="eight catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Прогулки <br>и путешествия</div>
                            <ul class="catalog-wrapper__item__list">
                                <li>
                                    <a href="javascript:void(0)">Автокресла</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Коляски</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Рюкзаки, слинги, вожи</a>
                                </li>
                            </ul>
                            <img src="img/img/decor-8-1.svg" alt="" class="decor-8-1 decor-item">
                            <img src="img/img/decor-8-2.svg" alt="" class="decor-8-2 decor-item">
                        </div>
                    </div>
                </div>
            </div>
            <div class="brands-wrap">
                <div class="container">
                    <div class="swiper-container brands-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    Все бренды
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
            <div class="news-slider-wrap">
                <div class="container">
                    <div class="h1">Новости, акции, статьи</div>
                    <div class="swiper-container news-slider slider-arrows-top">
                        <div class="swiper-wrapper">
					@foreach($data as $el)	
                            <div class="swiper-slide">
                                <div class="news-item">
    <a href="<?php echo '/'.$el->pris;?>" class="news-item__item-img">
        <img src="<?php echo 'img/img/'.$el->img;?>" alt="">
    </a>
    <a href="<?php echo '/'.$el->pris;?>" class="news-item__title">
      <?php  echo $el->title;?> 
    </a>
    <div class="news-item__date"><?php  echo $el->date;?> </div>
</div>
                            </div>
							
					@endforeach		
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                        <a href="javascript:void(0)" class="all-link">Смотреть все</a>
                    </div>
                </div>
            </div>
            <div class="about">
                <div class="container">
                    <div class="about-us">
                        <div class="about-us__description-wrap">
                            <dropdown-text
                                    open="Читать все"
                                    close="Читать меньше"
                            >
                                <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            </dropdown-text>
                        </div>
                        <div class="about-us__form-wrap">
                            <div class="title">Узнавайте об акциях первыми</div>
                            <form action="/">
                                <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                    <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                    <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                    <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                                </label>
                                <label class="submit" :class="{ disable: $v.$anyError }">
                                    <input type="submit" :disabled="$v.$anyError">
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <modal v-cloak ref="favorite">
    <div class="modal__title">Товар добавлен в ваш список желаний</div>
    <div class="cart-item">
        <a href="javascript:void(0)" class="cart-item__item-img">
            <img src="img/temp/result-img.jpg" alt="">
        </a>
        <div class="cart-item__info">
            <a href="javascript:void(0)" class="cart-item__info__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
            <div class="cart-item__info__params">р.24 ,красный</div>
        </div>
        <div class="cart-item__price">
            <span>42 999</span> грн
        </div>
    </div>
</modal>
<modal v-cloak ref="buy">
    <div class="modal__title">Товар добавлен в корзину</div>
    <cart-item></cart-item>
    <div class="buy-info">
        <div class="buy-info__title">Всего в корзине 333 товара на сумму: </div>
        <div class="buy-info__summ"> <span>42 999</span> грн.</div>
    </div>
    <a href="javascript:void(0)" class="button medium _upper">Перейти в корзину</a>
</modal>
<modal v-cloak ref="track" type="tight">
    <div class="modal__title">Отследить заказ</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: order.number, error: $v.order.number.$error  }">
            <input v-model="$v.order.number.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.order.number.required && $v.order.number.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="addchild" type="tight">
    <div class="modal__title">Добавить ребенка</div>
    <form action="/" class="max-width">
        <div class="sex-radio">
            <label class="sex-radio__item">
                <input type="radio" value="male" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Мальчик</span>
            </label>
            <label class="sex-radio__item">
                <input type="radio" value="female" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Девочка</span>
            </label>
        </div>
        <label class="validation-field" :class="{ hasvalue: child.name, error: $v.child.name.$error  }">
            <input v-model="$v.child.name.$model" placeholder="Имя*">
            <div class="error" v-if="!$v.child.name.required && $v.child.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <div class="validation-field">
            <v-date-picker
                    :popover="{ placement: 'bottom', visibility: 'click' }"
                    mode='single'
                    color="green"
                    v-model='child.date'
                    :input-props='{
                        placeholder: "Дата рождения"
                    }'
            >
            </v-date-picker>
        </div>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="changechild" type="tight">
    <div class="modal__title">Изменить данные ребенка</div>
    <form action="/" class="max-width">
        <div class="sex-radio">
            <label class="sex-radio__item">
                <input type="radio" value="male" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Мальчик</span>
            </label>
            <label class="sex-radio__item">
                <input type="radio" value="female" name="sex-radio" v-model="child.sex">
                <span class="sex-radio__title">Девочка</span>
            </label>
        </div>
        <label class="validation-field" :class="{ hasvalue: child.name, error: $v.child.name.$error  }">
            <input v-model="$v.child.name.$model" placeholder="Имя*">
            <div class="error" v-if="!$v.child.name.required && $v.child.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <div class="validation-field">
            <v-date-picker
                    :popover="{ placement: 'bottom', visibility: 'click' }"
                    mode='single'
                    color="green"
                    v-model='child.date'
                    :input-props='{
                        placeholder: "Дата рождения"
                    }'
            >
            </v-date-picker>
        </div>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Продолжить
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="callback" type="tight">
    <div class="modal__title">Заказать звонок</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: callback.name, error: $v.callback.name.$error  }">
            <input v-model="$v.callback.name.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.callback.name.required && $v.callback.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="validation-field" :class="{ hasvalue: callback.phone }">
            <div>
                <the-mask mask="+38 (###) ### ##-##" v-model="callback.phone" placeholder="Телефон"/>
            </div>
        </label>
        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Перезвоните мне
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
<modal v-cloak ref="guestbook" type="tight">
    <div class="modal__title">Оставить отзыв</div>
    <form action="/">
        <label class="validation-field" :class="{ hasvalue: guestbook.name, error: $v.guestbook.name.$error  }">
            <input v-model="$v.guestbook.name.$model" placeholder="Ваше имя*">
            <div class="error" v-if="!$v.guestbook.name.required && $v.guestbook.name.$dirty">Поле обязательно для заполнения</div>
        </label>
        <label class="validation-field" :class="{ hasvalue: guestbook.review, error: $v.guestbook.review.$error  }">
            <textarea v-model="$v.guestbook.review.$model" placeholder="Текст отзыва*"></textarea>
            <div class="error" v-if="!$v.guestbook.review.required && $v.guestbook.review.$dirty">Поле обязательно для заполнения</div>
        </label>

        <label class="button _upper medium" :class="{ disable: $v.$anyError }">
            Перезвоните мне
            <input type="submit" :disabled="$v.$anyError">
        </label>
    </form>
</modal>
        </div>
        <footer>
    <div class="container">
        <div class="top-line">
            <div class="footer-column-wrap">
                <div class="footer-column">
                    <a href="/" class="footer-logo">Kidsanna</a>
                    <a href="mailto:kidsanna.com@gmail.com" class="email">kidsanna.com@gmail.com</a>
                    <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Связаться с нами</a>
    </div>
</div>
                    <nav class="social">
    <a href="javascript:void(0)" class="fb"><span class="icon"></span></a>
    <a href="javascript:void(0)" class="inst"><span class="icon"></span></a>
</nav>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Магазин</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="/about-us">О нас</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отзывы</a>
                        </li>
                        <li>
                            <a href="/contacts">Контакты</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Полезные статьи</a>
                        </li>
                    </ul>
                    <img src="img/img/visa.svg" alt="" class="visa">
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Заказ</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Дисконтная программа</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Отследить заказ</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Личный кабинет</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Мой профиль</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Мои заказы</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Избранное</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-column">
                    <div class="footer-column__title">Популярные категории</div>
                    <ul class="footer-nav">
                        <li>
                            <a href="javascript:void(0)">Игрушки</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Одежда</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Питание</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Обувь</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Подгузники</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Коляски</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Автокресла</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">% Акции</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Outlet</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom-line">
            <div class="copyright">© 2020 Все права защищены</div>
            <a href="javascript:void(0)" class="flaxen">
                <img src="img/img/flaxen.svg" alt="">
            </a>
        </div>

    </div>
</footer>
    </div>
    <script src="js/scripts.min.js"></script>
<script src="js/main.min.js"></script>

</body>
</html>