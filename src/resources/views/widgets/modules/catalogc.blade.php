        <!-- initial-visible = true для развернутого списка меню каталога,
            без возможности свернуть, false для выпадающего списка по клику -->
        <catalog-dropdown
            v-cloak
            trigger="Каталог товаров"
            v-bind:initial-visible="false"
        >
            <ul class="catalog-menu__dropdown">
<!--					<?php // print_r($category); ?> -->
			@foreach($catalog as $cat1)
			    <li class="item-level-0">
                    <a href="{{LaravelLocalization::localizeUrl($cat1->url)}}" class="link-level-0">{{$cat1->namet}}</a>
                    <ul class="level-1">
					   <vuescroll>
 					    @foreach($category->where('catalog_id',$cat1->id)->all() as $cat2)
             			   <li class="item-level-1">
						      <a href="{{LaravelLocalization::localizeUrl($cat2->url)}}" class=link-level-1">{{$cat2->namet}}</a>
                                <ul class="level-2">
								@foreach($sub_category->where('category_id',$cat2->id)->all() as $cat3)
                                    <li class="item-level-2">
                                        <a href="{{LaravelLocalization::localizeUrl($cat3->url)}}" class="link-level-2">{{$cat3->namet}}</a>
                                    </li>
                                @endforeach
								    <li class="item-level-2">
                                        <a href="{{LaravelLocalization::localizeUrl($cat2->url)}}" class="link-level-2 right-arrow">Все</a>
                                    </li>
                                </ul>
							</li>	
			        @endforeach 
                        </vuescroll>

                    </ul>
                </li>
				@endforeach
            </ul>
        </catalog-dropdown>
 