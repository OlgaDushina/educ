<div class="header__middle-line">
    <div class="container">
        <a href="javascript:void(0)" class="logo">Kidsanna</a>
        <search ref="search" v-cloak></search>
        <div class="phone-box">
            <div class="phone-box__top-line">
                <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
                <a href="javascript:void(0)" class="viber circle-item"></a>
                <a href="javascript:void(0)" class="whatsapp circle-item"></a>
                <a href="javascript:void(0)" class="telegram circle-item"></a>
            </div>
            <div class="phone-box__bottom-line">
                <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Перезвоните мне</a>
            </div>
        </div>
        <div class="cart-like-watched">
            <a href="javascript:void(0)" class="cart-like-watched__item watched">
                <span>12</span>
            </a>
            <a href="javascript:void(0)" class="cart-like-watched__item like">
                <span>10</span>
            </a>
            <cart v-cloak></cart>
        </div>
    </div>
</div>
