    <div class="header__top-line">
        <div class="container">
            <div class="lang-switch">
               <a href="{{ LaravelLocalization::getLocalizedURL('ru') }}"
               class="lang-switch__item {{ LaravelLocalization::getCurrentLocale() == 'ru' ? 'active' : null }}">
                RU
            </a>
            <a href="{{ LaravelLocalization::getLocalizedURL('uk') }}"
               class="lang-switch__item {{ LaravelLocalization::getCurrentLocale() == 'uk' ? 'active' : null }}">
                UA
            </a>
            </div>
			<!--<?php print_r($menu);?>-->
            <ul class="nav">
             @foreach($menu as $item)
                <li>
                         <a href="{{LaravelLocalization::localizeUrl($item->url)}}">
                            {{$item->name}}</a>
                </li>
             @endforeach
             </ul>
            <a href="javascript:void(0)" class="track-order" @click="$refs.track.show()">Отследить заказ</a>
            <div class="personal-box">.
			@auth
			    <a href="javascript:void(0)" class="personal-box__title"> {{$user->name}}</a>
                <ul class="personal-box__dropdown">
                    <li>
                        <a href="javascript:void(0)">Профиль</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Мои заказы</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Моя семья</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Избранное</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Просмотренное</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Рассылки</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Моя скидка</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Бонусы</a>
                    </li>
                    <li>
                        <a href="/logout" class="right-arrow">Выйти</a>
                    </li>
                </ul>
			@else
             <a href="{{LaravelLocalization::localizeUrl('/register')}}" class="personal-box__title">Войти</a>
            @endif
			</div>
        </div>
    </div>
