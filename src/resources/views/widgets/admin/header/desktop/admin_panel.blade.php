  <div class="header__top-line">
        <div class="container">
            <ul class="nav">
             @foreach($menu_admin as $item)
                <li>
                         <a href="{{LaravelLocalization::localizeUrl($item->url)}}">
                            {{$item->name}}</a>
                </li>
             @endforeach
             </ul>

        </div>
    </div>
