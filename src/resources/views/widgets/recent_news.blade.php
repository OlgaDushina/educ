            <div class="news-slider-wrap">
                <div class="container">
                    <div class="h1">Новости, акции, статьи</div>
                    <div class="swiper-container news-slider slider-arrows-top">
                        <div class="swiper-wrapper">
					@foreach($data as $el)	
                            <div class="swiper-slide">
                                <div class="news-item">
    <a href="{{LaravelLocalization::localizeUrl( '/news/'.$el->pris)}}" class="news-item__item-img">
        <img src="<?php echo 'img/img/'.$el->img;?>" alt="">
    </a>
    <a href="{{LaravelLocalization::localizeUrl( '/news/'.$el->pris)}}" class="news-item__title">
      <?php  echo $el->title;?> 
    </a>
    <div class="news-item__date"><?php  echo $el->date;?> </div>
</div>
                            </div>
							
					@endforeach		
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                        <a href="javascript:void(0)" class="all-link">Смотреть все</a>
                    </div>
                </div>
            </div>
    