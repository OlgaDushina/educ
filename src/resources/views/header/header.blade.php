        <div class="wrap">
            <header class="header">
   @widget('header.desktop.topline');
   @widget('header.desktop.middleline');
   @widget('header.desktop.bottomline');
</header>
<header class="header-touch">
    <div class="container _f _i-center">
        <slide-box
                v-cloak
                trigger="burger"
                direction="left"
        >
            <div class="touch-menu">
                <div class="touch-menu__top-line" :class="{green: catalogMenuTouchVisible}">
                    <div class="personal-area-title" v-if="!catalogMenuTouchVisible" @click="personalListVisible = !personalListVisible" :class="{show: personalListVisible}">Анна Березина</div>
                    <div class="lang-switch" v-if="!catalogMenuTouchVisible">
                        <a href="javascript:void(0)" class="lang-switch__item active">RU</a>
                        <a href="javascript:void(0)" class="lang-switch__item">UA</a>
                    </div>
                    <div class="back" @click="catalogMenuTouchVisible = false" v-if="catalogMenuTouchVisible && !nextLevelContext"> <span class="arrow"></span>Назад</div>
                    <div v-if="nextLevelContext" class="back" @click="backLevel"><span class="arrow"></span>{nextLevelText}</div>
                </div>
                <div class="touch-menu__middle-line">
                    <vuescroll>
                        <div v-if="!catalogMenuTouchVisible">
                            <div v-if="personalListVisible" v-cloak="">
                                <ul class="personal-area-list">
    <li>
        <a href="javascript:void(0)">Профиль</a>
    </li>
    <li class="active">
        <a href="javascript:void(0)">Мои заказы</a>
    </li>
    <li>
        <a href="javascript:void(0)">Моя семья</a>
    </li>
    <li>
        <a href="javascript:void(0)">Избранное </a>
        <span>45 тов.</span>
    </li>
    <li>
        <a href="javascript:void(0)">Просмотренное </a>
        <span>122</span>
    </li>
    <li>
        <a href="javascript:void(0)">Рассылка</a>
    </li>
    <li>
        <a href="javascript:void(0)">Моя скидка</a>
        <span class="red">7%</span>
    </li>
    <li class="divide">
        <a href="javascript:void(0)">Бонусы</a>
        <span>15000</span>
    </li>
    <li class="ff">
        <a href="javascript:void(0)" class="right-arrow">Выйти</a>
    </li>
    <li class="ff">
        <a href="javascript:void(0)">Изменить пароль</a>
    </li>
</ul>
                            </div>
                            <div class="track">
                                <a href="javascript:void(0)" @click="$refs.track.show()">Отследить заказ</a>
                            </div>
                            <div class="to-catalog" @click="catalogMenuTouchVisible = true">
                                <span>КАТАЛОГ ТОВАРОВ</span>
                                <span class="arrow"></span>
                            </div>
                            <ul class="touch-nav-1">
                                <li>
                                    <a href="javascript:void(0)">Игрушки</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Одежда</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Питание</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Обувь</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Подгузники</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Коляски</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Автокресла</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><span>%</span> Акции</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Outlet</a>
                                </li>
                            </ul>
                            <ul class="touch-nav-2">
                                <li>
                                    <a href="/contacts">Контакты</a>
                                </li>
                                <li>
                                    <a href="about-us">О нас</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Отзывы</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Полезные статьи</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Доставка и оплата</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Дисконтная программа</a>
                                </li>
                            </ul>
                        </div>
                        <transition name="opacity-transition">
                        <div v-if="catalogMenuTouchVisible">
                            <ul class="catalog-nav-touch">
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level
                                        :decoration="true"
                                >
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года1</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность11</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности12</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности13</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности14</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности15</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность123</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года2</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность232</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года3</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                                <next-level>
                                    <template v-slot:link>
                                        <a href="javascript:void(0)" class="link-level-0">Малыши до года4</a>
                                    </template>
                                    <template v-slot:list>
                                        <ul class="level-1">
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                            <next-level>
                                                <template v-slot:link>
                                                    <a href="javascript:void(0)" class="link-level-1">Комфорт и безопасность</a>
                                                </template>
                                                <template v-slot:list>
                                                    <ul class="level-2">
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                        <li class="item-level-2">
                                                            <a href="javascript:void(0)" class="link-level-2">Средства безопасности</a>
                                                        </li>
                                                    </ul>
                                                </template>
                                            </next-level>
                                        </ul>
                                    </template>
                                </next-level>
                            </ul>
                        </div>
                        </transition>
                    </vuescroll>
                </div>
                <div class="touch-menu__bottom-line">
                    <div class="phone-box">
    <div class="phone-box__top-line">
        <a href="tel:+380504941797" class="phone">(050) 494 17 97</a>
        <a href="javascript:void(0)" class="viber circle-item"></a>
        <a href="javascript:void(0)" class="whatsapp circle-item"></a>
        <a href="javascript:void(0)" class="telegram circle-item"></a>
    </div>
    <div class="phone-box__bottom-line">
        <a href="javascript:void(0)" class="callback" @click="$refs.callback.show();">Перезвоните мне</a>
    </div>
</div>
                </div>
            </div>
        </slide-box>
        <a href="javascript:void(0)" class="logo">Kidsanna</a>
        <slide-box
                v-cloak
                trigger="search-trigger"
                direction="top"
        >
            <div class="container">
                <search ref="search"></search>
            </div>

        </slide-box>
        <div class="cart-like-watched">
            <a href="javascript:void(0)" class="cart-like-watched__item watched">
                <span class="count">12</span>
            </a>
            <a href="javascript:void(0)" class="cart-like-watched__item like">
                <span class="count">10</span>
            </a>
            <slide-box
                    v-cloak
                    trigger="cart-like-watched__item cart"
                    direction="top"
                    value="10"
            >
                <div class="container">
                    <cart-touch></cart-touch>
                </div>
            </slide-box>
        </div>
    </div>
</header>
       </div>
