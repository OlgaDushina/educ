@extends('admin.layouts._layout_admin')
@section('content')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
    <h1>Категории товаров</h1>
	<ul class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="{{LaravelLocalization::localizeUrl('/admin/acategory')}}" itemprop="item">
                            <span itemprop="name"><h3>Каталог</h3></span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a href="" itemprop="item">
                            <span itemprop="name"><h3>{{$catalog['namet']}}</h3></span>
                        </a>
                        <meta itemprop="position" content="2">
                    </li>
                 </ul>

	<a href="{{route('admin.Acategory@add')}}" > <button>Добавить категорию</button></a>
<form name="search" method="post" action="{{route('admin.Acategory@searchpost',['idc'=>$catalog['id'] ])}}">
    @csrf
	<input type="search" name="query" placeholder="Поиск">
    <button type="submit">Найти</button> 
</form>	

 
	<table class="catalog_table"><tr><th>ID</th><th>Наименование</th><th>ID верхнего уровня</th><th>URL</th><th>Позиция</th><th>Статус</th><th></th><th></th></tr>	 
		@foreach($category as $cat)
	
			<tr >
			<td class="id"> {{$cat['id']}}</td>
		    <td class="name"><a href="{{route('admin.Acategory@asubcat',['id' =>$cat['id'],'idc'=>$catalog['id'] ])}}"><button>{{$cat['namet']}} </button></a></td>
			<td class="catalog_id">{{$cat['catalog_id']}}</td>
			<td class="url">{{$cat['url']}}</td>
			<td class="sort">{{$cat['sort']}}</td>
			<td class="status">{{$cat['status']}}</td>
			<td><a href="{{route('admin.Acategory@edit',['id' =>$cat['id'],'idc'=>$catalog['id'] ])}}" id="{{$cat['id']}}" >Редактировать</a></td>
			<td><a href="{{route('admin.Acategory@del',['id' =>$cat['id'],'idc'=>$catalog['id'] ])}}" >Удалить</a></td>			
			</tr>
	
	@endforeach	
	<tr><td colspan="8">!!!!</td></tr>
	</table>
@stop
