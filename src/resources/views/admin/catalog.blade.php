@extends('admin.layouts._layout_admin')
@section('content')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
    <h1>Категории товаров</h1>
	
	<a href="{{route('admin.Acategory@add')}}" > <button>Добавить категорию</button></a>
<form name="search" method="post" action="{{route('admin.Acategory@searchpost')}}">
    @csrf
	<input type="search" name="query" placeholder="Поиск">
    <button type="submit">Найти</button> 
</form>	

 
	<table class="catalog_table"><tr><th>ID</th><th>Наименование</th><th>URL</th><th>Позиция</th><th>Статус</th><th></th><th></th></tr>	 
		@foreach($catalog as $cat)
	
			<tr >
			<td class="id"> {{$cat['id']}}</td>
		    <td class="name"><a href="{{route('admin.Acategory@acat',['id' =>$cat['id'] ])}}"><button>{{$cat['namet']}} </button></a></td>
			<td class="url">{{$cat['url']}}</td>
			<td class="sort">{{$cat['sort']}}</td>
			<td class="status">{{$cat['status']}}</td>
			<td><a href="{{route('admin.Acategory@edit',['id' =>$cat['id'] ])}}" id="{{$cat['id']}}" >Редактировать</a></td>
			<td><a href="{{route('admin.Acategory@del',['id' =>$cat['id'] ])}}" >Удалить</a></td>			
			</tr>
	
	@endforeach	
	<tr><td colspan="8">!!!!</td></tr>
	</table>
@stop
