   <!DOCTYPE html>
  <html>
  <head>
   <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
<style>
#edit_prod{
width:620px;
border:1px solid #C9D7F1;
background:#98fb98;
padding: 5px 10px 10px 10px;
margin-top:10px;
border-radius: 10px; 
-moz-border-radius: 10px; 
-webkit-border-radius: 10px; 
position: 
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto; 
}
 #main {
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      #okno {
        width: 300px;
		background:#98fb98;
        text-align: center;
        padding: 15px;
        border: 3px solid #0000cc;
        border-radius: 10px;
        color: #0000cc;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
      }
      #main:target {display: block;}
</style>
</head>
<body>
	<div id="edit_prod">
			<div class="popwindow">
				<div class="title_popwindow">
					Редактировать товар	
				</div>
				<div class="close_popwindow">
					<a href="{{route('admin.Catalogitem@index')}}" rel="cancel_edit_product" >
					<img  src="/img/img/admin/close.png"/>
					</a>
				</div>
			</div>	
			@if (count($errors) > 0)
			<a href="#" id="main">
 		    <div id="okno">
			<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
			</ul>
			</div>
			</a>
			<a href="#main"><h3 style="color:#ff4500">Посмотрите ошибки ввода информации</h3></a>
			@endif
			<form action="{{route('admin.Catalogitem@edit',['id' =>$products['id'],'page'=>$page])}}" method="post" enctype="multipart/form-data" class="contacts-form"  >
			@csrf
			<input type = "text" name = "id" value ={{$products['id']}} hidden />
			<table border="1">	
				<tr><td>Название:</td><td><input type="text" name="name" value="{{$products['name']}}" /></td> <td rowspan="5">Изображение:
				<div class="ed">
				Загрузить фото товара
				<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
				<input type="file" name="scr">
				</div> 
							
				<div id="edit">
				<img src="{{$products['scr']}}" alt="">
				</div>
	
				</tr>
				<tr><td>Артикул:</td><td><input type="text" name="artikul" value="{{$products['artikul']}}"/></td></tr>
				<tr><td>Barcode:</td><td><input type="text" name="barcode" value="{{$products['barcode']}}"/></td></tr>
				<tr><td>Цена:</td><td><input type="text" name="price" value="{{$products['price']}}"/> грн.</td></tr>
				<tr><td>Цена старая :</td><td><input type="text" name="price_old" value="{{$products['price_old']}}"/> грн.</td></tr>
				<tr><td>Описание:</td><td colspan="1"><textarea name="product_description"  style="width:100%; height: 250px;">{{$products['product_description']}}</textarea></td>
				<td>
				   <label> <input type="checkbox" name="is_top_sale"  value="1" {{$products['is_top_sale'] ? 'checked': null }}>Топ продаж<Br></label> 
				    @if($products['is_super_price'])<input type="checkbox" name="is_super_price" value="1"  checked>Супер цена<Br>
					@else<input type="checkbox" name="is_super_price" value="0" >Супер цена<Br>@endif
					@if($products['is_new'])<input type="checkbox" name="is_new" value="1"  checked>Новинка<Br>
					@else<input type="checkbox" name="is_new" value="1">Новинка<Br>@endif
					@if($products['is_color'])<input type="checkbox" name="is_color" value="1"  checked>Есть разные цвета<Br>
					@else<input type="checkbox" name="is_color" value="1">Есть разные цвета<Br>@endif
					@if($products['is_size'])<input type="checkbox" name="is_size" value="1"  checked>Есть разные размеры<Br>
					@else<input type="checkbox" name="is_size" value="1">Есть разные размеры<Br>@endif
					@if($products['active'])<input type="checkbox" name="active" value="1"  checked>Активно<Br>
					@else<input type="checkbox" name="active" value="1">Активно<Br>@endif
					@switch($products['sex'])
					@case('g')
					<p><input name="sex" type="radio" value="g" checked> Для девочек</p>
                    <p><input name="sex" type="radio" value="b"> Для мальчиков</p>
                    <p><input name="sex" type="radio" value="u"> Унисекс</p>
					@break
					@case('b')
					<p><input name="sex" type="radio" value="g"> Для девочек</p>
                    <p><input name="sex" type="radio" value="b" checked> Для мальчиков</p>
                    <p><input name="sex" type="radio" value="u"> Унисекс</p>
					@break
					@case('u')
					<p><input name="sex" type="radio" value="g"> Для девочек</p>
                    <p><input name="sex" type="radio" value="b"> Для мальчиков</p>
                    <p><input name="sex" type="radio" value="u" checked> Унисекс</p>
					@break
					@endswitch
					Возраст от <input type="text" name="agefrom" value="{{$products['agefrom']}}"/><Br>
					Возраст до <input type="text" name="ageto" value="{{$products['ageto']}}"/><Br>
				</tr>
				<tr><td><h3>Выберите бренд</h3>
				<p><select size="5" multiple name="brand_id">
               	@foreach($brand as $br)
                @if($br['id']==$products['brand_id'])
				<option value="{{$br['id']}}"selected>{{$br['name']}}</option>
                @else
				<option value="{{$br['id']}}">{{$br['name']}}</option>
				@endif
				@endforeach
                </select></p>
				</td>
				<td><h3>Выберите категорию</h3>
				<p><select size="5" multiple name="sub_category_id">
                @foreach($sub_category as $sub)
                @if($sub['id']==$products['sub_category_id'])
				<option value="{{$sub['id']}}"selected>{{$sub['namet']}}</option>
                @else
				<option value="{{$sub['id']}}">{{$sub['namet']}}</option>
				@endif
				@endforeach
                </select></p>
				</td>
				<td> Укажите позицию <input type="text" name="position" value="{{$products['position']}}"/><Br></td>
				</tr>
				<tr><td colspan="3" style="height:40px; text-align:right;">

				</td></tr>
			</table>
             
			    <label class="button"><input type="submit" value= "Сохранить">
                </label>
            </form>	
	</div>	
</body>	
</html>

