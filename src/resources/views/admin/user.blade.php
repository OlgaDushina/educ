@extends('admin.layouts._layout_admin')
@section('content')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
    <h1>База пользователей</h1>
	
<form name="search" method="post" action="{{route('admin.UserController@searchpost')}}">
    @csrf
	<input type="search" name="query" placeholder="Поиск">
    <button type="submit">Найти</button> 
</form>	


	<table class="catalog_table"><tr><th>ID</th><th>Имя</th><th>Телефон</th><th>Email</th><th>Право админа</th><th></th><th></th></tr>	 
	@foreach($user as $data)
	
	
			<tr >
			<td class="id">{{$data['id']}}</td>
			<td class="name">{{$data['name']}}</td>
			<td class="phone">{{$data['phone']}}</td>
			<td class="email">{{$data['email']}}</td>
			<td class="is_admin">{{$data['is_admin']}}</td>
			<td><a href="{{route('admin.UserController@edit',['id' =>$data['id'] ])}}" id="{{$data['id']}}" >Редактировать</a></td>
			<td><a href="{{route('admin.UserController@delete',['id' =>$data['id'] ])}}" >Удалить</a></td>			
			</tr>
	@endforeach	
	

	<tr><td colspan="8">!!!!</td></tr>
	</table>
@stop
