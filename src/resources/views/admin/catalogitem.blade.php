@extends('admin.layouts._layout_admin')
@section('content')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
    <h1>Каталог товаров</h1>
	
	<a href="{{route('admin.Catalogitem@add')}}" > <button>Добавить товар</button></a><br><br><br>
<form name="search" method="post" action="{{route('admin.Catalogitem@searchpost')}}">
    @csrf
	<input type="search" name="query" placeholder="Поиск">
    <button type="submit">Найти</button> 
</form>	


	<table class="catalog_table"><tr><th>ID</th><th>Изображение</th><th>Артикул</th><th>Название</th><th>Описание</th><th>Цена</th><th></th><th></th></tr>	 
	@foreach($products as $data)
	
	
			<tr >
			<td class="id">{{$data['id']}}</td>
			<td class="image_url"><img src="{{asset('storage/'.$data['scr'])}}" alt=""></td>
			<td class="artikul">{{$data['artikul']}}</td>
			<td class="name">{{$data['name']}}</td>
			<td class="product_description">{{$data['product_description']}}</td>
			<td class="price">{{$data['price']}}</td>
			<td><a href="{{route('admin.Catalogitem@edit',['id' =>$data['id'],isset($_GET['page']) ? $_GET['page']: 1])}}" id="{{$data['id']}}" >Редактировать</a></td>
			<td><a href="{{route('admin.Catalogitem@delete',['id' =>$data['id'] ])}}" >Удалить</a></td>			
			</tr>
	@endforeach	
	

	<tr><td colspan="8">{{ $products->links() }}</td></tr>
	</table>
	
@stop
