  <!DOCTYPE html>
  <html>
  <head>
   <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
<style>
#edit_prod{
width:620px;
border:1px solid #C9D7F1;
background:#98fb98;
padding: 5px 10px 10px 10px;
margin-top:10px;
border-radius: 10px; 
-moz-border-radius: 10px; 
-webkit-border-radius: 10px; 
position: 
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto; 
}
 #main {
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      #okno {
        width: 300px;
		background:#98fb98;
        text-align: center;
        padding: 15px;
        border: 3px solid #0000cc;
        border-radius: 10px;
        color: #0000cc;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
      }
      #main:target {display: block;}
</style>
</head>
<body>
	<div id="edit_prod">
			<div class="popwindow">
				<div class="title_popwindow">
					Редактировать таблицу пользователей
				<div class="close_popwindow">
					<a href="{{route('admin.UserController@index')}}"  >
					<img  src="/img/img/admin/close.png"/>
					</a>
				</div>
			</div>	
			@if (count($errors) > 0)
			<a href="#" id="main">
 		    <div id="okno">
			<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
			</ul>
			</div>
			</a>
			<a href="#main"><h3 style="color:#ff4500">Посмотрите ошибки ввода информации</h3></a>
			@endif
			<form action="{{route('admin.UserController@editpost')}}" method="post" class="contacts-form"  >
			@csrf
			<input type = "text" name = "id" value ={{$user['id']}} hidden />
			<table border="1">	
				<tr><td>Имя:</td><td><input type="text" name="name" value="{{$user['name']}}" /></td> 
				</tr>
				<tr><td>Телефон:</td><td><input type="phone" name="phone" value="{{$user['phone']}}"/></td></tr>
				<tr><td>Email:</td><td><input type="email" name="email" value="{{$user['email']}}"/></td></tr>
				<tr><td>Права:</td><td><input type="boolean" name="is_admin" value="{{$user['is_admin']}}"/> </td></tr>
				<tr><td colspan="3" style="height:40px; text-align:right;">

				</td></tr>
			</table>
             
			    <label class="button"><input type="submit" value= "Сохранить">
                </label>
            </form>	
	</div>	
</body>	
</html>

