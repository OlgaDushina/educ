   <!DOCTYPE html>
  <html>
  <head>
   <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
<style>
#edit_prod{
width:620px;
border:1px solid #C9D7F1;
background:#98fb98;
padding: 5px 10px 10px 10px;
margin-top:10px;
border-radius: 10px; 
-moz-border-radius: 10px; 
-webkit-border-radius: 10px; 
position: 
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto; 
}
 #main {
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      #okno {
        width: 300px;
		background:#98fb98;
        text-align: center;
        padding: 15px;
        border: 3px solid #0000cc;
        border-radius: 10px;
        color: #0000cc;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
      }
      #main:target {display: block;}
</style>
</head>
<body>
	<div id="edit_prod">
			<div class="popwindow">
				<div class="title_popwindow">
					<h3>Добавить товар.	 Все поля должны быть заполнены!!!</h3>
				</div>
				<div class="close_popwindow">
					<a href="{{route('admin.Catalogitem@index')}}" rel="cancel_edit_product" >
					<img  src="/img/img/admin/close.png"/>
					</a>
				</div>
			</div>	
			@if (count($errors) > 0)
			<a href="#" id="main">
 		    <div id="okno">
			<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
			</ul>
			</div>
			</a>
			<a href="#main"><h3 style="color:#ff4500">Посмотрите ошибки ввода информации</h3></a>
			@endif
			
			<form action="{{route('admin.Catalogitem@add')}}" method="post" enctype="multipart/form-data" class="contacts-form"  >
			@csrf
			<table border="1">	
				<tr><td>Название:</td><td><input type="text" name="name"  /></td> <td rowspan="5">Изображение:
				<div class="ed">
				Загрузить фото товара
				<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
				<input type="file" name="scr">
				</div> 
							
				<div id="edit">
				
				</div>
	
				</tr>
				<tr><td>Артикул:</td><td><input type="text" name="artikul" value=""/></td></tr>
				<tr><td>Barcode:</td><td><input type="text" name="barcode" value=""/></td></tr>
				<tr><td>Цена:</td><td><input type="text" name="price" value=""/> грн.</td></tr>
				<tr><td>Цена старая :</td><td><input type="text" name="price_old" value=""/> грн.</td></tr>
				<tr><td>Описание:</td><td colspan="1"><textarea name="product_description"  style="width:100%; height: 250px;"></textarea></td>
				<td>
				    <input type="checkbox" name="is_top_sale" value="1">Топ продаж<Br>
					<input type="checkbox" name="is_super_price" value="1" >Супер цена<Br>
					<input type="checkbox" name="is_new" value="1">Новинка<Br>
					<input type="checkbox" name="is_color" value="1">Есть разные цвета<Br>
					<input type="checkbox" name="is_size" value="1">Есть разные размеры<Br>
					<input type="checkbox" name="active" value="1">Активно<Br>
					<p><input name="sex" type="radio" value="g"> Для девочек</p>
                    <p><input name="sex" type="radio" value="b"> Для мальчиков</p>
                    <p><input name="sex" type="radio" value="u" checked> Унисекс</p>
					Возраст от <input type="text" name="agefrom" value=""><Br>
					Возраст до <input type="text" name="ageto" value=""><Br>
				</tr>
				<tr><td><h3>Выберите бренд</h3>
				<p><select size="5" multiple name="brand_id">
               	@foreach($brand as $br)
        				<option value="{{$br['id']}}">{{$br['name']}}</option>
				@endforeach
                </select></p>
				</td>
				<td><h3>Выберите категорию</h3>
				<p><select size="5"  name="sub_category_id">
                @foreach($sub_category as $subb)
				<option value="{{$subb['id']}}">{{$subb['namet']}}</option>
				@endforeach
                </select></p>
				</td>
				<td> Укажите позицию <input type="text" name="position" value=""/><Br></td>
				</tr>
				<tr><td colspan="3" style="height:40px; text-align:right;">

				</td></tr>
			</table>
		
			    <label class="button"><input type="submit" value= "Сохранить">
                </label>
            </form>	
	</div>	
</body>	
</html>

