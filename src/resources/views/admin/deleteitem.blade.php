    <!DOCTYPE html>
  <html>
  <head>
   <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/st.css') }}">
 <style>
  #zatemnenie {
        background: rgba(102, 102, 102, 0.5);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
      }
      #okno {
        width: 300px;
        text-align: center;
        background:#98fb98;
		padding: 15px;
        border: 3px solid #0000cc;
        border-radius: 10px;
        color: #0000cc;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
        background: #fff;
      }
      #zatemnenie:target {display: block;}
      .close {
        display: inline-block;
        border: 1px solid #0000cc;
        color: #0000cc;
        padding: 0 12px;
        margin: 10px;
        text-decoration: none;
        background: #f2f2f2;
        font-size: 14pt;
        cursor:pointer;
      }
      .close:hover {background: #e6e6ff;}
    </style>	
</head>
<body>
	<div id="zatemnenie">
      <div id="okno">
          <div>
            Вы действительно хотите удалить этот товар?<br>
            После удаления восстановление будет невозможно! <br>
            Продолжить?
          </div>
          <div style="text-align: left;">
           	<form action="{{route('admin.Catalogitem@delete',['id' =>$id])}}" method="post"  class="contacts-form"  >
			@csrf
		   <input type = "text" name = "id" value ={{$id}} hidden />
		   <ul>
           <li><input type="radio" name="del" value="1"> <button>Да</button></li>
           <li><input type="radio" name="del" value="0" checked> <button>Нет</button></li>
          </ul>
		  <label class="button"><input type="submit" value= "Решение принято"></label>
		  </form>
          </div>
 <!--       <a href="#" class="close">Закрыть окно</a>  -->
      </div>
    </div>

	</body>	
</html>
