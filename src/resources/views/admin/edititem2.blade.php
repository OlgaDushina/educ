@extends('layouts._layout')
@section('content')
    <div class="wrap">
        <div class="content">
            <div class="container">
                <h1>{{$products['name']}}</h1>
                <img src="img/temp/product-logo.jpg" alt="" class="product-logo">
                <div class="product-page-wrap">
                    <div class="column">
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="{{$products['scr']}}" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper gallery">
                                <a href="{{$products['scr']}}" class="swiper-slide">
                                    <img src="{{$products['scr']}}" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                                <a href="img/temp/gallery-big.jpg" class="swiper-slide">
                                    <img src="img/temp/gallery-big.jpg" alt="">
                                </a>
                            </div>
						    <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <span class="item-type">
                              @if($products->is_super_price) 
							  <span class="item-type__price">Суперцена</span>
                              @endif
							  @if($products->is_new)
							  <span class="item-type__new">Новинка</span>
                              @endif
						</span>
                     </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                     </div>
                     <div class="column">
                        <div class="_f _j-between _mb-30 _i-center">
                         <div class="vendor-code">Артикул: {{$products->artikul}}</div>
                        </div>
                        <div class="product-price">
                            @if($products->price_old<>0)
							<div class="old"><span>{{$products->price_old}}</span> грн.</div>
                            @endif
							<div class="new"><span>{{$products->price}}</span> грн.</div>
                        </div>
                        <div class="product-params">
                                @if($products->is_color) <?php $i='0';?>
								 <div class="product-params__column">
								 <div class="params-item__title">Цвет:</div>
                                <div class="_f _f-wrap">
                                  @foreach($product_count->where('product_id',$products->id)->sortBy('color')->all() as $pr) 
								   @if($pr->color<>$i) <?php $i=$pr->color;?>
								   <label class="color-item">
                                        <input type="radio" name="color" value="{{$pr->color}}">
                                        <span class="color" style="background-color:{{$pr->color}}"></span>
                                    </label>
                                 @endif
							    @endforeach
							   </div>
                              </div>
							  @endif
							  @if($products->is_size) <?php $i='0';?>
							  <div class="product-params__column">
                                <div class="params-item__title">Размер:</div>
                                <a href="javascript:void(0)" class="size-info-link">Таблица размеров</a>
                                <div class="_f _f-wrap">
                                    @foreach($product_count->where('product_id',$products->id)->sortBy('size')->all() as $pr)
                                   @if($pr->size<>$i) <?php $i=$pr->size;?>
									<label class="size-item">
                                        <input type="radio" name="size" value="26">
                                        <span class="size">{{$pr->size}}</span>
                                    </label>
                                    @endif
					                @endforeach
                               </div>
                            </div>
                             @endif
						</div>
                     </div>
                </div>
 

            </div>
        </div>
 
    </div>
@stop 
