@extends('layouts._layout')
@section('content')
       <div class="wrap">
             <div class="banner-wrap">
                <div class="container">
                    <div class="swiper-container banner">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="img/temp/banner.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/temp/banner.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="img/temp/banner.jpg" alt="">
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <div class="advantages">
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-1.svg" alt="">
                            </div>
                            <div class="advantages__title">Доставка по Украине и регионам</div>
                        </div>
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-2.svg" alt="">
                            </div>
                            <div class="advantages__title">Огромный ассортимент товаров в наличии</div>
                        </div>
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-3.svg" alt="">
                            </div>
                            <div class="advantages__title">Сертифицированные и безопасные товары</div>
                        </div>
                        <div class="advantages__item">
                            <div class="advantages__item-img">
                                <img src="img/img/advantages-4.svg" alt="">
                            </div>
                            <div class="advantages__title">Мы работаем пн-вс с 7:55 до 20:05</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profitable-offers">
                <div class="container">
                    <div class="profitable-slider-wrap">
                        <div class="catalog-slider-wrap profitable">
                            <div class="h1">Выгодные предложения</div>
                            <div class="swiper-container profitable-slider slider-arrows-top">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                                    </div>
                                </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                        <div class="top-slider-wrap">
                            <div class="swiper-container top-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="catalog-item-big">
    <span class="item-type">
        <span class="item-type__top">Топ продаж</span>
    </span>
    <a href="javascript:void(0)" class="catalog-item-big__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
    </a>
    <div class="catalog-item-big__info">
        <div class="catalog-item-big__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item-big__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="price-wrap">
            <div class="old"><span>42 999</span> грн</div>
            <div class="new">
                <div class="discount"><span>- 152</span> грн</div>
                <span>42 999</span> грн
            </div>
        </div>
        <a href="javascript:void(0)" class="button small" @click="$refs.buy.show()">
            Купить
        </a>
    </div>
</div>
                                    </div>
                                </div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="new-offers">
                <div class="container">
                    <div class="h1">Новинки</div>
                    <div class="swiper-container catalog-slider slider-arrows-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                            <div class="swiper-slide">
                                <div class="catalog-item">
    <a href="javascript:void(0)" class="catalog-item__like active" @click="$refs.favorite.show()"></a>
    <a href="javascript:void(0)" class="catalog-item__item-img">
        <img src="img/temp/catalog-item.jpg" alt="">
        <span class="item-type">
            <span class="item-type__price">Суперцена</span>
            <span class="item-type__new">Новинка</span>
        </span>
    </a>
    <div class="catalog-item__info">
        <div class="catalog-item__description">3 мес. – 3 года   TM Biomecanics</div>
        <a href="javascript:void(0)" class="catalog-item__title">Кроссовки Reima Elege Dark BlueКроссовки Reima Elege Dark Blue</a>
        <div class="_f _i-end _j-between">
            <div class="price-wrap">
                <div class="old"><span>42 999</span> грн</div>
                <div class="new"><span>42 999</span> грн</div>
            </div>
            <a href="javascript:void(0)" class="button" @click="$refs.buy.show()">
                Купить
            </a>
        </div>
    </div>
</div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
            <div class="catalog-grid-wrapper">
                <div class="container">
                    <div class="catalog-wrapper">
                        <a href="javascript:void(0)" class="one catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Товары для <br> малышей</div>
                            <img src="img/img/decor-1-1.svg" alt="" class="decor-1-1 decor-item">
                            <img src="img/img/decor-1-2.svg" alt="" class="decor-1-2 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="two catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Детская комната</div>
                            <img src="img/img/decor-2-1.svg" alt="" class="decor-2-1 decor-item">
                            <img src="img/img/decor-2-2.svg" alt="" class="decor-2-2 decor-item">
                            <img src="img/img/decor-2-3.svg" alt="" class="decor-2-3 decor-item">
                            <img src="img/img/decor-2-4.svg" alt="" class="decor-2-4 decor-item">
                            <img src="img/img/decor-2-5.svg" alt="" class="decor-2-5 decor-item">
                            <img src="img/img/decor-2-6.svg" alt="" class="decor-2-6 decor-item">
                            <img src="img/img/decor-2-7.svg" alt="" class="decor-2-7 decor-item">
                        </a>
                        <div class="three catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Одежда и Обувь</div>
                            <ul class="catalog-wrapper__item__list">
                                <li>
                                    <a href="javascript:void(0)">Верхняя одежда</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Одежда для малышей</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Штаны джинсы шорты</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Футболки и топы</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Кросовки и кеды</a>
                                </li>
                            </ul>
                            <img src="img/img/decor-3-1.svg" alt="" class="decor-3-1 decor-item">
                            <img src="img/img/decor-3-2.svg" alt="" class="decor-3-2 decor-item">
                            <img src="img/img/decor-3-3.svg" alt="" class="decor-3-3 decor-item">
                        </div>
                        <a href="javascript:void(0)" class="four catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Подобрать игрушку</div>
                            <img src="img/img/decor-4-1.svg" alt="" class="decor-4-1 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="five catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Настольные игры</div>
                            <img src="img/img/decor-5-1.svg" alt="" class="decor-5-1 decor-item">
                            <img src="img/img/decor-5-2.svg" alt="" class="decor-5-2 decor-item">
                            <img src="img/img/decor-5-3.svg" alt="" class="decor-5-3 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="six catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Куклы L.O.L</div>
                            <img src="img/img/decor-6-1.svg" alt="" class="decor-6-1 decor-item">
                        </a>
                        <a href="javascript:void(0)" class="seven catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">LEGO</div>
                            <img src="img/img/decor-7-1.svg" alt="" class="decor-7-1 decor-item">
                            <img src="img/img/decor-7-2.svg" alt="" class="decor-7-2 decor-item">
                            <img src="img/img/decor-7-3.svg" alt="" class="decor-7-3 decor-item">
                            <img src="img/img/decor-7-4.svg" alt="" class="decor-7-4 decor-item">
                            <img src="img/img/decor-7-5.svg" alt="" class="decor-7-5 decor-item">
                            <img src="img/img/decor-7-6.svg" alt="" class="decor-7-6 decor-item">
                        </a>
                        <div class="eight catalog-wrapper__item">
                            <div class="catalog-wrapper__item__title">Прогулки <br>и путешествия</div>
                            <ul class="catalog-wrapper__item__list">
                                <li>
                                    <a href="javascript:void(0)">Автокресла</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Коляски</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Рюкзаки, слинги, вожи</a>
                                </li>
                            </ul>
                            <img src="img/img/decor-8-1.svg" alt="" class="decor-8-1 decor-item">
                            <img src="img/img/decor-8-2.svg" alt="" class="decor-8-2 decor-item">
                        </div>
                    </div>
                </div>
            </div>
            <div class="brands-wrap">
                <div class="container">
                    <div class="swiper-container brands-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    Все бренды
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)" class="brand-item">
                                    <img src="img/temp/brand.svg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
         @widget('recent_news');
        <div class="about">
                <div class="container">
                    <div class="about-us">
                        <div class="about-us__description-wrap">
                            <dropdown-text
                                    open="Читать все"
                                    close="Читать меньше"
                            >
                                <h1>Товары для детей в интернет-магазине Kidsanna</h1>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                                <p>Все детские товары в одном месте - это интернет-магазин Антошка. Родителям больше не нужно выходить из дома, чтобы выбрать, сравнить, купить и получить с доставкой по своему адресу всё, что может понадобиться ребенку: от детского питания на любой возраст малыша до новинок индустрии детских игрушек. Все эти преимущества обеспечивает сервис интернет-магазина Антошка, где вы можете выбрать и заказать товары для детей 24/7. Ассортимент детских товаров в Антошке Интернет-магазин Антошка предоставляет каждому</p>
                            </dropdown-text>
                        </div>
                        <div class="about-us__form-wrap">
                            <div class="title">Узнавайте об акциях первыми</div>
                            <form action="/">
                                <label class="validation-field" :class="{ hasvalue: subscribe.email, error: $v.subscribe.email.$error }">
                                    <input v-model="$v.subscribe.email.$model" placeholder="email*">
                                    <div class="error" v-if="!$v.subscribe.email.email && $v.subscribe.email.$dirty ">Введите корректный e-mail</div>
                                    <div class="error" v-if="!$v.subscribe.email.required && $v.subscribe.email.$dirty">Поле обязательно для заполнения</div>
                                </label>
                                <label class="submit" :class="{ disable: $v.$anyError }">
                                    <input type="submit" :disabled="$v.$anyError">
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			</div>
@stop