<?php

use Illuminate\Support\Facades\Route;
//Route::get('/', function () {
//   return view('index', ['data'=>News::all()]);
//})->name('index');
/*	Route::get('/{page}', function ($page) {
    return view($page);
})->name('{page}');*/


Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
Route::group(['middleware' => ['auth', 'isadmin'], 'prefix' => 'admin', 'as' => 'admin.',], function()
{
	Route::get('/ddd', function () {
	return view('admin.cat');});
Route::get('/','App\Http\Controllers\Admin\Dashboard@index')->name('dashboard');
Route::get('/catalogi','App\Http\Controllers\Admin\Catalogitem@index')->name('Catalogitem@index');
Route::match(['get', 'post'], '/catalogi/{id}/{page}/edit_product', 'App\Http\Controllers\Admin\Catalogitem@edit')->name('Catalogitem@edit');
Route::match(['get', 'post'], '/catalogi/add_product', 'App\Http\Controllers\Admin\Catalogitem@add')->name('Catalogitem@add');
Route::match(['get', 'post'],'/catalogi/{id}/delete_product','App\Http\Controllers\Admin\Catalogitem@delete')->name('Catalogitem@delete');
Route::post('/catalogi/search_productp','App\Http\Controllers\Admin\Catalogitem@searchpost')->name('Catalogitem@searchpost');
Route::get('/user','App\Http\Controllers\Admin\UserController@index')->name('UserController@index');
Route::get('/user/edit','App\Http\Controllers\Admin\UserController@edit')->name('UserController@edit');
Route::post('/user/editp','App\Http\Controllers\Admin\UserController@editpost')->name('UserController@editpost');
Route::get('/user/delete','App\Http\Controllers\Admin\UserController@delete')->name('UserController@delete');
Route::post('/user/deletep','App\Http\Controllers\Admin\UserController@deletepost')->name('UserController@deletepost');
Route::post('/user/search','App\Http\Controllers\Admin\UserController@searchpost')->name('UserController@searchpost');
Route::get('/acategory','App\Http\Controllers\Admin\Acategory@index')->name('Acategory@index');
Route::get('/acat','App\Http\Controllers\Admin\Acategory@acat')->name('Acategory@acat');
Route::get('/asubcat','App\Http\Controllers\Admin\Acategory@asubcat')->name('Acategory@asubcat');
Route::get('/acategory/edit','App\Http\Controllers\Admin\Acategory@edit')->name('Acategory@edit');
Route::get('/acategory/add','App\Http\Controllers\Admin\Acategory@add')->name('Acategory@add');
Route::post('/acategory/search','App\Http\Controllers\Admin\Acategory@searchpost')->name('Acategory@searchpost');
Route::get('/acategory/del','App\Http\Controllers\Admin\Acategory@del')->name('Acategory@del');
Route::get('/asubcat/del','App\Http\Controllers\Admin\Acategory@del_s')->name('Acategory@del_s');
Route::post('/asubcat/delp','App\Http\Controllers\Admin\Acategory@del_spost')->name('Acategory@del_spost');
});	
	
Route::get('/register','App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');	
Route::post('/register','App\Http\Controllers\Auth\RegisterController@register');
Route::get('/login','App\Http\Controllers\Auth\LoginController@Login')->name('login');
Route::post('/login','App\Http\Controllers\Auth\LoginController@authenticate')->name('login_post');
Route::get('/catalog/{category}/f/','App\Http\Controllers\Catalog\Catalogfiltr@index')->name('Catalogfiltr@index');	
Route::get('/catalog/{category}/{subcategory}/f/','App\Http\Controllers\Catalog\Catalogfiltr@index1')->name('Catalogfiltr@index1');
Route::get('/catalog/{category}/{subcategory}/{items}/f/','App\Http\Controllers\Catalog\Catalogfiltr@index2')->name('Catalogfiltr@index2');
Route::get('/','App\Http\Controllers\Home@index')->name('index');
Route::get('/logout','App\Http\Controllers\Home@logout_u');
Route::get('/contacts','App\Http\Controllers\Contacts@index');
Route::get('/catalog/{category}','App\Http\Controllers\Catalog\Catalogp@index')->name('Catalogp@index');
Route::get('/catalog/{category}/{subcategory}','App\Http\Controllers\Catalog\Catalogs@index')->name('Catalogs@index');
Route::get('/catalog/{category}/{subcategory}/{items}','App\Http\Controllers\Catalog\Catalogf@index')->name('Catalogf@index');
Route::get('/catalog/{category}/{subcategory}/{items}/{item}','App\Http\Controllers\Catalog\Catalogi@index')->name('Catalogi@index');
Route::get('/news/{news}','App\Http\Controllers\NewsController@index');
Route::get('/map','App\Http\Controllers\Contacts@map');
Route::post('/contacts','App\Http\Controllers\Contacts@post')->name('Contacts@post');
Route::post('/call','App\Http\Controllers\Call@post')->name('Call@post');
/**
	Route::get('test',function(){
		return View::make('test');
	});*/


});

/*Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function(){ //...
});*/

